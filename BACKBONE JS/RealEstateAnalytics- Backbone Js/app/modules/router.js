/**
 * Created with JetBrains PhpStorm.
 * User: Ankit
 * Date: 11/24/13
 * Time: 3:31 AM
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'jquery-ui',
    'views/homepage/mapView',
    'views/homepage/optionsPanelView',
    'models/gmapDataModel',
    'globalcontrol'
], function ($, _, Backbone,jqueryui, mapView,optionsPanelView,gmapDataModel) {

    var mainRouter = Backbone.Router.extend({

        routes: {
            //Define routes here

            // Define some URL routes
             'login': 'test',
            //'signup': '',

            // Default
            'home': 'defaultAction',
            ''    : 'defaultAction'
        }

    });

    var initialize = function () {

        var mainroute = new mainRouter;

        mainroute.on('route:test', function () {

        });

        mainroute.on('route:defaultAction', function () {

            var selected_location = decodeURIComponent($.deparam().point);
            // We have no matching route, lets display the home page
            console.log('home-route');
            var optionsPanel = new optionsPanelView();
            optionsPanel.render();
            var HomeView = new mapView();
            HomeView.render(selected_location);
        });

        Backbone.history.start();
    };
    return {
        initialize: initialize,
        welletoRouter:mainRouter
    };
});
