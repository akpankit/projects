
/**
 * Created with JetBrains PhpStorm.
 * User: Ankit
 * Date: 11/22/13
 * Time: 5:15 PM
 */

require.config({

    paths:{
        "jquery":"vendor/jquery/jquery",
        "underscore":"vendor/underscore-amd/underscore",
        "backbone":"vendor/backbone-amd/backbone",
        "bootstrap":"vendor/bootstrap/bootstrap",
        "text"   :"vendor/requirejs-text/text",
        "numeraljs":"helpers/numeral.min",
        "scrollToFixed":"helpers/scrollToFixed",
        "globalcontrol":"helpers/globalControl",
        "raphael":"vendor/endless-modules/rapheal.min",
        "morris":"vendor/endless-modules/morris.min",
        "colorbox":"vendor/endless-modules/jquery.colorbox.min",
        "jcookie":"vendor/endless-modules/jquery.cookie.min",
        "sparklines":"vendor/endless-modules/jquery.sparkline.min",
        "gmapclustering":"vendor/googlemaps/gmapclustering",
        "jquery-ui":"//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min"
    },
    shim: {
        /* Set bootstrap dependencies (just jQuery) */
        "bootstrap" : ['jquery'],
         "jquery-ui": {
            exports: "jquery-ui",
            deps: ['jquery']
          },
        "numeraljs":{
            exports: "numeraljs",
            deps: ['jquery']
        },
        "scrollToFixed":{
            exports: "scrollToFixed",
            deps: ['jquery']
        },
        "raphael":{
            exports: "raphael",
            deps: ['jquery']
        },
        "morris":{
            exports: "morris",
            deps: ['jquery']
        },
        "colorbox":{
            exports: "colorbox",
            deps: ['jquery']
        },
        "jcookie":{
            exports: "jcookie",
            deps: ['jquery']
        },
        "sparklines":{
            exports: "sparkline",
            deps: ['jquery']
        },
        "gmapclustering":{
            exports: "gmapclustering",
            deps: ['jquery']
        },
        "globalcontrol":{
            exports: "globalcontrol",
            deps: ['jquery']
        }
    }
});

//Entry point of the app, normally the entry point to the app will be decided by router .


require([
    // Load our app module and pass it to our definition function
    'app',
    'helpers/general'
], function(App){
    // The "app" dependency is passed in as "App"
    // Again, the other dependencies passed in are not "AMD" therefore don't pass a parameter to this function
    window.OSName="Unknown OS";
    if (navigator.appVersion.indexOf("Win")!=-1) window.OSName="Windows";
    if (navigator.appVersion.indexOf("Mac")!=-1) window.OSName="MacOS";
    if (navigator.appVersion.indexOf("X11")!=-1) window.OSName="UNIX";
    if (navigator.appVersion.indexOf("Linux")!=-1) window.OSName="Linux";

    App.initialize();
});
