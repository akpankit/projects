/**
 * Created with JetBrains PhpStorm.
 * User: Ankit
 * Date: 11/24/13
 * Time: 3:46 AM
 * To change this template use File | Settings | File Templates.
 */

/*
 This view is responsible for loading google maps whenever the view is instantiated.
 "map_canvas" is required to load maps
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'gmapclustering',
    'helpers/general',
    "helpers/shareResource",
    'collections/gmap/gmapCollection',
    'views/results/resultsView'
], function ($, _, Backbone, generalJS,gmapclustering, appRegistry, gmapDataCollection, resultsView) {
    var eventAgg = _.extend({}, Backbone.Events);
    var topZips = [];
    var over;
    var reqQueue;
    var searchDict = {};

    var mapView = Backbone.View.extend({

        el: '#map_canvas',

        initialize: function () {
            window.highlight = false;
            window.enable_cluster = false;
        },
        createMap: function (options) {
            // Roughly the center of the white plains
            options || (options = {});
//            console.log(options.location);
            appRegistry.tempGlobals.polyLines = [];
            appRegistry.tempGlobals.prevZip = 0;

//            var latlng = new google.maps.LatLng(41.0339862, -73.7629097);

            var latlng = new google.maps.LatLng(options.location[0], options.location[1]);
            // Google Maps Options
            var myOptions = {
//                styles: this.mapStyling(),
                zoom: options.lastzoom ? options.lastzoom : 12,
                minZoom: 10,
                center: options.lastcenter ? options.lastcenter : latlng,
                mapTypeControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                    position: google.maps.ControlPosition.BOTTOM_CENTER
                },
                panControl: false,
                zoomControl: true,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.SMALL,
                    position: google.maps.ControlPosition.BOTTOM_CENTER
                },
                streetViewControl: true,
                streetViewControlOptions: {
                    position: google.maps.ControlPosition.BOTTOM_LEFT
                }
            };

            // Force the height of the map to fit the window
            this.$el.height($(window).height() - ($(".navbar").height() + $("#footer").height()));

            //make everything else map height
            $('#pref-panel,.wrp').height($('#map_canvas').css('height'));

            //remove the extra white space  from the nav bar
            $('.navbar').css('margin-bottom', 0);

            // Add the Google Map to the page and set it as global
            window.map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);

        },
        mapStyling: function () {
            var style = [
                {
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    featureType: "all",
                    elementType: "labels",
                    stylers: [
                        { visibility: "on" },
                        {"color": "black"}
                    ]
                },
                {
                    "featureType": "road",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#fee379"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#fee379"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#f3f4f4"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#7fc8ed"
                        }
                    ]
                },
                {},
                {
                    "featureType": "road.local",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {"color": "black"}
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#83cead"
                        }
                    ]
                },
                {
                    "elementType": "labels",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "landscape.man_made",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "weight": 0.9
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                }
            ]
            return style;
        },
        render: function (usergeo) {
            var userloc = usergeo.split(',');
            this.createMap({location: userloc});
            return this;
        },
        clearMap: function () {
            //reset variables
            topZips = [];

            if (typeof appRegistry.tempGlobals.polyLines !== 'undefined') {
                $.each(appRegistry.tempGlobals.polyLines, function (i, path) {
                    path.setMap(null);
                });
            }
        },
        initGmap: function (event, formParams) {

            //event: event of map search from optionsPanelView.
            console.log("initGmap started");
            var self = this;
            var MapSplitted;
            reqQueue = [];
            over = 0;
            var animation = [];
            topZips = [];


            //clear the map
            self.clearMap();
            //Now split the map
            var area = window.map.getBounds();

            //create clusters
            if(window.map.getZoom() < 13 && window.enable_cluster == true){

                window.clustering = true;
               self.createClusters(this.generateAjaxUrl(area, formParams));
                var zoomlistener = google.maps.event.addListener(window.map, 'zoom_changed', function() {

                    google.maps.event.removeListener(zoomlistener);
                    var zoomLevel = map.getZoom();
                    if(zoomLevel >= 13 && window.clustering == true){
                        window.clustering = false;
                        window.markerCluster.clearMarkers();
                        self.processRequests(self.generateAjaxUrl(window.map.getBounds(), formParams));
                    }
                });
            }else{
                self.processRequests(this.generateAjaxUrl(area, formParams));
            }
        },
        createClusters : function(link){

            var markers = [];
            $.ajax({
                url:'http://192.241.243.189/restful/search/'+link + '&a=cluster',
                type: 'GET',
                dataType: "json",async:false}).done(function(data) {
                    markers = data.markers;
                });

            for (var i = 0; i < markers.length; i++) {
                var lat = markers[i][1];
                var lng = markers[i][0];

                var latLng = new google.maps.LatLng(parseFloat(lat),parseFloat(lng));

                var marker = new google.maps.Marker({
                    position: latLng,
                    map:window.map
                });
                markers[i] = marker;
            }
            window.markerCluster = new MarkerClusterer(window.map,markers);
            //remove loader
            $('#loader').remove();
        },
        processRequests: function (urls) {


			window.ziptocity = [];
			var self = this;
			
			var bestMatchesInfo = "";
			var source = new EventSource('http://192.241.243.119/r/search/'+urls+'&test=1a');
			source.addEventListener('message', function(e) {
			  //console.log(e.data);
			  var data = {};
			  data['response'] = JSON.parse(e.data);
			  if(typeof data.response.stop != 'undefined'){
				source.close();
				$('#loader').remove();
				self.printBestMatches(bestMatchesInfo);
			  }
			  else{
				var tempInfo;
				tempInfo = self.getPaths(data);
				
				if(bestMatchesInfo == ""){ //if this is a first return - just assign it to bestMatchesInfo
					bestMatchesInfo = tempInfo;
				}
				else{ //if it's a second and above, we need to append/merge data
					if(typeof tempInfo['condo_count'] != 'undefined'){
						bestMatchesInfo['condo_count'] += tempInfo['condo_count'];
					}
					if(typeof tempInfo['single_count'] != 'undefined'){
						bestMatchesInfo['single_count'] += tempInfo['single_count'];
					}
					if(typeof tempInfo['zips'] != 'undefined'){
						for(zip in tempInfo['zips']){
							if(zip in bestMatchesInfo['zips']){
								bestMatchesInfo['zips'][zip] += tempInfo['zips'][zip];
							}
							else{
								bestMatchesInfo['zips'][zip] = tempInfo['zips'][zip];
							}
						}						
						bestMatchesInfo['resp_c'] += tempInfo['resp_c'];
					}
				}
			  }
			}, false);

			source.addEventListener('open', function(e) {
			  // Connection was opened.
			}, false);

			source.addEventListener('error', function(e) {
			  if (e.readyState == EventSource.CLOSED) {
				// Connection was closed.
			  }
			}, false);
            return;
        },
        splitMap: function () {

            /* we could split the bounding into multiple bounding boxes */
            var newBounds = [];
            var oldmap = [];
            var bounds = window.map.getBounds();
            var zoom = window.map.getZoom();
            var zoom_adj = 1;
            var lim = 0;
            if (zoom >= 15) {
                lim = 3000;
                zoom_adj = 1;
            }
            else if (zoom >= 14) {
                lim = 3000;
                zoom_adj = 2;
            }
            else if (zoom >= 13) {
                lim = 2000;
                zoom_adj = 4;
            }
            else if (zoom >= 12) {
                lim = 500;
                zoom_adj = 9;
            }
            else if (zoom >= 11) {
                lim = 300;
                zoom_adj = 12;
            }
            else if (zoom >= 10) {
                lim = 200;
                zoom_adj = 14;
            }
            else if (zoom >= 9) {
                lim = 200;
                zoom_adj = 14;
            }
            if (newBounds.length == 0) {
                var newLng = (bounds.getNorthEast().lng() - bounds.getSouthWest().lng()) / zoom_adj;
                var newLat = (bounds.getNorthEast().lat() - bounds.getSouthWest().lat()) / zoom_adj;
                //console.debug(newLat +" "+newLng);

                var minLat = bounds.getSouthWest().lat();
                var minLng = bounds.getSouthWest().lng();
                //inc lat / long by 0.001 so the boxes overlap

                for (var x = 0; x < zoom_adj; x++) {
                    for (var y = 0; y < zoom_adj; y++) {
                        newBounds.push(new Array(
                            new Array(minLat + (newLat * x), minLng + (newLng * y)),
                            new Array((minLat + newLat) + (newLat * x) + 0.001, (minLng + newLng) + (newLng * y) + 0.001))
                        );
                    }
                }

            } // end of newBounds.length == 0
            return newBounds;//return the new generated bounds of current map view.
        },
        generateAjaxUrl: function (bounds, formParams) {
            /*
             Following api url syntax, need only 4 points of map to define the bounding area.
             should go in order like mapbounds/sliders/priorities/pointset/housingfilters.
             */

            var url = ''; //initialize the empty url
            url += bounds;
            url += '/' + formParams.retail + '/' + formParams.transport + '/' + formParams.food + '/' + formParams.community;

            //check if priorities are set, if nothing than append 0
            if(typeof formParams.priorites  != 'undefined' && formParams.priorites != 0){
                url += '/'+ formParams.priorites.join(';');
            }
            else{
                url += '/0';
            }

            //check if pointset is set, if nothing than append 0
            if(appRegistry.allowPointSet == true){
                url += '/'+window.pointSetURL;
            }else{
                url += '/0';
            }

            //last part we don't need to append zero because its optional as per API design.
            if(typeof formParams.actions != 'undefined'){
                url += '/'+formParams.actions;
            }

            url = url.replace(/ /g,'');
            url += '?zoom_level=' + window.map.getZoom();
//            console.log(url);
            return url;
        },
        paramParser: function (formParams) {
            //this should be the last checkpoint to sanitize any form parameters before the url being constructed.

        },
        getPaths: function (payload) {
			/*keep track of top zips, etc inside bestMatchesInfo
			* this is to simulate the data.php process */			
			var bestMatchesInfo = {};
			bestMatchesInfo['zips'] = {};
			bestMatchesInfo['condo_count'] = 0;
			bestMatchesInfo['single_count'] = 0;
			bestMatchesInfo['resp_c'] = 0;
			/*******************************************/
			
            var self = this;
			var cnt = 0;
            $.each(payload['response'], function (id, info) {
			
				if(typeof info['zip'] != 'undefined'){
					cnt ++;
					var temp = [];//temp array  to parse the path and add to polyline

					for (var key in info['path']) { // iterate on object properties
						var value = info['path'][key].split(',');
						var long = value[1];
						var lat = value[0];

						temp.push(new google.maps.LatLng(lat, long));

					}
					//set city for zipcode
					window.ziptocity[info['zip']] = info['city'];
					//Now draw paths
					self.drawPaths(temp, info, '#F75D59');
					
					//keep track of top zips, etc inside bestMatchesInfo
					//this is to simulate the data.php process
					
						if(info['zip'] in bestMatchesInfo['zips']){
							bestMatchesInfo['zips'][info['zip']]++;
						}
						else{
							bestMatchesInfo['zips'][info['zip']] = 1;
						}
				}
            });
			
			/* keep track of top zips, etc inside bestMatchesInfo
			* this is to simulate the data.php process */
			bestMatchesInfo['condo_count'] = payload['response']['condo_count'];
			bestMatchesInfo['single_count'] = payload['response']['single_count'];
			bestMatchesInfo['resp_c'] = (cnt);
			//console.log(bestMatchesInfo['zips']);
			
			return bestMatchesInfo;
        },
		printBestMatches : function(bestMatchesInfo)
		{
			var self = this;
			console.log("bestMatchesInfo ");
			console.log(bestMatchesInfo);
			
			var sortable = [];
			for (zip in bestMatchesInfo['zips']) {
				sortable.push([zip, bestMatchesInfo['zips'][zip]]);
			}
			sortable.sort(function(a, b) {
				if(a[1] < b[1]) return 1;
				if(a[1] > b[1]) return -1;
				return 0;
				//return !(a[1] - b[1])
			});

            //top 5 zips results
            var zipsOutput = '';
			var cnt = 0;
			for(id in sortable){
				//console.log(zip);
				zipsOutput += '<div class="bm_row"><p><a href="#" class="zip"><u>' + sortable[id][0] + "</u></a> - "+ self.toTitleCase(ziptocity[sortable[id][0]]) +": " + sortable[id][1] + " Streets</p></div>";
				cnt++;
				if(cnt >= 5) break;
			}
			
            var total_sales = bestMatchesInfo['condo_count'] + bestMatchesInfo['single_count'];
//            console.log('<span class="text">Streets: '+ payload['response'].length +' | Zip Codes: '+window.ziptocity.length +'| Estimated homes for sale: '+ total_sales +'</span>');
            $('.toplineresults').html('<span class="text">Streets: '+ bestMatchesInfo['resp_c'] +' | Zip Codes: '+ Object.keys(bestMatchesInfo['zips']).length +'| Estimated homes for sale: '+total_sales+'</span>');
            $('.fltr_box > div.best_matched_list').html(zipsOutput);
			
		},
        toTitleCase : function(str)
        {
            return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
        },
        drawPaths: function (paths, info, color) {

            var flightPath = new google.maps.Polyline({
                path: paths,
                strokeColor: color,
                strokeOpacity: 0.8,
                strokeWeight: 3,
                blockInfo: info,
                highlighted: false
            });

            //organize polylines by zipcode
            if (typeof appRegistry.tempGlobals.zippath[info['zip']] != 'undefined' && appRegistry.tempGlobals.zippath[info['zip']][info['block_id']]) {
                appRegistry.tempGlobals.zippath[info['zip']][info['block_id']] = flightPath;
            } else {
                if (typeof appRegistry.tempGlobals.zippath[info['zip']] == 'undefined') {
                    appRegistry.tempGlobals.zippath[info['zip']] = {};
                }
                appRegistry.tempGlobals.zippath[info['zip']][info['block_id']] = flightPath;
            }

            //store  all polylines so we can clear them out later
            appRegistry.tempGlobals.polyLines.push(flightPath);

            google.maps.event.addListener(flightPath, 'click', function (point) {

                //Reset the previously clicked polyline color
                if (typeof appRegistry.tempGlobals.prev !== 'undefined' && this.highlighted == false && this.blockInfo['zip'] != appRegistry.tempGlobals.prevZip) {

                    appRegistry.tempGlobals.prev.setOptions({strokeColor: '#F75D59', strokeOpacity: 0.8, strokeWeight: 4});

                } else if (typeof appRegistry.tempGlobals.prev !== 'undefined' && this.highlighted == true) {
                    appRegistry.tempGlobals.prev.setOptions({strokeColor: '#FF8F00', strokeOpacity: 0.8, strokeWeight: 4});
                }

                //Apply the color to currently clicked polyline
                appRegistry.tempGlobals.prev = this;
                this.setOptions({strokeColor: 'blue', strokeOpacity: 0.8, strokeWeight: 5});

                var res = new resultsView(point, this.blockInfo);
                //set view status
                if(window.resultsview != null){
                    window.resultsview.destroy_view();
                    window.resultsview =  res.render();
                }else{
                    window.resultsview =  res.render();
                }
            });
            flightPath.setMap(window.map);
        },
        highlightZip: function (zipcode) {
            var self = this;
            var lines = appRegistry.tempGlobals.zippath[zipcode];
            window.highlight = true;
            if (appRegistry.tempGlobals.prevZip == zipcode) {
                return;
            }
            //clear the previous highlighted zips, at this point !=0 or >0 doesnt matter for now
            if (appRegistry.tempGlobals.prevZip != 0) {

                //loop through line instances on map to reset color
                var prevLines = appRegistry.tempGlobals.zippath[appRegistry.tempGlobals.prevZip];
                $.each(prevLines, function (block, lineInst) {
                    lineInst.highlighted = false;
                    lineInst.setOptions({strokeColor: '#F75D59', strokeOpacity: 0.8, strokeWeight: 4});
                });
            }

            //loop through line instances on map for clicked zip.
            $.each(lines, function (block, lineInst) {
                lineInst.highlighted = true;
                lineInst.setOptions(({strokeColor: '#FF8F00', strokeOpacity: 0.8, strokeWeight: 5}));
            });

            appRegistry.tempGlobals.prevZip = zipcode;
        },
        drawMarker: function (markers, animation) {
            if (animation == true || animation == 'reset') {
                var image = animation ? "./images/loader.gif" : null;
                for (marker in markers) {
                    var mark = new google.maps.Marker({
                        position: new google.maps.LatLng(markers[marker]['lat'], markers[marker]['lng']),
                        title: "F U!!" + marker,
                        map: window.map,
                        icon: image,
                        optimized: false
                    });
                    mark.setMap(window.map)
                }
            } else {
                for (marker in markers) {
                    var mark = new google.maps.Marker({
                        position: new google.maps.LatLng(markers[marker]['lat'], markers[marker]['lng']),
                        title: "F U!!" + marker
                    });
                    mark.setMap(window.map)
                }
            }

        }
    });
    return mapView; //End of the view, we return the view to the router/caller.
});



