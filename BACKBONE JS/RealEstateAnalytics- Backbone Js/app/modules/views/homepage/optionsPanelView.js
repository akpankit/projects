/**
 * Created with JetBrains PhpStorm.
 * User: Ankit
 * Date: 11/25/13
 * Time: 2:33 AM
 * To change this template use File | Settings | File Templates.
 */

define([
    'router',
    'jquery',
    'underscore',
    'backbone',
    'scrollToFixed',
    'helpers/shareResource',
    'helpers/general',
    'helpers/priorities',
    'views/homepage/mapView',
    'views/results/zipresults',
    'collections/homepage/optionsPanelCollection',
    'collections/gmap/gmapCollection',
    'text!templates/home/options-panel.html',
    'text!templates/home/housing-panel.html',
    'text!templates/home/neighborhood-panel.html'
], function(Router,$,_, Backbone,scrolltofixed,appRegistry,generalJS,prioritiesJS,mapView,zipresultsView,panelCollection,gmapDataCollection,optionPanelTemplate,housingPanelTemplate,neighborhoodPanelTemplate){

    var optCol = '';
    var zipview;

    var panelView = Backbone.View.extend({

        el: '.options-panel-body',

        initialize:function(){
            //get list of sliders
            optCol = new panelCollection();
        },

        render : function(){

            //passing in the array of models(data) and the underscore to render the templates.
            var data = {
                data:optCol.models,
                _:_
            };
            var compileTemplate = _.template(optionPanelTemplate, data);
//            this.$el.html(compileTemplate);
            $('.options-panel-body').html(compileTemplate);

            // display sliders once the template is compiled and set up
            $.each(optCol.models, function(i, model){
                $( "#"+model.get('targetDiv')+"-slider" ).slider({
                    range: model.get('range'),
                    values:model.get('values'),
                    min: model.get('min'),
                    max: model.get('max'),
                    step: model.get('step'),
                    slide: function( event, ui ) {
                        var name = model.get('targetDiv');

                        $( "#"+model.get('targetDiv')).val( ui.values[0] +"-"+ui.values[1] );

                        if(ui.values[0] == 1 && ui.values[1] == 2){
                            $( "#"+model.get('targetDiv')).data('level','Low');
                        }
                        if(ui.values[0] == 2 && ui.values[1] == 4){
                            $( "#"+model.get('targetDiv')).data('level','Medium');
                        }
                        if(ui.values[0] == 4 && ui.values[1] == 5){
                            $( "#"+model.get('targetDiv')).data('level','High');
                        }
                        console.log($( "#"+model.get('targetDiv')).data('level'));
                    }
                });
            }); // enable slider loop ends

            //Now load housing panel template
            housing_data = {};
            var housingTempalte = _.template(housingPanelTemplate,data);
            $('.housing-panel').append(housingTempalte);
            //set up sliders for  housing panel view
            $("#rooms-slider").slider({
                range: true,
                values:[2,4],
                min: 1,
                max: 5,
                step:1,
                slide: function( event, ui ) {
                    $("#room_num").val( ui.values[0]+"-"+ ui.values[1]);
                }
            });

            $("#budget-slider").slider({
                range: true,
                values: [100000,700000],
                min: 0,
                max: 5000000,
                step:10000,
                slide: function( event, ui ) {
                    console.log(ui.values);
                    $("#sel-budget").val(ui.values[0]+"-"+ ui.values[1]);
                }
            });

            //Now load Neighborhood panel
            nghbrpanel_data = {};
            var neighborhoodTempalte = _.template(neighborhoodPanelTemplate,nghbrpanel_data);
            $('.neighborhood-panel').append(neighborhoodTempalte);

            //tmporary changes to css
            if(window.OSName == 'Windows'){
                $('#pref-panel').css('width',300);
                $('.full_map').css('padding-left',300);
            }
            this.setElement($('.neighborhood-panel'));
//            console.log(this.$el);
            return this;
        },
        reset:function(){
            console.log(Router);
            Router.welletoRouter.navigate("",{trigger:true});

        },
        events:{
            "click .fltr_btns button#search-btn":"submit",
            "click #priority a":"doPriority",
            "click a#reset-form":"formreset"
        },
        formreset: function(e){
            e.preventDefault();
//            alert('resest');
            var formreset = {};
            $('.options-panel-body').empty();
            $('.housing-panel').empty();
            $('.neighborhood-panel').empty();
            this.initialize();
            this.render();
        },
        doPriority: function(event){
            event.preventDefault();

            appRegistry.allowPointSet = true;

            this.setupPriority();
        },
        setupPriority: function(){
            var PointSet;
            var self = this;
            var PointSetMarker;
            var PointSetCircle;

            google.maps.event.addListener(window.map, 'click', function(event) {
                //if(allowPointSet){
                PointSet = event.latLng;
                //console.log(PointSetMarker);
                if(typeof PointSetMarker != 'undefined'){
                    //console.log("ok 1");
                    var p = PointSetMarker.getMap() instanceof Object;
                    if(p === true){
                        //console.log("ok 2");
                        PointSetMarker.setMap(null);
                        PointSetCircle.setMap(null);
                    }
                }
                PointSetMarker = new google.maps.Marker({position: event.latLng, map: window.map});
                PointSetCircle = new google.maps.Circle({
                    map: window.map,
                    radius: ($("#priority_point_within").val() / 0.00062137),    // 10 miles in metres
                    fillColor: '#AA0000',
                    fillOpacity: 0.2,
                    strokeWeight: 1,
                    zIndex: 0,
                    draggable: true,
                    clickable: false
                });
                PointSetCircle.bindTo('center', PointSetMarker, 'position');
                //}

                self.generatePriority(PointSet);
            });
        },
        generatePriority: function(PointSet){
            //console.log(PointSet);
            var PointSetSquare;
            var url;
            if(typeof PointSet != 'undefined'){
                PointSetSquare = "";
                var temp = google.maps.geometry.spherical.computeOffset( PointSet, ($("#priority_point_within").val() / 0.00062137), 0);
                PointSetSquare += temp.lng()+","+temp.lat()+",";
//console.log("["+temp.lng()+","+temp.lat()+"],");
                temp = google.maps.geometry.spherical.computeOffset( PointSet, ($("#priority_point_within").val() / 0.00062137), 90);
                PointSetSquare += temp.lng()+","+temp.lat()+",";
//console.log("["+temp.lng()+","+temp.lat()+"],");
                temp = google.maps.geometry.spherical.computeOffset( PointSet, ($("#priority_point_within").val() / 0.00062137), 180);
                PointSetSquare += temp.lng()+","+temp.lat()+",";
//console.log("["+temp.lng()+","+temp.lat()+"],");
                temp = google.maps.geometry.spherical.computeOffset( PointSet, ($("#priority_point_within").val() / 0.00062137), 270);
                PointSetSquare += temp.lng()+","+temp.lat()+",";
//console.log("["+temp.lng()+","+temp.lat()+"]");
            }
            url = PointSet.lng()+","+PointSet.lat()+","+$("#priority_point_within").val()+","+PointSetSquare;

            window.pointSetURL = url;
            console.log(url);
            return url;
        },
        submit: function(event){
            event.preventDefault();

            //destroy the results views
            if(window.resultsview != null ){
                window.resultsview.destroy_view();
            }

            //show the loader
            $('#map_canvas').append("" +
                "<div id='loader'><img src='./images/loader3.gif'></div>" +
                "");

            //take care of parameters formatting
            //convert form parameters to serialize object using custom helper function from helpers/general.js
            var formParams = $('form#filters-form').serializeObject();
            var priorities = [];
            var property_filters = [];

            //set up paramaters for urls
            if(!formParams['action'] || !formParams['area_has']){
                delete formParams['budget'];
                delete formParams['rooms'];
            }else{
                property_filters.push(formParams['budget']);
                property_filters.push(formParams['rooms']);
                delete formParams['budget'];
                delete formParams['rooms'];
                formParams['actions'] = property_filters.join(',') + ',' +formParams['area_has'];
            }

            if(formParams['places'] && formParams['places_within']){
                formParams['places'] = formParams['places']+','+ formParams['places_within'];
                priorities.push(formParams['places']);
            }else{delete formParams['places'];delete formParams['places_within'];}

            if(formParams['commute'] && formParams['commute_within']){
                formParams['commute'] = formParams['commute']+','+ formParams['commute_within'];
                priorities.push(formParams['commute']);
            }else{delete formParams['commute'];delete formParams['commute_within'];}

            if(formParams['schools'] && formParams['schools_within']){
                formParams['schools'] = formParams['schools']+','+ formParams['schools_within'];
                priorities.push(formParams['schools']);
            }else{delete formParams['schools'];delete formParams['schools_within'];}

            if(priorities.length > 0){
            formParams['priorites'] = priorities;
            }else{
                formParams['priorities'] = 0;
            }

            //after filtering not needed params set the order of parameters.
            console.log(formParams);

            if(zipview == null){
                zipview = new zipresultsView;
                zipview.render();
            }else{
                console.log('destroyView');
                zipview.destroy_view();
                zipview = new zipresultsView;
                zipview.render();
            }
                var mapViewInst = new mapView;
                mapViewInst.initGmap(event,formParams);
        }
    });
    return panelView;
});
