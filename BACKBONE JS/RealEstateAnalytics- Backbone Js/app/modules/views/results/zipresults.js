/**
 * Created with JetBrains PhpStorm.
 * User: Ankit
 * Date: 12/27/13
 * Time: 7:59 AM
 * 
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'numeraljs',
    'helpers/shareResource',
    'helpers/general',
    'views/homepage/mapView',
    'views/results/popUpView',
    'text!templates/results/zip-results.html',
    'text!templates/results/zipoptions.html'
], function($,_, Backbone,numeralJS,appRegistry,generalJS,mapview,popUp,zipsPanel,zipOptions){

    var resCollection;
    var block_id ;
    var topEntities;

    var zipresults = Backbone.View.extend({

        el:'div#zipres > div#filter_wrp',
        initialize:function(){

            appRegistry.tempGlobals.zippath = [];

            //control top margins when zip options are shown
            $('.street-results-panel').css('margin-top',$('.zip-box').height() + 10);
            console.log("street" + $('.street-results-panel').height() + "zip" + $('.zip-box').height());
            $('.surrounding-results-panel').css('margin-top', $('.street-results-panel.panel').height() + $('.zip-box').height() + 30);

        },
        render:function(){
            var data = {};
            var compileTemplate = _.template(zipsPanel, data);
            this.$el.html(compileTemplate);
            return this;
        },
        events:{
            "click div.bm_row a.zip":"zipoptions",
            "click .info_btn":"showPopup"
        },
        showPopup:function(e){
            e.preventDefault();
            var popUps = new popUp();
            popUps.render();

            $("#dialog-modal").dialog({
                   modal:true,
                    width:800,
                    open: function(){
                    $('.ui-widget-overlay').bind('click',function(){
                        $('#dialog-modal').dialog('close');
                    })
                }
            });

            //set dialog to map height
            $("div.ui-dialog.ui-widget.ui-front").css('top','20px');
            $('#wrapper').height($('#map_canvas').css('height'));
            $('#wrapper').css('min-height',$('#map_canvas').css('height'));
            $('#wrapper').css('overflow','auto');

        },
        destroy_view: function() {

            //COMPLETELY UNBIND THE VIEW
            this.undelegateEvents();

            this.$el.removeData().unbind();

            //Remove view from DOM
            $('div#filter_wrp').empty();
        },
        zipoptions:function(event){

             event.preventDefault();
            var clickedZip = $(event.currentTarget).text().trim();

            $('div.bm_tool').remove();

            //handle the showing and not showing of zip options

            $.each($('div.best_matched_list div.bm_row'),function(sel,div){
                var zip = $(div).find('a').text().trim();

                if(($(div).hasClass('bm_row_open') && zip == clickedZip) || $(div).hasClass('bm_row_open')){

                    $(div).removeClass('bm_row_open');

                }else if(!$(div).hasClass('bm_row_open') && zip == clickedZip ){
					var zillow_params = "";
					if($('select[name=action] option:selected').text() != "Select"){
						//zillow pattern: /2-_beds/50000-100000_price/
						var bed_min = $('#room_num').val().substring(0,1);
						var sel_budget = $('#sel-budget').val().split("-");
						var home_type = "";
						switch($('select[name=area_has] option:selected').text()){
							case "Condo/Townhouse":
								home_type = "apartment_condo_type";
								break;
							case "Single Family Home":
								home_type = "house_type";
								break;
						}
						zillow_params = "/"+bed_min+"-_beds/"+sel_budget[0]+"-"+sel_budget[1]+"_price/"+home_type;
					}
					
                    var compiletemplate = _.template(zipOptions);
                    $(event.currentTarget).parent().parent().addClass('bm_row_open');
                    $(event.currentTarget).parent().after(compiletemplate({zipc:clickedZip,zillow_params:zillow_params}));
                }
            });

            //once done with zipoptions features now lets highlight paths.
            var map = new mapview;
            map.highlightZip(clickedZip);
        }
    });
    return zipresults;
});