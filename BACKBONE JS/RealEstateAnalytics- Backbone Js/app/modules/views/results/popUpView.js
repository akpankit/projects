
/**
 * Created with JetBrains PhpStorm.
 * User: Ankit
 * Date: 11/28/13
 * Time: 6:35 AM
 * To change this template use File | Settings | File Templates.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'numeraljs',
    'helpers/shareResource',
    'helpers/general',
    'collections/results/resultsCollection',
    'text!templates/results/popUp.html'
], function($,_, Backbone,numeralJS,appRegistry,generalJS,resultsCollection,PopUpTemplate){


    var popUp = Backbone.View.extend({

        el:'#dialog-modal',
        initialize:function(){

        },
        render:function(){
            var data = {};
            var compileTemplate = _.template(PopUpTemplate, data);
            this.$el.html(compileTemplate);
            return this;
        },
        reset:function(){

        }

    });

    return popUp;
});