/**
 * Created with JetBrains PhpStorm.
 * User: Ankit
 * Date: 11/28/13
 * Time: 6:35 AM
 * To change this template use File | Settings | File Templates.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'numeraljs',
    'helpers/shareResource',
    'helpers/general',
    'views/homepage/mapView',
    'collections/results/resultsCollection',
    'text!templates/results/results-panel.html'
], function($,_, Backbone,numeralJS,appRegistry,generalJS,mapView,resultsCollection,resultsPanelTemplate){

    var resCollection;
    var block_id ;
    var topEntities;
    _.templateSettings.variable = "m"

    var resultsView = Backbone.View.extend({

        el:'#zipres',
        initialize:function(event,info){
            info || (info = {});
            var newInfo = this.cleanInfo(info);
            block_id = newInfo['block_id'];
            topEntities = this.getTopEntities();
            newInfo['topEntities'] = topEntities;
            resCollection = new resultsCollection(newInfo);
        },
        cleanInfo: function(info){

            var reInfo = {
                block_id:info['block_id'],
                streetName: info['name'],
                providers:['zillow'],
                property: {
                    count:info['properties_count'],
                    condo_minPrice:info['p_condo_min'] ? this.CurrencyFormat(info['p_condo_min'].toString()) : null,
                    condo_maxPrice: info['p_condo_max'] ? this.CurrencyFormat(info['p_condo_max'].toString()) :null,
					condo_avgPrice: info['p_condo_avg'] ? this.CurrencyFormat(info['p_condo_avg'].toString()) :null,
                    single_minPrice:info['p_single_min'] ? this.CurrencyFormat(info['p_single_min'].toString()) : null,
                    single_maxPrice: info['p_single_max'] ? this.CurrencyFormat(info['p_single_max'].toString()) :null,
                    single_avgPrice: info['p_single_avg'] ? this.CurrencyFormat(info['p_single_avg'].toString()) :null,
					
					averagePrice:info['B25077e1'] ? this.CurrencyFormat(info['B25077e1'].toString()) : null,
					
                    bedcondomin:info['bed_condo_min'] ? info['bed_condo_min'] : null,
                    bedcondomax:info['bed_condo_max'] ? info['bed_condo_max'] : null,
                    bedsinglemin:info['bed_single_min'] ? info['bed_single_min'] : null,
                    bedsinglemax:info['bed_single_max'] ? info['bed_single_max'] : null,
                    minBed:info['bed_min'],
                    maxBed:info['bed_max']
                },
                averageRent:info['B25064e1'] ? this.CurrencyFormat(info['B25064e1'].toString()): null,
                location:{
                    city:this.toTitleCase(info['city']),
                    state:info['state'].toUpperCase(),
                    zip:info['zip'],
                    coordinates:info['coords']
                },
                extraInfo:{
                    path:info['path']
                }
            }
            return reInfo;
        },
        toTitleCase : function (str)
        {
            return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
        },
        CurrencyFormat: function (n)
        {
            //convert to short currency to save space
            var fnum = numeral(n).format('($ 0a)');
            var unit = fnum.slice(-1);
            fnum = fnum.replace(unit,unit.toUpperCase());
            return fnum;
        },
        render: function(){
			/******************************************************************* 
				used for zillow link generation 
			    same code exists inside zipresults.js - maybe move into 1 place ?
			***********************************************************************/
			var zillow_params = "";
			if($('select[name=action] option:selected').text() != "Select"){
				//zillow pattern: /2-_beds/50000-100000_price/apartment_condo_type/
				var bed_min = $('#room_num').val().substring(0,1);
				var sel_budget = $('#sel-budget').val().split("-");
				var home_type = "";
				switch($('select[name=area_has] option:selected').text()){
					case "Condo/Townhouse":
						home_type = "apartment_condo_type";
						break;
					case "Single Family Home":
						home_type = "house_type";
						break;
				}
				zillow_params = "/"+bed_min+"-_beds/"+sel_budget[0]+"-"+sel_budget[1]+"_price/"+home_type;
			}
			/***********************************************************************/
			
            var data = {
                data:resCollection.models,
                _:_,
				zillow_params:zillow_params
            };
            console.log('view');
            console.log(data);
            var compileTemplate = _.template(resultsPanelTemplate, data);
            this.$el.nextAll().remove();
            this.$el.append(compileTemplate);

            //change top margins of the results view dynamically for now.
            // TODO CSS fix needed for this
            $('.street-results-panel').css('margin-top',$('.zip-box').height() + 10)
            $('.surrounding-results-panel').css('margin-top', 135 + parseInt($('.street-results-panel').css('margin-top')));

            //set surrounding panel max height based of top margins
            var totalheight = $('#map_canvas').height();
            var areaperc = $('.zip-box').height() + $('.street-results-panel').height();
            var areacovered = (areaperc * 100)/totalheight;
            var unusedarea = ((100 - areacovered)/100) * totalheight;

//            console.log("c"+parseInt(areacovered));
//            console.log("u"+ parseInt(unusedarea));


            $('.surrounding-results-panel').css('max-height',parseInt(unusedarea) - 52);
            return this;
        },
        destroy_view: function() {

        //COMPLETELY UNBIND THE VIEW
//        this.undelegateEvents();

//        this.$el.removeData().unbind();

        //Remove view from DOM
        $('div#filter_wrp').nextAll().remove();
        },
        getTopEntities:function(){

        var topEntity ;
            //get top lists only  when requested.
            $.ajax({
                url:'http://192.241.243.189/restful/get/block_tops/'+block_id,
                type: 'GET',
                dataType: "json",async:false}).done(function(data) {
                    console.log(data);
                    var importantKeys = {};
                    var level = '';
                    $.each(data,function(option,entity){
                        if(option.length == 0){
                            return;
                        }
                        var k ='';
                        if(option =="TOP_NIGHTLIFE"){
                            k = "Nightlife";
                            level = $('#COMMUNITY_AMENITIES_DENSITY').data('level');
                        }
                        if(option =="TOP_GROCERY"){
                            k = "Grocery";
                            level = $('#RETAIL_DENSITY').data('level');
                        }
                        if(option =="TOP_SCHOOL"){
                            k = "Schools";
                            level = $('#COMMUNITY_AMENITIES_DENSITY').data('level');
                        }
                        if(option =="TOP_TRANSPORT"){
                            k = "Transportation";
                            level = $('#TRANSPORT_DENSITY').data('level');
                        }						
						if(option =="TOP_FEATURES"){
                            k = "Features";
                            level = $('#RETAIL_DENSITY').data('level');
                        }						
						if(option =="TOP_PLACES_OF_WORSHIP"){
                            k = "Worship";
                            level = $('#COMMUNITY_AMENITIES_DENSITY').data('level');
                        }
						
						
                        if(k == ''){
                           k = 'other';
                           level = 'none';
                        }
                            importantKeys[k]={};
                        $.each(entity,function(key,value){
                            importantKeys[k][key] = {};
//                            importantKeys[k][key]['option_title']= key;
                            importantKeys[k][key]['name']= value['name'];
                            importantKeys[k][key]['distance'] = parseFloat(value['dist']).toFixed(2);

                        });
                         //set level
                        importantKeys[k]['level']=level;
                    });
                    topEntity = importantKeys;
                });
            console.log(topEntity);
            return topEntity;
        }

    });
    return resultsView;
});