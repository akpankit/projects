/**
 * Created with JetBrains PhpStorm.
 * User: Ankit
 * Date: 11/28/13
 * Time: 8:49 PM
 *
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'models/resultsModel'
], function($,_,Backbone,resultsModel){

    var resultsCollection = Backbone.Collection.extend({

        model: resultsModel,
        initialize:function(data){
            this.add(data);
        }
    });
    return resultsCollection;
});