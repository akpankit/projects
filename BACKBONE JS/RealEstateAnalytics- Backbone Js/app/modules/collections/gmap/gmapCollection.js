/**
 * Created with JetBrains PhpStorm.
 * User: Ankit
 * Date: 11/26/13
 * Time: 1:52 AM
 * To change this template use File | Settings | File Templates.
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'models/gmapDataModel'
], function($,_,Backbone,gmapDataModel){

    var gmapDataCollection = Backbone.Collection.extend({

        model:gmapDataModel,
        initialize: function(options) {
            options || (options = {});
        },
        url:function(){
           return  "/getdata/1/";
        },
        parse:function(response){
            this.response = response.response;
            this.zips = response.zips;
            this.paths = response.paths;
            this.resp_c = response.resp_c;
        }
    });

    return gmapDataCollection;
});