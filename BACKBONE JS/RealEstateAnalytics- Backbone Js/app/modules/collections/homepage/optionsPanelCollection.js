/**
 * Created with JetBrains PhpStorm.
 * User: Ankit
 * Date: 11/25/13
 * Time: 7:35 PM
 *
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'models/sliders-model'
 ], function($,_,Backbone,sliderModel){

    var SlidersCollection = Backbone.Collection.extend({

        model: sliderModel,

        initialize: function(){

            //Add default sliders
            this.add([
                {
                    targetDiv: 'RETAIL_DENSITY',
                    title: "Retail Options",
                    name: "retail"
                },
                {
                    targetDiv:'TRANSPORT_DENSITY',
                    title: "Transport Options",
                    name: "transport"
                },
                {
                    targetDiv:'FOOD_DRINK_DENSITY',
                    title: "Food Options",
                    name: "food"
                },
                {
                    targetDiv: 'COMMUNITY_AMENITIES_DENSITY',
                    title: 'Community Amenities',
                    name: 'community'
                }
            ]);
        }

    })
    return SlidersCollection;
});
