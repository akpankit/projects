/**
 * Created with JetBrains PhpStorm.
 * User: Ankit
 * Date: 11/23/13
 * Time: 10:30 PM
 * To change this template use File | Settings | File Templates.
 */
define([
    'jquery',
    'underscore',
    'backbone',
    'jquery-ui'
], function ($, _, Backbone,jqueryui) {

    var sliderModel = Backbone.Model.extend({
        defaults: {
            range: true,
            values:[2,4],
            min: 1,
            max: 5,
            step:1,
            targetDiv:'',
            name:'',
            title:''
        }

    });
    //end of slider model
    return sliderModel;
});




