/**
 * Created with JetBrains PhpStorm.
 * User: Ankit
 * Date: 11/28/13
 * Time: 4:45 PM
 * To change this template use File | Settings | File Templates.
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'jquery-ui'
], function ($, _, Backbone,jqueryui) {

    var resultsModel = Backbone.Model.extend({

       defaults:
       {
           block_id:'',
           streetName: '',
           providers:['zillow'],
           property: {
               count:'',
               type:'',
               condo_minPrice:'',
               condo_maxPrice:'',
               single_minPrice:'',
               single_maxPrice:'',
               averagePrice:'',
               minBed:'',
               maxBed:''
           },
           averageRent:'',
           years:'',
           salesActivity:'',
           location:{
               city:'',
               state:'',
               zip:'',
               coordinates:''
           },
           topEntities:{

           },
           extraInfo:{
               path:''
           }
       }
    });
    return resultsModel;
});


