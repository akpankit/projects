/**
 * Created with JetBrains PhpStorm.
 * User: Ankit
 * Date: 11/24/13
 * Time: 3:43 AM
 * 
 */

// Filename: app.js
define([
    'jquery',
    'underscore',
    'backbone',
    'router' // Request router.js
], function($, _, Backbone, Router){
    var initialize = function(){
        // Pass in our Router module and call it's initialize function
        Router.initialize();
    };

    return {
        initialize: initialize
    };
});
