﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web page/MPage1.master" AutoEventWireup="true" CodeFile="~/Web page/Register.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="main" Runat="Server">
<div id="contentPanel">
    <asp:Table runat="server" HorizontalAlign="Center">        
        <asp:TableHeaderRow HorizontalAlign="Center">
            <asp:TableCell ColumnSpan="2"><h2>Register</h2></asp:TableCell>
        </asp:TableHeaderRow>
        <asp:TableRow>
            <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                Username
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtUsername" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                Password
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtPassword" TextMode="Password" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                Confirm password:
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtConfirm" runat="server" TextMode="Password"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                Fullname:
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtFullname" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                Email:
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                Phone:
            </asp:TableCell>
            <asp:TableCell>
                <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                Profile image:
            </asp:TableCell>
            <asp:TableCell>
                <asp:TableCell><asp:FileUpload ID="fu1" runat="server" /></asp:TableCell>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow> 
            <asp:TableCell></asp:TableCell>          
            <asp:TableCell HorizontalAlign="center">
                <asp:Button ID="btnRegister" runat="server" CssClass="login" OnClick="btnRegister_Click" Text="Register" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</div>
</asp:Content>

