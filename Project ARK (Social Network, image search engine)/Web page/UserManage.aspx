﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web page/Mpage1.master" AutoEventWireup="true" CodeFile="UserManage.aspx.cs" Inherits="Web_page_UserManage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="up" Runat="Server">
<div id="menuContent">
    <ul>
        <li><asp:HyperLink ID="HyperLink4" Text="My image" runat="server" NavigateUrl="~/Web page/PixManage.aspx"><span>My image</span></asp:HyperLink></li>
        <li><div class="blank"></div></li>
        <li><asp:HyperLink ID="HyperLink5" Text="My Friend" runat="server" NavigateUrl="~/Web page/FriendManage.aspx"><span>My Friend</span></asp:HyperLink></li>
        <li><div class="blank"></div></li>
        <li><asp:HyperLink ID="HyperLink6" Text="Message" runat="server" NavigateUrl="~/Web page/MessageManage.aspx"><span>Message</span></asp:HyperLink></li>
        <li><div class="blank"></div></li>        
    </ul>
</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" Runat="Server">
<div id="contentPanel">    
    <asp:Table ID="tableRequest" BorderColor="#497f35" BorderStyle="Inset" BorderWidth="1" runat="server">
        <asp:TableHeaderRow>
            <asp:TableCell ColumnSpan="5"><h2>Register Request</h2></asp:TableCell>
        </asp:TableHeaderRow>
    </asp:Table><br />
    <asp:Table ID="tableUser" BorderColor="#497f35" BorderStyle="Inset" GridLines="Both" runat="server">
        <asp:TableHeaderRow>
            <asp:TableCell ColumnSpan="6"><h2>User List</h2></asp:TableCell>
        </asp:TableHeaderRow>
    </asp:Table>
</div>
</asp:Content>

