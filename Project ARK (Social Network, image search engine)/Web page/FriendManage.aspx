﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web page/Mpage1.master" AutoEventWireup="true" CodeFile="~/Web page/FriendManage.aspx.cs" Inherits="Web_page_FriendManage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="up" Runat="Server">
<div id="menuContent">
    <ul>
        <li><asp:HyperLink ID="HyperLink1" Text="My image" runat="server" NavigateUrl="~/Web page/PixManage.aspx"><span>My image</span></asp:HyperLink></li>
        <li><div class="blank"></div></li>
        <li><asp:HyperLink ID="HyperLink2" Text="Message" runat="server" NavigateUrl="~/Web page/MessageManage.aspx"><span>Message</span></asp:HyperLink></li>        
        <li><div class="blank"></div></li>
        <li><asp:HyperLink ID="lnkUser" Visible="false" Text="Manage user" runat="server" NavigateUrl="~/Web page/UserManage.aspx"><span>Manage</span></asp:HyperLink></li>
    </ul>
</div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="main" Runat="Server">
<div id="contentPanel">
    <asp:Table ID="tableFriendRequest" runat="server">
        <asp:TableHeaderRow>
            <asp:TableCell ColumnSpan="4">
                <h2>Friend Request</h2>
            </asp:TableCell>
        </asp:TableHeaderRow>
    </asp:Table>
    <asp:Table ID="tableFriend" runat="server">
        <asp:TableHeaderRow >            
            <asp:TableCell ColumnSpan="3">
                <h2>Friend list</h2>
            </asp:TableCell>
        </asp:TableHeaderRow>
    </asp:Table>
    <asp:Table ID="tableBlackList" runat="server">
        <asp:TableHeaderRow >            
            <asp:TableCell ColumnSpan="3">
                <h2>Black list</h2>
            </asp:TableCell>
        </asp:TableHeaderRow>
    </asp:Table>
</div>
</asp:Content>

