﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Web_page_MessageManage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //verify if the current user has logged in
        if (Session.Count == 0) Response.Redirect("~/Web page/Default.aspx");
        if (new ClassProcess().isSU(Session["user"].ToString()))
        {
            lnkUser.Visible = true;
        }
        TableRow row;
        TableCell cell;

        //init warning message response
        DataTable table = new ClassProcess().getMessageList("sender", Session["user"].ToString());
        if (table != null && table.Rows.Count != 0)
        {
            // init header
            row = new TableRow();
            cell = new TableCell();
            cell.Text = "From";
            cell.Font.Bold = true;
            row.Controls.Add(cell);

            cell = new TableCell();
            cell.Text = "Content";
            cell.Font.Bold = true;
            row.Controls.Add(cell);

            cell = new TableCell();
            cell.Text = "Response";
            cell.Font.Bold = true;
            row.Controls.Add(cell);
            row.HorizontalAlign = HorizontalAlign.Center;
            tableResponse.Controls.Add(row);

            for (int i = 0; i < table.Rows.Count; i++)
            {
                //init sender
                row = new TableRow();
                cell = new TableCell();
                cell.Text = table.Rows[i]["receiver"].ToString();
                row.Controls.Add(cell);

                //init content
                cell = new TableCell();
                cell.Text = table.Rows[i]["messagecontent"].ToString();
                row.Controls.Add(cell);

                //init response
                cell = new TableCell();
                cell.Text = table.Rows[i]["responsecontent"].ToString();
                row.Controls.Add(cell);
                row.VerticalAlign = VerticalAlign.Middle;

                //init accept button
                cell = new TableCell();
                LinkButton lnk = new LinkButton();
                lnk.Text = "Accept";
                lnk.CssClass = "login";
                lnk.SkinID = table.Rows[i]["id"].ToString();
                lnk.Click += new EventHandler(btn_Click);
                cell.Controls.Add(lnk);
                row.Controls.Add(cell);

                //init reject button
                cell = new TableCell();
                lnk = new LinkButton();
                lnk.Text = "Reject";
                lnk.CssClass = "login";
                lnk.SkinID = table.Rows[i]["id"].ToString();
                lnk.Click += new EventHandler(btn_Click);
                cell.Controls.Add(lnk);
                row.Controls.Add(cell);
                //add row to table
                tableResponse.Controls.Add(row);
            }
        }
        else
        {
            row = new TableRow();
            cell = new TableCell();
            cell.Text = "There is no warning message response";
            row.Controls.Add(cell);
            tableResponse.Controls.Add(row);
        }

        // Init inbox
        table = new ClassProcess().getMessageList("receiver", Session["user"].ToString());
        if (table != null && table.Rows.Count != 0)
        {
            // init header
            row = new TableRow();
            cell = new TableCell();
            cell.Text = "From";
            cell.Font.Bold = true;
            row.Controls.Add(cell);

            cell = new TableCell();
            cell.Text = "Content";
            cell.Font.Bold = true;
            row.Controls.Add(cell);

            cell = new TableCell();
            cell.Text = "Response";
            cell.Font.Bold = true;
            row.Controls.Add(cell);

            cell = new TableCell();
            cell.Text = "Status";
            cell.Font.Bold = true;
            row.Controls.Add(cell);
            row.HorizontalAlign = HorizontalAlign.Center;
            tableInbox.Controls.Add(row);

            for (int i = 0; i < table.Rows.Count; i++)
            {
                //init sender
                row = new TableRow();
                cell = new TableCell();
                cell.Text = table.Rows[i]["sender"].ToString();
                row.Controls.Add(cell);

                //init content
                cell = new TableCell();                
                cell.Text = table.Rows[i]["messagecontent"].ToString();
                row.Controls.Add(cell);

                //init response
                cell = new TableCell();
                if (!table.Rows[i]["responsecontent"].ToString().ToLower().Equals(""))
                {
                    cell.Text = "responsed";
                    row.BackColor = System.Drawing.Color.Aqua;
                }
                else
                {
                    HyperLink lnk = new HyperLink();
                    lnk.Text = "Reply";
                    // check if this is the last time user can log in
                    if (int.Parse(new ClassProcess().getOutstanding(Session["user"].ToString())) < 2)
                    {
                        lnk.NavigateUrl = "~/Web page/MessageResponse.aspx?sender=" + table.Rows[i]["sender"].ToString();
                    }
                    lnk.Font.Underline = false;
                    cell.Controls.Add(lnk);
                }
                cell.HorizontalAlign = HorizontalAlign.Center;
                row.Controls.Add(cell);

                //init status
                cell = new TableCell();
                if (table.Rows[i]["outstanding"].ToString().ToLower().Equals("true"))
                    cell.Text = "warning";
                else cell.Text = "accepted";
                cell.HorizontalAlign = HorizontalAlign.Center;
                row.Controls.Add(cell);
                row.VerticalAlign = VerticalAlign.Middle;
                tableInbox.Controls.Add(row);
            }
        }
        else
        {
            row = new TableRow();
            cell = new TableCell();
            cell.Text = "There is no message";
            row.Controls.Add(cell);
            tableInbox.Controls.Add(row);
        }

        // Init sent
        table = new ClassProcess().getSentMessageList(Session["user"].ToString());
        if (table != null && table.Rows.Count != 0)
        {
            // init header
            row = new TableRow();
            cell = new TableCell();
            cell.Text = "Sent to";
            cell.Font.Bold = true;
            row.Controls.Add(cell);

            cell = new TableCell();
            cell.Text = "Content";
            cell.Font.Bold = true;
            row.Controls.Add(cell);

            cell = new TableCell();
            cell.Text = "Response";
            cell.Font.Bold = true;
            row.Controls.Add(cell);
            row.HorizontalAlign = HorizontalAlign.Center;
            tableSent.Controls.Add(row);

            for (int i = 0; i < table.Rows.Count; i++)
            {                
                //init sender
                row = new TableRow();
                cell = new TableCell();
                cell.Text = table.Rows[i]["sender"].ToString();
                row.Controls.Add(cell);

                //init content
                cell = new TableCell();
                cell.Text = table.Rows[i]["messagecontent"].ToString();
                row.Controls.Add(cell);

                //init response
                cell = new TableCell();
                cell.Text = table.Rows[i]["responsecontent"].ToString();
                row.Controls.Add(cell);
                row.VerticalAlign = VerticalAlign.Middle;
                tableSent.Controls.Add(row);
            }
        }
        else
        {
            row = new TableRow();
            cell = new TableCell();
            cell.Text = "There is no message";
            row.Controls.Add(cell);
            tableSent.Controls.Add(row);
        }
    }

    protected void btn_Click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        new ClassProcess().deleteRow("Message", "id", lnk.SkinID);
        Response.Redirect("~/Web page/MessageManage.aspx");
    }
}