﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class _Default : System.Web.UI.Page
{
    DataTable table = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack) return;
        if (Session.Count != 0)
        {
            img.Visible = true;
            fu.Visible = true;
            btn.Visible = true;
            ListItem lItem = new ListItem("histogram","histogram");
            dropSearch.Items.Add(lItem);
            dropSearch.SelectedIndex = 3;
        }
        DataTable mytable = new ClassProcess().getAcceptedUserList();
        if (mytable != null && mytable.Rows.Count != 0)
        {
            TableRow row;
            TableCell cell;
            int c = 0;
            for (int i = 0; i < mytable.Rows.Count; i++)
            {
                string username = mytable.Rows[i]["username"].ToString();
                string ext = new ClassProcess().getInfo("user", "ext", "username", username);
                if (ext.Equals(""))
                {
                    c++;
                    continue;
                }
                row = new TableRow();
                //init cell
                cell = new TableCell();
                cell.Text = (i + 1).ToString() + ". " + username + "'s password has matched with the other's password. Add '" 
                    + ext + "' at the end of your password.";
                row.Controls.Add(cell);
                tableNews.Controls.Add(row);
            }
            if (c == mytable.Rows.Count) tableNews.Rows[0].Controls[0].Visible = false;
        }
        else tableNews.Visible = false;
        if (Request["pid"] != null)
        {
            //load the searched data
            loadData();
            if (dropSearch.SelectedIndex != 0)
                initDisplay(int.Parse(Request["pid"]));
            else initOwnerDisplay(int.Parse(Request["pid"]));
        }
    }

    private void loadData()
    {
        switch (dropSearch.SelectedValue)
        {
            case "owner":
                table = new ClassProcess().getUser("%" + txtSearch.Text.Trim() + "%");
                if (table != null && table.Rows.Count != 0)
                {
                    initOwnerDisplay(1);
                }
                else lblResult.Text = "There is no user matching with your search";
                break;
            case "popular":
                //get list of all image
                DataTable mytable = new ClassProcess().getImageList("%");
                if (mytable != null && mytable.Rows.Count != 0)
                {
                    //store pixid of pix
                    string[] pixids = new string[mytable.Rows.Count];
                    //store number of visit on pix
                    int[] visits = new int[mytable.Rows.Count];
                    //get the value for 2 above array
                    for (int i = 0; i < mytable.Rows.Count; i++)
                    {
                        if (new ClassProcess().getSumOfVisit(mytable.Rows[i]["pixid"].ToString()).Equals(""))
                            visits[i] = int.Parse(mytable.Rows[i]["visit"].ToString());
                        else visits[i] = int.Parse(new ClassProcess().getSumOfVisit(mytable.Rows[i]["pixid"].ToString()))
                            + int.Parse(mytable.Rows[i]["visit"].ToString());
                        pixids[i] = mytable.Rows[i]["pixid"].ToString();
                    }
                    //sort descent due to visit times
                    sort(pixids, visits);
                    //init table to display
                    table = new DataTable();
                    table.Columns.Add("pixid");
                    table.Columns.Add("path");
                    for (int i = 0; i < visits.Length; i++)
                    {
                        //add data to table
                        table.Rows.Add(pixids[i], new ClassProcess().getPixPath(pixids[i]));
                    }
                    //display searched result
                    initDisplay(1);
                }
                else lblResult.Text = "There is no image matching with your search";
                break;
            case "pix":
                table = new ClassProcess().getSearchResult(txtSearch.Text.Split(' '));
                if (table != null && table.Rows.Count != 0)
                {
                    initDisplay(1);
                }
                else lblResult.Text = "There is no image matching with your search";
                break;
            case "histogram":
                table = new ClassProcess().getImageList("%");
                if (table != null && table.Rows.Count != 0)
                {
                    Histogram h1 = new Histogram();
                    img.AlternateText = MapPath(img.AlternateText);
                    h1.setImage(new System.Drawing.Bitmap(img.AlternateText));
                    h1.calcHisto(img);

                    for (int i = 0; i < table.Rows.Count; i++)
                    {
                        Histogram h2 = new Histogram();
                        Image imgTmp = new Image();
                        imgTmp.AlternateText = MapPath(table.Rows[i]["path"].ToString());
                        h2.setImage(new System.Drawing.Bitmap(imgTmp.AlternateText));
                        h2.calcHisto(imgTmp);

                        if (!h1.compareTo(h2.histogram))
                        {
                            table.Rows[i].Delete();
                        }
                    }
                    table.AcceptChanges();
                    if (table.Rows.Count != 0)
                    {
                        initDisplay(1);
                    }
                    else lblResult.Text = "There is no image matching with your search";
                }
                else lblResult.Text = "There is no image matching with your search";
                break;
        }
    }

    protected void lnkSearch_Click(object sender, EventArgs e)
    {
        if (dropSearch.SelectedIndex != 1 && dropSearch.SelectedIndex != 3)
        {
            if (txtSearch.Text.Trim().Equals(""))
            {
                txtSearch.Focus();
                lblResult.Text = "Enter content to search";
                return;
            }
            if (txtSearch.Text.Trim().Equals("%"))
            {
                txtSearch.Focus();
                return;
            }
        }
        loadData();
    }

    private void sort(string[] pixids, int[] visits)
    {
        for (int i = 0; i < visits.Length; i++)
        {
            for (int j = i + 1; j < visits.Length; j++)
            {
                if (visits[i] < visits[j])
                {
                    int tmp = visits[i];
                    visits[i] = visits[j];
                    visits[j] = tmp;

                    string tmpStr = pixids[i];
                    pixids[i] = pixids[j];
                    pixids[j] = tmpStr;
                }
            }
        }
    }

    private void initOwnerDisplay(int page)
    {
        int pagesize = 2;
        //clear all element in table
        tableImage.Controls.Clear();
        //init row and cell for the header
        TableRow row1 = new TableRow();
        TableRow row2 = new TableRow();
        TableCell cell = new TableCell();
        cell.Text = "<h2>Search result</h2>";
        cell.ColumnSpan = 5;
        cell.ForeColor = System.Drawing.Color.FromName("#497f35");
        row1.Controls.Add(cell);
        tableImage.Controls.Add(row1);
        row1 = new TableRow();
        int start = pagesize * (page - 1);
        int end = pagesize * page;
        if (end > table.Rows.Count || end == 0 || table.Rows.Count < pagesize * page) end = table.Rows.Count;
        for (int i = start; i < end; i++)
        {
            if (i % 5 == 0)
            {
                row1 = new TableRow();
            }
            //init cell
            cell = new TableCell();
            //init image
            Image img = new Image();
            img.AlternateText = table.Rows[i]["username"].ToString();
            img.ImageUrl = table.Rows[i]["profile"].ToString();
            img.Height = 85; img.Width = 85;
            //init link of image
            HyperLink lnk = new HyperLink();
            string owner = img.AlternateText;
            if (Session.Count != 0)
            {
                if (owner.Equals(Session["user"].ToString()))
                    lnk.NavigateUrl = "~/Web page/PixProfile.aspx?";
                else lnk.NavigateUrl = "~/Web page/FriendPix.aspx?username=" + owner;
            }
            else lnk.NavigateUrl = "~/Web page/FriendPix.aspx?username=" + owner;
            lnk.Controls.Add(img);
            cell.Controls.Add(lnk);
            row1.Width = 90;
            row1.HorizontalAlign = HorizontalAlign.Center;
            row1.Controls.Add(cell);

            //init cell
            cell = new TableCell();
            cell.HorizontalAlign = HorizontalAlign.Center;
            cell.Text = img.AlternateText;
            row2.Width = 90;
            row2.Controls.Add(cell);
            if (i % 5 == 0)
            {
                tableImage.Controls.Add(row1);
                tableImage.Controls.Add(row2);
            }
        }
        if (table.Rows.Count % 5 != 0)
        {
            tableImage.Controls.Add(row1);
            tableImage.Controls.Add(row2);
        }
        row1 = new TableRow();
        cell = new TableCell();
        cell.ColumnSpan = 5;
        if (table.Rows.Count / pagesize > 0)
        {
            int num = 0;
            if (table.Rows.Count / pagesize == 1 && table.Rows.Count % pagesize != 0) num = 2;
            else num = table.Rows.Count / pagesize + 1;
            for (int i = 0; i < num; i++)
            {
                //init page number link
                HyperLink btn = new HyperLink();
                btn.Text = (i + 1).ToString() + " ";
                string searchType = "";
                switch (dropSearch.SelectedValue)
                {
                    case "owner": searchType = "owner";
                        break;
                    case "popular": searchType = "popular";
                        break;
                    case "pix": searchType = "pix";
                        break;
                    case "histogram": searchType = "histogram";
                        break;
                }
                btn.NavigateUrl = "~/Web page/Default.aspx?pid=" + (i + 1).ToString() + "&type=" + searchType;
                btn.Font.Underline = false;
                cell.Controls.Add(btn);
                row1.Controls.Add(cell);
                row1.HorizontalAlign = HorizontalAlign.Right;
            }
        }
        tableImage.Controls.Add(row1);
    }

    private void initDisplay(int page)
    {
        int pagesize = 10;
        //clear all element in table
        tableImage.Controls.Clear();
        //init row and cell for the header
        TableRow row1 = new TableRow();
        TableRow row2 = new TableRow();
        TableCell cell = new TableCell();
        cell.Text = "<h2>Search result</h2>";
        cell.ColumnSpan = 5;
        cell.ForeColor = System.Drawing.Color.FromName("#497f35");
        row1.Controls.Add(cell);
        tableImage.Controls.Add(row1);
        row1 = new TableRow();
        int start = pagesize * (page - 1);
        int end = pagesize * page;
        if (end > table.Rows.Count || end == 0 || table.Rows.Count < pagesize * page) end = table.Rows.Count;
        for (int i = start; i < end; i++)
        {
            if (i % 5 == 0)
            {
                row1 = new TableRow();
            }
            //init cell
            cell = new TableCell();
            //init image
            Image img = new Image();
            img.AlternateText = table.Rows[i]["pixid"].ToString();
            img.ImageUrl = table.Rows[i]["path"].ToString();
            img.Height = 80; img.Width = 80;
            //init link of image
            HyperLink lnk = new HyperLink();
            string owner = new ClassProcess().getInfo("Pix", "owner", "pixid", img.AlternateText);
            if (Session.Count != 0)
            {
                if (owner.Equals(Session["user"].ToString()))
                    lnk.NavigateUrl = "~/Web page/PixView.aspx?pixid=" + img.AlternateText;
                else lnk.NavigateUrl = "~/Web page/FriendPix.aspx?username=" + owner;
            }
            else lnk.NavigateUrl = "~/Web page/FriendPix.aspx?username=" + owner;
            lnk.Controls.Add(img);
            cell.Controls.Add(lnk);
            row1.Width = 90;
            row1.HorizontalAlign = HorizontalAlign.Center;
            row1.Controls.Add(cell);

            //init cell
            cell = new TableCell();
            //init label for display info
            Label lbl = new Label();
            lbl.Text = "Title: " + new ClassProcess().getInfo("Pix", "title", "pixid", img.AlternateText) + "<br>";
            lbl.Text += "Owner: " + new ClassProcess().getInfo("Pix", "owner", "pixid", img.AlternateText) + "<br>";
            string type = new ClassProcess().getInfo("Pix", "type", "pixid", img.AlternateText);
            lbl.Text += "Type: " + new ClassProcess().getInfo("Type", "name", "typeid", type) + "<br>";
            string download = new ClassProcess().getInfo("Pix", "download", "pixid", img.AlternateText);
            if (download.Equals(""))
                download = "0";
            lbl.Text += "Download: " + download;
            cell.HorizontalAlign = HorizontalAlign.Justify;
            cell.Controls.Add(lbl);
            row2.Width = 90;
            row2.Controls.Add(cell);
            if (i % 5 == 0)
            {
                tableImage.Controls.Add(row1);
                tableImage.Controls.Add(row2);
            }
        }
        if (table.Rows.Count % 5 != 0)
        {
            tableImage.Controls.Add(row1);
            tableImage.Controls.Add(row2);
        }
        row1 = new TableRow();
        cell = new TableCell();
        cell.ColumnSpan = 5;
        if (table.Rows.Count / pagesize > 0)
        {
            int num = 0;
            if (table.Rows.Count / pagesize == 1 && table.Rows.Count % pagesize != 0) num = 2;
            else num = table.Rows.Count / pagesize + 1;
            for (int i = 0; i < num; i++)
            {
                //init page number link
                HyperLink btn = new HyperLink();
                btn.Text = (i + 1).ToString() + " ";
                string searchType = "";
                switch (dropSearch.SelectedValue)
                {
                    case "owner": searchType = "owner";
                        break;
                    case "popular": searchType = "popular";
                        break;
                    case "pix": searchType = "pix";
                        break;
                    case "histogram": searchType = "histogram";
                        break;
                }
                btn.NavigateUrl = "~/Web page/Default.aspx?pid=" + (i + 1).ToString() + "&type=" + searchType;
                btn.Font.Underline = false;
                cell.Controls.Add(btn);
                row1.Controls.Add(cell);
                row1.HorizontalAlign = HorizontalAlign.Right;
            }
        }
        tableImage.Controls.Add(row1);
    }

    protected void dropSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (dropSearch.SelectedIndex == 3)
        {
            img.Visible = true;
            fu.Visible = true;
            btn.Visible = true;
            txtSearch.Enabled = false;
        }
        else if (dropSearch.SelectedIndex == 1)
        {
            img.Visible = false;
            fu.Visible = false;
            btn.Visible = false;
            txtSearch.Enabled = false;
        }
        else
        {
            img.Visible = false;
            fu.Visible = false;
            btn.Visible = false;
            txtSearch.Enabled = true; ;
        }
    }
   
    protected void btn_CLick(object sender, EventArgs e)
    {
        if (fu.FileName.Trim().Equals(""))
            return;
        string realPath = MapPath("~/Upload Images");
        string ext = fu.FileName.Split('.')[fu.FileName.Split('.').Length - 1];
        string name = fu.FileName;
        fu.PostedFile.SaveAs(realPath + "/" + name + "." + ext);
        img.ImageUrl = "~/Upload Images/" + name + "." + ext;
        img.AlternateText = "~/Upload Images/" + name + "." + ext;
    }
}