﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web page/Mpage1.master" AutoEventWireup="true" CodeFile="~/Web page/FriendPix.aspx.cs" Inherits="FriendPix" %>
<asp:Content ID="Content2" ContentPlaceHolderID="up" runat="server">
<div id="menuContent">
    <ul>
        <li><asp:HyperLink ID="HyperLink4" Text="My image" runat="server" NavigateUrl="~/Web page/PixManage.aspx"><span>My image</span></asp:HyperLink></li>
        <li><div class="blank"></div></li>
        <li><asp:HyperLink ID="HyperLink5" Text="My Friend" runat="server" NavigateUrl="~/Web page/FriendManage.aspx"><span>My Friend</span></asp:HyperLink></li>
        <li><div class="blank"></div></li>
        <li><asp:HyperLink ID="HyperLink6" Text="Message" runat="server" NavigateUrl="~/Web page/MessageManage.aspx"><span>Message</span></asp:HyperLink></li>        
        <li><div class="blank"></div></li>
        <li><asp:HyperLink ID="lnkUser" Visible="false" Text="Manage user" runat="server" NavigateUrl="~/Web page/UserManage.aspx"><span>Manage</span></asp:HyperLink></li>
        <li><div class="blank"></div></li>
        <li><asp:LinkButton ID="lnkFriend" Text="Add Friend" Visible="false" runat="server" OnClick="lnkAdd_Click"></asp:LinkButton></li>
    </ul>
</div>

</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="main" Runat="Server">
<div id="contentPanel">    
    <center><asp:Label ID="lblError" runat="server" ForeColor="Red" Visible="false"></asp:Label></center>    
    <table>
        <tr>
            <td>
                <asp:Table ID="tableProfile" HorizontalAlign="Center" runat="server">
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2"><h2><asp:Label ID="lblProfile" ForeColor="#497f35" runat="server"></asp:Label></h2></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Fullname:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtFullname" Enabled="false" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Contact:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtContact" Enabled="false" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            Phone:
                        </asp:TableCell>
                        <asp:TableCell>
                            <asp:TextBox ID="txtPhone" Enabled="false" runat="server"></asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell></asp:TableCell>
                        <asp:TableCell>
                            <asp:LinkButton ID="btnUpdate" CssClass="login" Visible="false" Text="Update" runat="server"></asp:LinkButton>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
            <td>
                <asp:Table ID="tableImage" HorizontalAlign="Center" runat="server">
                    <asp:TableRow HorizontalAlign="Center">                        
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
    </table>    
</div>
</asp:Content>

