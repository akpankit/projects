﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Web_page_FriendManage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //verify if the current user has logged in
        if (Session.Count == 0) Response.Redirect("~/Web page/Default.aspx");
        if (new ClassProcess().isSU(Session["user"].ToString()))
        {
            lnkUser.Visible = true;
        }
        TableRow row;
        TableCell cell;
        DataTable table = new ClassProcess().getFriendRequestList(Session["user"].ToString());
        if (table != null && table.Rows.Count != 0)
        {
            // init header
            row = new TableRow();
            cell = new TableCell();
            cell.Text = "Order";
            cell.Font.Bold = true;
            cell.HorizontalAlign = HorizontalAlign.Center;
            row.Controls.Add(cell);

            cell = new TableCell();
            cell.Text = "Username";
            cell.Font.Bold = true;
            row.Controls.Add(cell);

            cell = new TableCell();
            cell.Text = "Contact";
            cell.Font.Bold = true;
            row.Controls.Add(cell);
            row.HorizontalAlign = HorizontalAlign.Center;
            tableFriendRequest.Controls.Add(row);

            int c = 0;
            for (int i = 0; i < table.Rows.Count; i++)
            {
                if (new ClassProcess().getInfo("relation", "friend", "owner", "friend",
                    Session["user"].ToString(), table.Rows[i]["owner"].ToString()).Equals(""))
                {
                    row = new TableRow();
                    //init order
                    cell = new TableCell();
                    cell.Text = (i + 1).ToString();
                    row.Controls.Add(cell);

                    //init username
                    cell = new TableCell();
                    HyperLink lnk = new HyperLink();
                    lnk.Text = table.Rows[i]["owner"].ToString();
                    lnk.Font.Underline = false;
                    lnk.NavigateUrl = "~/Web page/FriendPix.aspx?username=" + table.Rows[i]["owner"].ToString();
                    cell.Controls.Add(lnk);
                    row.Controls.Add(cell);

                    //init contact
                    cell = new TableCell();
                    cell.Text = new ClassProcess().getInfo("user", "contact", "username", table.Rows[i]["owner"].ToString());
                    row.Controls.Add(cell);

                    //init accept button
                    cell = new TableCell();
                    LinkButton btn = new LinkButton();
                    btn.CssClass = "login";
                    btn.Text = "Accept";
                    btn.SkinID = table.Rows[i]["owner"].ToString();
                    btn.Click += new EventHandler(btnAccept_Click);
                    cell.Controls.Add(btn);
                    row.Controls.Add(cell);

                    //init accept button
                    cell = new TableCell();
                    btn = new LinkButton();
                    btn.CssClass = "login";
                    btn.Text = "Reject";
                    btn.SkinID = table.Rows[i]["owner"].ToString();
                    btn.Click += new EventHandler(btnReject_Click);
                    cell.Controls.Add(btn);
                    row.Controls.Add(cell);
                    tableFriendRequest.Controls.Add(row);
                }
                else c++;
            }
            if (c == table.Rows.Count) tableFriendRequest.Visible = false;
        }
        else tableFriendRequest.Visible = false;
        //init friend list
        table = new ClassProcess().getFriendList(Session["user"].ToString(), true);
        if (table != null && table.Rows.Count != 0)
        {
            for (int i = 0; i < table.Rows.Count; i++)
            {
                row = new TableRow();
                cell = new TableCell();
                // add username to tablecell
                HyperLink lnk = new HyperLink();
                lnk.Text = table.Rows[i]["friend"].ToString();
                lnk.NavigateUrl = "~/Web page/FriendPix.aspx?username=" + table.Rows[i]["friend"].ToString();
                lnk.Font.Underline = false;
                cell.Controls.Add(lnk);
                cell.Wrap = true;
                // add cell to row
                row.Controls.Add(cell);


                // add blacklist button to cell
                cell = new TableCell();
                LinkButton btn = new LinkButton();
                btn.Text = "Blacklist";
                btn.Click += new EventHandler(btn_Click);
                btn.SkinID = table.Rows[i]["friend"].ToString();
                btn.CssClass = "login";
                // check if this is the last time user can log in
                if (int.Parse(new ClassProcess().getOutstanding(Session["user"].ToString())) >= 2)
                {
                    btn.Enabled = false;
                }
                cell.Controls.Add(btn);
                // add cell to row
                row.Controls.Add(cell);

                // add Remove button to cell
                cell = new TableCell();
                btn = new LinkButton();
                btn.Text = "Remove";
                btn.Click += new EventHandler(btn_Click);
                btn.SkinID = table.Rows[i]["friend"].ToString();
                btn.CssClass = "login";
                // check if this is the last time user can log in
                if (int.Parse(new ClassProcess().getOutstanding(Session["user"].ToString())) >= 2)
                {
                    btn.Enabled = false;
                }
                cell.Controls.Add(btn);
                // add cell to row
                row.Controls.Add(cell);

                // add send message button to cell
                cell = new TableCell();
                lnk = new HyperLink();
                lnk.Text = "Send message";
                lnk.NavigateUrl = "~/Web page/MessageSend.aspx?username=" + table.Rows[i]["friend"].ToString();
                lnk.CssClass = "login";
                // check if this is the last time user can log in
                if (int.Parse(new ClassProcess().getOutstanding(Session["user"].ToString())) >= 2)
                {
                    btn.Enabled = false;
                }
                cell.Controls.Add(lnk);
                // add cell to row
                row.Controls.Add(cell);
                //add row to table
                tableFriend.Controls.Add(row);
            }
        }
        else
        {
            row = new TableRow();
            cell = new TableCell();
            cell.Text = "There is noone in Friend list yet";
            row.Controls.Add(cell);
            tableFriend.Controls.Add(row);
        }

        //init black list
        table = new ClassProcess().getFriendList(Session["user"].ToString(), false);
        if (table != null && table.Rows.Count != 0)
        {
            for (int i = 0; i < table.Rows.Count; i++)
            {
                row = new TableRow();
                cell = new TableCell();

                // add username to tablecell
                HyperLink lnk = new HyperLink();
                lnk.Text = table.Rows[i]["friend"].ToString();
                lnk.NavigateUrl = "~/Web page/FriendPix.aspx?username=" + lnk.Text;
                cell.Controls.Add(lnk);
                // add cell to row
                row.Controls.Add(cell);

                // add turn to friend button to cell
                cell = new TableCell();
                LinkButton btn = new LinkButton();
                btn.Text = "To friend";
                btn.Click += new EventHandler(btn_Click);
                btn.SkinID = table.Rows[i]["friend"].ToString();
                btn.CssClass = "login";
                // check if this is the last time user can log in
                if (int.Parse(new ClassProcess().getOutstanding(Session["user"].ToString())) >= 2)
                {
                    btn.Enabled = false;
                }
                cell.Controls.Add(btn);
                // add cell to row
                row.Controls.Add(cell);

                // add Remove button to cell
                cell = new TableCell();
                btn = new LinkButton();
                btn.Text = "Remove";
                btn.Click += new EventHandler(btnRemove_Click);
                btn.SkinID = table.Rows[i]["friend"].ToString();
                btn.CssClass = "login";
                // check if this is the last time user can log in
                if (int.Parse(new ClassProcess().getOutstanding(Session["user"].ToString())) >= 2)
                {
                    btn.Enabled = false;
                }
                cell.Controls.Add(btn);
                // add cell to row
                row.Controls.Add(cell);

                tableBlackList.Controls.Add(row);
            }
        }
        else
        {
            row = new TableRow();
            cell = new TableCell();
            cell.Text = "There is noone in Blacklist yet";
            row.Controls.Add(cell);
            tableBlackList.Controls.Add(row);
        }
    }

    protected void btn_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        if (btn.Text.Equals("Blacklist"))
            new ClassProcess().updateFriendStatus(Session["user"].ToString(), btn.SkinID, false);
        else new ClassProcess().updateFriendStatus(Session["user"].ToString(), btn.SkinID, true);
        Response.Redirect("~/Web page/FriendManage.aspx");
        
    }

    protected void btnRemove_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        new ClassProcess().deleteRow("Relation", "owner", "friend", Session["user"].ToString(), btn.SkinID);
        Response.Redirect("~/Web page/FriendManage.aspx");
    }

    protected void btnAccept_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        new ClassProcess().insertFriend(Session["user"].ToString(), btn.SkinID, true);
        Response.Redirect("~/Web page/FriendManage.aspx");
    }

    protected void btnReject_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        new ClassProcess().deleteRow("relation", "owner", "friend", btn.SkinID, Session["user"].ToString());
        Response.Redirect("~/Web page/FriendManage.aspx");
    }
}