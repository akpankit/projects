﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class PixView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ClassProcess cProcess = new ClassProcess();
        if (Request["pixid"] == null) Response.Redirect("~/Web page/PixManage.aspx");
        //init title
        txtTitle.Text = cProcess.getInfo("pix", "title", "pixid", Request["pixid"]);
        //init place
        txtPlace.Text = cProcess.getInfo("pix", "place", "pixid", Request["pixid"]);
        //init the drop down list
        dropType.DataSource = null;
        dropType.DataSource = new ClassProcess().getPixType();
        dropType.DataTextField = "name";
        dropType.DataBind();

        dropType.SelectedIndex = int.Parse(cProcess.getInfo("pix", "type", "pixid", Request["pixid"]));
        if (Request["username"] == null)
        {
            txtTitle.Enabled = true;
            txtPlace.Enabled = true;
            dropType.Enabled = true;
            btnUpdate.Visible = true;
        }
        else 
        {
            txtTitle.Enabled = false;
            txtPlace.Enabled = false;
            dropType.Enabled = false;
            btnUpdate.Visible = false;
        }
        // check if the current user has logged in to view the comment box
        if (Session.Count == 0)
        {
            changeViewStatus(false);
            // set visit by visitor by one
            cProcess.updateVisit("Pix", Request["pixid"]);            
        }
        else changeViewStatus(true);
        
        // check if the owner of the current image is already friend to view add friend button
        if (Request["username"] != null && Session.Count != 0)
        {
            if (cProcess.getInfo("Relation", "welcome", "owner", "friend", Session["user"].ToString(), Request["username"]).Equals(""))
            {
                // set visit by visitor by one
                cProcess.updateVisit("Pix", Request["pixid"]);
            }
            else
            {
                // check if the visit to the current image is not existent
                if (cProcess.getInfo("Visit", "visit", "pixid", "username", Request["pixid"], Session["user"].ToString()).Equals(""))
                    // add visit
                    cProcess.insertVisit(Session["user"].ToString(), Request["pixid"]);
                // set the visit by one
                else cProcess.updateVisit("Visit", Request["pixid"], Session["user"].ToString());
            }
            // check if the current image belongs to current user to view add friend button
            if (Request["username"].Equals(Session["user"].ToString()))
            {
                // check if the visit to the current image is not existent
                if (cProcess.getInfo("Visit", "visit", "pixid", "username", Request["pixid"], Session["user"].ToString()).Equals(""))
                    // add visit
                    cProcess.insertVisit(Session["user"].ToString(), Request["pixid"]);
                // set the visit by one
                else cProcess.updateInfo("Visit", "visit", "pixid", "username", "visit+1", Request["pixid"], Session["user"].ToString());                
            }
        }
        string owner = cProcess.getInfo("Pix", "owner", "pixid", Request["pixid"]);
        if (Session.Count != 0)
        {
            // check if this is the last time user can log in
            if (int.Parse(cProcess.getOutstanding(Session["user"].ToString())) >= 2)
            {
                changeViewStatus(false);
            }
        }
        
        // init average ratings
        double rate = 0;
        if (!cProcess.getSumOfRate(Request["pixid"]).Equals(""))
            rate = double.Parse(cProcess.getSumOfRate(Request["pixid"])) /
                double.Parse(cProcess.getCountOfRate(Request["pixid"]));
        lblRate.Text = rate.ToString("0.00");
        lblDownload.Text = cProcess.getInfo("Pix", "download", "pixid", Request["pixid"]);
        if (lblDownload.Text.Equals("")) lblDownload.Text = "0";
        
        // init owner visit        
        lblOwner.Text = cProcess.getInfo("Visit", "visit", "username", owner);
        if (lblOwner.Text.Equals("")) lblOwner.Text = "0";
        
        // init OU visit
        lblOU.Text = cProcess.getInfo("Visit", "visit", "pixid", Request["pixid"]);
        if (lblOU.Text.Equals("")) lblOU.Text = "0";
        
        // init VI visit
        lblVI.Text = cProcess.getInfo("Pix", "visit", "pixid", Request["pixid"]);
        if (lblVI.Text.Equals("")) lblVI.Text = "0";
        lblTotal.Text = (int.Parse(lblOwner.Text) + int.Parse(lblOU.Text) + int.Parse(lblVI.Text)).ToString();
        
        // init image view
        string imagePath = cProcess.getPixPath(Request["pixid"]);
        Image image = new Image();
        image.ImageUrl = imagePath;
        image.Width = 150;
        image.Height = 150;
        // add image to the cell
        cell.Controls.Add(image);

        //init histogram
        Histogram hist = new Histogram();
        hist.setImage(new System.Drawing.Bitmap(MapPath(imagePath)));
        img.AlternateText = MapPath(imagePath);
        hist.calcHisto(img);
        

        // view the comment of this image
        DataTable tableComment = cProcess.getCommentList(Request["pixid"]);
        if (tableComment != null && tableComment.Rows.Count != 0)
        {
            for (int i = 0; i < tableComment.Rows.Count; i++)
            {
                TableRow tmpRow = new TableRow();
                TableCell tmpCell = new TableCell();
                // add username to comment
                LinkButton btn = new LinkButton();
                btn.Text = tableComment.Rows[i]["username"].ToString();
                btn.Font.Underline = false;
                btn.Click += new EventHandler(btn_Click);                
                tmpCell.Controls.Add(btn);

                // add date to comment
                Label lbl = new Label();
                lbl.Text = " " + tableComment.Rows[i]["post"].ToString() + " : ";
                tmpCell.Controls.Add(lbl);
                tmpCell.Wrap = true;

                // add comment
                lbl = new Label();
                lbl.Text = tableComment.Rows[i]["comment"].ToString();
                tmpCell.Controls.Add(lbl);
                tmpRow.Controls.Add(tmpCell);
                tableImage.Controls.AddAt(4, tmpRow);
            }
        }
    }

    private long[] GetHistogram(System.Drawing.Bitmap picture)
    {
        long[] myHistogram = new long[256];

        for (int i = 0; i < picture.Size.Width; i++)
            for (int j = 0; j < picture.Size.Height; j++)
            {
                System.Drawing.Color c = picture.GetPixel(i, j);

                long Temp = 0;
                Temp += c.R;
                Temp += c.G;
                Temp += c.B;

                Temp = (int)Temp / 3;
                myHistogram[Temp]++;
            }

        return myHistogram;
    }

    private void changeViewStatus(bool state)
    {
        lblComment.Visible = state;
        txtComment.Visible = state;
        btnPost.Visible = state;
        lnk1.Visible = state;
        lnk2.Visible = state;
        lnk3.Visible = state;
        lnk4.Visible = state;
        lnk5.Visible = state;
    }

    protected void lnkRate_Click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        if(!new ClassProcess().getInfo("Rate", "rate", "username", "pixid", Session["user"].ToString(), Request["pixid"]).Equals(""))
            new ClassProcess().updateInfo("Rate", "rate", "username", "pixid", lnk.Text, Session["user"].ToString(), Request["pixid"]);
        else new ClassProcess().insertRate(Session["user"].ToString(), Request["pixid"], lnk.Text);
        if (Request["username"] == null)
            Response.Redirect("~/Web page/PixView.aspx?pixid=" + Request["pixid"]);
        else Response.Redirect("~/Web page/PixView.aspx?pixid=" + Request["pixid"] + "&username=" + Request["username"]);
    }

    protected void btnPost_Click(object sender, EventArgs e)
    {
        if (txtComment.Text.Trim().Equals(""))
        {
            txtComment.Focus();
            return;
        }
        new ClassProcess().insertComment(Session["user"].ToString(), Request["pixid"], txtComment.Text);
        if (Request["username"] == null)
            Response.Redirect("~/Web page/PixView.aspx?pixid=" + Request["pixid"]);
        else Response.Redirect("~/Web page/PixView.aspx?pixid=" + Request["pixid"] + "&username=" + Request["username"]);
    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        new ClassProcess().insertFriend(Session["user"].ToString(), Request["username"], true);
        lblError.Text = "You have added this user as friend";
        lblError.Visible = true;
    }

    protected void btn_Click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        if(!Session["user"].ToString().Equals(lnk.Text))
            Response.Redirect("~/Web page/FriendPix.aspx?username=" + lnk.Text);
        Response.Redirect("~/Web page/UserInfo.aspx");
    }

    protected void lnkSend_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Web page/MessageSend.aspx?username=" + Request["username"]);
    }

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        if (txtPath.Text.Trim().Equals(""))
        {
            lblError.Text = "Input file path to save on your computer";
            txtPath.Focus();
            return;
        }
        System.Net.WebClient wClient = new System.Net.WebClient();
        ClassProcess cProcess = new ClassProcess();
        int download = int.Parse(cProcess.getInfo("Pix", "download", "pixid", Request["pixid"]));
        string url = HttpContext.Current.Request.Url.AbsoluteUri;
        url = url.Substring(0, url.IndexOf("Web%20page"));
        wClient.DownloadFile(url + cProcess.getInfo("Pix", "path", "pixid", Request["pixid"]).Substring(2), txtPath.Text);
        new ClassProcess().updateInfo("Pix", "download", "pixid", (download + 1).ToString(), Request["pixid"]);
        lblError.Text = "Downloaded file successfully";
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        new ClassProcess().updateInfo("pix", "title", "pixid", txtTitle.Text, Request["pixid"]);
        new ClassProcess().updateInfo("pix", "place", "pixid", txtPlace.Text, Request["pixid"]);
        new ClassProcess().updateInfo("pix", "type", "pixid", dropType.SelectedIndex.ToString(), Request["pixid"]);
    }

    protected void lnkComplain_Click(object sender, EventArgs e)
    {
        new ClassProcess().insertMessage(Session["user"].ToString(), "admin", "Complain Report: " + Request["username"]);        
        lblError.Text = "You have just sent complain report to administrator";
    }
}