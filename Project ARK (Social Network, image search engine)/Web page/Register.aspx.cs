﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

public partial class Default2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public bool checkEmailInvalid(TextBox email)
    {
        //define the valid format
        string reg = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
        Regex emailRegex = new Regex(reg);
        Match m = emailRegex.Match(email.Text);
        //verify if the email is not in valid format
        if (!m.Success)
        {
            lblError.Text = ("Email is invalid");
            email.Focus();
            return true;
        }
        return false;
    }

    protected void btnRegister_Click(object sender, EventArgs e)
    {
        //verify if the username is empty
        if (txtUsername.Text.Trim().Equals(""))
        {
            lblError.Text = "input username";
            txtUsername.Focus();
            return;
        }
        //verify if the password is empty
        if (txtPassword.Text.Trim().Equals(""))
        {
            lblError.Text = "input password";
            txtPassword.Focus();
            return;
        }
        //verify if the confirm password is empty
        if (txtConfirm.Text.Trim().Equals(""))
        {
            lblError.Text = "input confirm password";
            txtConfirm.Focus();
            return;
        }
        //verify if the password is different from the confirm password
        if (!txtPassword.Text.Equals(txtConfirm.Text))
        {
            lblError.Text = "password is different from the confirm password";
            txtPassword.Focus();
            return;
        }
        //verify if the fullname is empty
        if (txtFullname.Text.Trim().Equals(""))
        {
            lblError.Text = "input your fullname";
            txtFullname.Focus();
            return;
        }
        //verify if the email is not empty
        if (!txtEmail.Text.Trim().Equals(""))
        {
            //verify if the email is in valid format
            if (checkEmailInvalid(txtEmail)) return;
        }
        if (!txtPhone.Text.Trim().Equals(""))
        {
            long check;
            if (!long.TryParse(txtPhone.Text.Trim(), out check))
            {
                lblError.Text = "the phone number must be numeric";
                txtPhone.Focus();
                return;
            }
        }
        if (!new ClassProcess().userExisted(txtUsername.Text, txtPassword.Text).Equals(""))
        {
            lblError.Text = "this username already existed";
        }
        //verify if the file to upload is empty
        if (fu1.FileName.Equals(""))
        {
            lblError.Text = "choose image to upload";
            return;
        }
        string pass = txtPassword.Text;        
        //announce on the web page
        System.Data.DataTable tab = new ClassProcess().getMatchedUserList(txtPassword.Text);
        string realPath = MapPath("~/Upload Images");
        System.IO.Directory.CreateDirectory(realPath + "/" + txtUsername.Text);
        string extFile = fu1.FileName.Split('.')[fu1.FileName.Split('.').Length - 1];
        string name = new ClassProcess().getMaxIndexOfName("Pix", "name");
        fu1.PostedFile.SaveAs(realPath + "/" + txtUsername.Text + "/" + name + "." + extFile);
        if (tab != null && tab.Rows.Count != 0)
        {
            string[] ext = new string[] { "a4e", "4er", "era", "ra3", "a3s", "3so", "so9", };
            Random r = new Random();
            do
            {
                int index = r.Next(7);

                System.Data.DataTable tabTmp = new ClassProcess().getMatchedUserList(txtPassword.Text + ext[index]);
                if (tabTmp == null || tabTmp.Rows.Count == 0)
                {
                    pass += ext[index];
                    new ClassProcess().insertUser(txtUsername.Text, pass, txtFullname.Text, txtEmail.Text, txtPhone.Text, "~/Upload Images/" + txtUsername.Text + "/" + name + "." + extFile);
                    new ClassProcess().updateInfo("user", "ext", "username", ext[index], txtUsername.Text);
                    break;
                }
            } while (true);
        }
        else
            //insert into db
            new ClassProcess().insertUser(txtUsername.Text, txtPassword.Text, txtFullname.Text, txtEmail.Text, txtPhone.Text, "~/Upload Images/" + txtUsername.Text + "/" + name + "." + extFile);
        lblError.Text = "Register finished. Please wait till the administrator accept your request";
    }
}