﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web page/Mpage1.master" AutoEventWireup="true" CodeFile="Profile.aspx.cs" Inherits="Web_page_Profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="up" Runat="Server">
<div id="menuContent">
    <ul>
        <li><asp:HyperLink ID="HyperLink4" Text="My image" runat="server" NavigateUrl="~/Web page/PixManage.aspx"><span>My image</span></asp:HyperLink></li>
        <li><div class="blank"></div></li>
        <li><asp:HyperLink ID="HyperLink5" Text="My Friend" runat="server" NavigateUrl="~/Web page/FriendManage.aspx"><span>My Friend</span></asp:HyperLink></li>
        <li><div class="blank"></div></li>
        <li><asp:HyperLink ID="HyperLink6" Text="Message" runat="server" NavigateUrl="~/Web page/MessageManage.aspx"><span>Message</span></asp:HyperLink></li>
        <li><div class="blank"></div></li>
        <li><asp:HyperLink ID="lnkUser" Visible="false" Text="Manage user" runat="server" NavigateUrl="~/Web page/UserManage.aspx"><span>Manage</span></asp:HyperLink></li>        
    </ul>
</div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" Runat="Server">
<div id="contentPanel">
    <asp:Table GridLines="Both" BorderStyle="Inset" BorderColor="#497f35" runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <center><asp:Image ID="imgProfile" Height="180" Width="180" runat="server" /></center><br />
                Blacklist:<br />
                <asp:Table ID="tableBlackList" runat="server">
                </asp:Table>
                Warnings:<asp:Hyperlink ID="lnkWarn" Font-Underline="false" runat="server" NavigateUrl="~/Web page/MessageManage.aspx" ></asp:Hyperlink><br />                
            </asp:TableCell>
            <asp:TableCell VerticalAlign="Middle">
                <asp:Table ID="tableImage" runat="server">
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</div>
</asp:Content>

