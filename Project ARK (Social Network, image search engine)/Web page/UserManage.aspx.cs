﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Web_page_UserManage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session.Count == 0) Response.Redirect("~/Web page/Default.aspx");
        //init accepted user
        DataTable table = new ClassProcess().getRegisteredUserList();
        TableRow row;
        TableCell cell;
        if (table != null && table.Rows.Count != 0)
        {
            row = new TableRow();
            //init header
            cell = new TableCell();
            //init order header
            cell.Text = "Order";
            cell.Font.Bold = true;
            row.Controls.Add(cell);

            //init username header
            cell = new TableCell();
            cell.Text = "Username";
            cell.Font.Bold = true;
            row.Controls.Add(cell);

            // init fullname header
            cell = new TableCell();
            cell.Text = "Fullname";
            cell.Font.Bold = true;
            row.Controls.Add(cell);

            // init contact header
            cell = new TableCell();
            cell.Text = "Contact";
            cell.Font.Bold = true;
            row.Controls.Add(cell);
            row.HorizontalAlign = HorizontalAlign.Center;
            //add row to table
            tableRequest.Controls.Add(row);

            for (int i = 0; i < table.Rows.Count; i++)
            {
                row = new TableRow();
                //init cell                
                string username = table.Rows[i]["username"].ToString();
                string contact = table.Rows[i]["contact"].ToString();
                string fullname = table.Rows[i]["fullname"].ToString();

                // init order
                cell = new TableCell();
                cell.Text = (i + 1).ToString();
                cell.HorizontalAlign = HorizontalAlign.Center;
                row.Controls.Add(cell);

                //init username
                cell = new TableCell();
                cell.Text = username;
                row.Controls.Add(cell);

                //init fullname
                cell = new TableCell();
                cell.Text = fullname;
                row.Controls.Add(cell);

                //init contact
                cell = new TableCell();
                cell.Text = contact;
                row.Controls.Add(cell);

                //init accept button
                cell = new TableCell();
                LinkButton lnk = new LinkButton();
                lnk.Text = "Accept";
                lnk.CssClass = "login";
                lnk.SkinID = username;
                lnk.Click += new EventHandler(lnk_Click);
                cell.Controls.Add(lnk);
                row.Controls.Add(cell);
                // add row to table
                tableRequest.Controls.Add(row);
            }
        }
        else
        {
            row = new TableRow();
            cell = new TableCell();
            cell.Text = "There is no registered request";
            row.Controls.Add(cell);
            tableRequest.Controls.Add(row);
        }
        
        //init list of user in system
        table = new ClassProcess().getUser("%");
        if (table != null && table.Rows.Count != 0)
        {
            // init header
            row = new TableRow();
            cell = new TableCell();
            cell.Text = "Username";
            cell.Font.Bold = true;
            row.Controls.Add(cell);

            cell = new TableCell();
            cell.Text = "Password";
            cell.Font.Bold = true;
            row.Controls.Add(cell);

            cell = new TableCell();
            cell.Text = "Fullname";
            cell.Font.Bold = true;
            row.Controls.Add(cell);

            cell = new TableCell();
            cell.Text = "Contact";
            cell.Font.Bold = true;
            row.Controls.Add(cell);

            cell = new TableCell();
            cell.Text = "Phone";
            cell.Font.Bold = true;
            row.Controls.Add(cell);

            cell = new TableCell();
            cell.Text = "Type";
            cell.Font.Bold = true;
            row.Controls.Add(cell);
            row.HorizontalAlign = HorizontalAlign.Center;
            tableUser.Controls.Add(row);

            for (int i = 0; i < table.Rows.Count; i++)
            {
                //init username
                row = new TableRow();
                cell = new TableCell();
                HyperLink lnk = new HyperLink();
                lnk.Text = table.Rows[i]["username"].ToString();
                lnk.Font.Underline = false;
                lnk.NavigateUrl = "~/Web page/FriendPix.aspx?username=" + table.Rows[i]["username"].ToString();
                cell.Controls.Add(lnk);
                row.Controls.Add(cell);

                //init password
                cell = new TableCell();
                cell.Text = table.Rows[i]["password"].ToString();
                row.Controls.Add(cell);

                //init fullname
                cell = new TableCell();
                cell.Text = table.Rows[i]["fullname"].ToString();
                row.Controls.Add(cell);
                row.VerticalAlign = VerticalAlign.Middle;

                //init contact
                cell = new TableCell();
                cell.Text = table.Rows[i]["contact"].ToString();
                row.Controls.Add(cell);
                row.VerticalAlign = VerticalAlign.Middle;

                //init phone
                cell = new TableCell();
                cell.Text = table.Rows[i]["phone"].ToString();
                row.Controls.Add(cell);
                row.VerticalAlign = VerticalAlign.Middle;
                row.Controls.Add(cell);

                //init type
                cell = new TableCell();
                if(table.Rows[i]["phone"].ToString().Equals("1"))
                    cell.Text = "visitor";
                else cell.Text = "ordinary user";
                row.Controls.Add(cell);
                row.VerticalAlign = VerticalAlign.Middle;
                row.Controls.Add(cell);
                //add row to table
                tableUser.Controls.Add(row);
            }
        }
        else
        {
            row = new TableRow();
            cell = new TableCell();
            cell.Text = "There is no user yet";
            row.Controls.Add(cell);
            tableUser.Controls.Add(row);
        }
    }

    protected void lnk_Click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        if(!new ClassProcess().getInfo("user","inuse", "username", lnk.SkinID).Equals("1"))
            new ClassProcess().updateInfo("User", "type", "username", "2", lnk.SkinID);
        Response.Redirect("~/Web page/UserManage.aspx");
    }

    protected void btnReject_Click(object sender, EventArgs e)
    {
        LinkButton lnk = (LinkButton)sender;
        new ClassProcess().updateInfo("message", "reject", "id", "1", lnk.SkinID);
        Response.Redirect("~/Web page/UserManage.aspx");
    }
}