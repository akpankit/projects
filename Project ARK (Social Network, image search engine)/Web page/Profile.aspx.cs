﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Web_page_Profile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session.Count == 0) Response.Redirect("~/Web page/Default.aspx");
        if (new ClassProcess().isSU(Session["user"].ToString()))
        {
            lnkUser.Visible = true;
        }
        //init profile image
        imgProfile.ImageUrl = new ClassProcess().getInfo("user", "profile", "username", Session["user"].ToString());

        //init warnning message
        DataTable table = new ClassProcess().getMessageList("receiver", Session["user"].ToString());
        if (table != null && table.Rows.Count != 0)
        {
            lnkWarn.Text = table.Rows.Count.ToString();
        }
        else lnkWarn.Text = "0";

        //init black list
        table = new ClassProcess().getFriendList(Session["user"].ToString(), false);
        if (table != null && table.Rows.Count != 0)
        {
            TableRow row = new TableRow();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                if (i % 3 == 0)
                {
                    row = new TableRow();
                }
                TableCell cell = new TableCell();

                Image img = new Image();
                img.ImageUrl = new ClassProcess().getInfo("user", "profile", "username", table.Rows[i]["friend"].ToString());
                img.Height = img.Width = 40;
                // add username to tablecell
                HyperLink lnk = new HyperLink();
                lnk.Text = table.Rows[i]["friend"].ToString();
                lnk.NavigateUrl = "~/Web page/FriendPix.aspx?username=" + lnk.Text;
                lnk.Controls.Add(img);
                cell.Controls.Add(lnk);
                // add cell to row
                row.Controls.Add(cell);
                if (i % 3 == 0)
                {
                    tableBlackList.Controls.Add(row);
                }
            }
            if (table.Rows.Count % 3 != 0)
            {
                tableBlackList.Controls.Add(row);
            }
        }
        else
        {
            TableRow row = new TableRow();
            TableCell cell = new TableCell();
            cell.Text = "There is noone in Blacklist yet";
            row.Controls.Add(cell);
            tableBlackList.Controls.Add(row);
        }



        //init table image
        initDisplay(1);
        if (Request["pid"] != null)
        {
            //load the image            
            initDisplay(int.Parse(Request["pid"]));
        }
    }

    private void initDisplay(int page)
    {
        int pagesize = 10;
        //clear all element in table
        tableImage.Controls.Clear();
        DataTable table = new ClassProcess().getImageList(Session["user"].ToString());
        if (table != null && table.Rows.Count != 0)
        {
            //init row and cell for the header
            TableRow row1 = new TableRow();
            TableCell cell = new TableCell();
            int start = pagesize * (page - 1);
            int end = pagesize * page;
            if (end > table.Rows.Count || end == 0 || table.Rows.Count < pagesize) end = table.Rows.Count;
            for (int i = start; i < end; i++)
            {
                if (i % 5 == 0)
                {
                    row1 = new TableRow();
                }
                //init cell
                cell = new TableCell();
                //init image
                Image img = new Image();
                img.AlternateText = table.Rows[i]["pixid"].ToString();
                img.ImageUrl = table.Rows[i]["path"].ToString();
                img.Height = img.Width = 150;
                //init link of image
                HyperLink lnk = new HyperLink();
                lnk.NavigateUrl = "~/Web page/PixView.aspx?pixid=" + img.AlternateText;
                string owner = new ClassProcess().getInfo("Pix", "owner", "pixid", img.AlternateText);
                lnk.Controls.Add(img);
                cell.Controls.Add(lnk);
                row1.Width = 90;
                row1.HorizontalAlign = HorizontalAlign.Center;
                row1.Controls.Add(cell);

                if (i % 5 == 0)
                {
                    tableImage.Controls.Add(row1);
                }
            }
            if (table.Rows.Count % 5 != 0)
            {
                tableImage.Controls.Add(row1);
            }
            row1 = new TableRow();
            cell = new TableCell();
            cell.ColumnSpan = 5;
            if (table.Rows.Count / pagesize > 0)
            {
                int num = 0;
                if (table.Rows.Count / pagesize == 1 && table.Rows.Count % pagesize != 0) num = 2;
                else num = table.Rows.Count / pagesize + 1;
                for (int i = 0; i < num; i++)
                {
                    //init page number link
                    HyperLink btn = new HyperLink();
                    btn.Text = (i + 1).ToString() + " ";
                    btn.NavigateUrl = "~/Web page/Profile.aspx?pid=" + (i + 1).ToString();
                    btn.Font.Underline = false;
                    cell.Controls.Add(btn);
                    row1.Controls.Add(cell);
                    row1.HorizontalAlign = HorizontalAlign.Right;
                }
            }
            tableImage.Controls.Add(row1);
        }
        else
        {
            TableRow row = new TableRow();
            TableCell cell = new TableCell();
            cell.Text = "There is no image yet";
            row.Controls.Add(cell);
            tableImage.Controls.Add(row);
        }
    }
}