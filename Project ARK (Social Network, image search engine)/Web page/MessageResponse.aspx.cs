﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Web_page_MessageResponse : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session.Count == 0) Response.Redirect("~/Web page/Default.aspx");
        ClassProcess cProcess = new ClassProcess();
        lblUsername.Text = cProcess.getInfo("Message", "sender", "sender", "receiver", Request["sender"], Session["user"].ToString()) + " ";
        lblContent.Text = " " + cProcess.getInfo("Message", "messagecontent", "sender", "receiver", Request["sender"], Session["user"].ToString());
    }

    protected void btnResponse_Click(object sender, EventArgs e)
    {
        if (txtResponse.Text.Trim().Equals(""))
        {
            lblError.Text = "input content to response";
            txtResponse.Focus();
            return;
        }
        new ClassProcess().updateInfo("Message", "responsecontent", "sender", "receiver", txtResponse.Text, Request["sender"], 
            Session["user"].ToString());
        Response.Redirect("~/Web page/MessageManage.aspx");
    }
}