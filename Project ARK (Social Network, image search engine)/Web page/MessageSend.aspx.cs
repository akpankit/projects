﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Web_page_MessageSend : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session.Count == 0) Response.Redirect("~/Web page/Default.aspx");
        lblUsername.Text = Request["username"];
    }

    protected void btnSend_Click(object sender, EventArgs e)
    {
        if (txtContent.Text.Trim().Equals(""))
        {
            lblError.Text = "input content to send";
            txtContent.Focus();
            return;
        }
        new ClassProcess().insertMessage(Session["user"].ToString(), Request["username"], txtContent.Text);
        if (new ClassProcess().isSU(Session["user"].ToString()))
        {
            new ClassProcess().updateInfo("message", "outstanding", "sender", "receiver", "1", Session["user"].ToString(), Request["username"]);
            Response.Redirect("~/Web page/UserManage.aspx");
        }
        Response.Redirect("~/Web page/MessageManage.aspx");
    }
}