﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;

public partial class PixManage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //verify if the current user has logged in
        if (Session.Count == 0) Response.Redirect("~/Web page/Default.aspx");
        if (new ClassProcess().isSU(Session["user"].ToString()))
        {
            lnkUser.Visible = true;
        }
        //init the drop down list
        dropType.DataSource = null;
        dropType.DataSource = new ClassProcess().getPixType();
        dropType.DataTextField = "name";
        dropType.DataBind();

        //load the image
        DataTable tabImage = new ClassProcess().getImageList(Session["user"].ToString());
        if (tabImage != null && tabImage.Rows.Count != 0)
        {
            TableRow row1 = new TableRow();
            TableRow row2 = new TableRow();
            for (int j = 0; j < tabImage.Rows.Count; j++)
            {
                if (j % 5 == 0)
                {
                    row1 = new TableRow();
                    row2 = new TableRow();
                }
                TableCell cell = new TableCell();
                //init image
                Image image = new Image();
                string imagePath = tabImage.Rows[j]["path"].ToString();
                image.ImageUrl = imagePath;
                image.Width = 90;
                image.Height = 90;
                image.ToolTip = tabImage.Rows[j]["title"].ToString();                
                //init link of image
                HyperLink lnk = new HyperLink();
                lnk.Controls.Add(image);
                lnk.NavigateUrl = "~/Web page/PixView.aspx?pixid=" + tabImage.Rows[j]["pixid"].ToString() + "&username=" + Session["user"].ToString();
                cell.Controls.Add(lnk);
                row1.Controls.Add(cell);

                cell = new TableCell();
                LinkButton btn = new LinkButton();
                btn.CssClass = "login";
                btn.Text = "Remove";
                btn.ID = tabImage.Rows[j]["pixid"].ToString();
                btn.Click += new EventHandler(btn_Click);
                cell.Controls.Add(btn);
                cell.HorizontalAlign = HorizontalAlign.Center;
                row2.Controls.Add(cell);
                if (j % 5 == 4)
                {
                    tableImage.Controls.Add(row1);
                    tableImage.Controls.Add(row2);
                }
            }
            if (tabImage.Rows.Count % 5 != 0)
            {
                tableImage.Controls.Add(row1);
                tableImage.Controls.Add(row2);
            }
        }
    }

    protected void lnkUpload_Click(object sender, EventArgs e)
    {
        string realPath = MapPath("~/Upload Images");
        //verify if the title is empty
        if (txtTitle.Text.Trim().Equals(""))
        {
            lblError.Text = "input title";
            txtTitle.Focus();
            return;
        }
        //verify if the place is empty
        if (txtPlace.Text.Trim().Equals(""))
        {
            lblError.Text = "input place";
            txtPlace.Focus();
            return;
        }
        //verify if the annotation is empty
        if (txtAnnotation.Text.Trim().Equals(""))
        {
            lblError.Text = "input annotation";
            txtAnnotation.Focus();
            return;
        }
        //verify if the file to upload is empty
        if (fu1.FileName.Equals(""))
        {
            lblError.Text = "choose image to upload";
            return;
        }
        Directory.CreateDirectory(realPath + "/" + Session["user"].ToString());
        string ext = fu1.FileName.Split('.')[fu1.FileName.Split('.').Length - 1];
        string name = new ClassProcess().getMaxIndexOfName("Pix", "name");
        fu1.PostedFile.SaveAs(realPath + "/" + Session["user"].ToString() + "/" + name + "." + ext);
        new ClassProcess().insertPix(txtTitle.Text, txtPlace.Text, calTime.SelectedDate.ToString(), txtAnnotation.Text, (dropType.SelectedIndex + 1).ToString(), Session["user"].ToString(),
            "~/Upload Images/" + Session["user"].ToString() + "/" + name + "." + ext);
        Response.Redirect("~/Web page/PixManage.aspx");
    }

    protected void btn_Click(object sender, EventArgs e)
    {
        LinkButton btn = (LinkButton)sender;
        ClassProcess cProcess = new ClassProcess();
        cProcess.deleteRow("Visit", "pixid", btn.ID);
        cProcess.deleteRow("Rate", "username", btn.ID);
        cProcess.deleteRow("Comment", "username", btn.ID);
        cProcess.deleteRow("Pix", "pixid", btn.ID);
        Response.Redirect("~/Web page/PixManage.aspx");
    }
}