﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

public partial class UserInfo : System.Web.UI.Page
{
    ClassProcess cProcess = null; 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session.Count == 0) Response.Redirect("Default.aspx");
        if (IsPostBack) return;        
        cProcess = new ClassProcess();
        if (cProcess.isSU(Session["user"].ToString()))
        {
            lnkUser.Visible = true;
        }
        txtFullname.Text = Session["name"].ToString();
        txtEmail.Text = cProcess.getInfo("User", "contact", "username", Session["user"].ToString());
        txtPhone.Text = cProcess.getInfo("User", "phone", "username", Session["user"].ToString());
        // check if this is the last time user can log in
        if (int.Parse(cProcess.getOutstanding(Session["user"].ToString())) >= 2)
        {
            lnkChangePass.Enabled = false;
            lnkUpdateInfo.Enabled = false;
        }
    }

    public bool checkEmailInvalid(TextBox email)
    {
        //define the valid format
        string reg = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
        Regex emailRegex = new Regex(reg);
        Match m = emailRegex.Match(email.Text);
        //verify if the email is not in valid format
        if (!m.Success)
        {
            lblError.Text = ("Email is invalid");
            email.Focus();
            return true;
        }
        return false;
    }

    protected void lnkChangePass_Click(object sender, EventArgs e)
    {
        //verify if the old password is empty
        if (txtOldPass.Text.Trim().Equals(""))
        {
            lblError.Text = "input old password";
            txtOldPass.Focus();
            return;
        }
        // verify if the old password is different from that in db
        string pass = new ClassProcess().getInfo("user", "password", "username", Session["user"].ToString());
        if (!txtOldPass.Text.Trim().Equals(pass))
        {
            lblError.Text = "input invalid old password";
            txtOldPass.Focus();
            return;
        }
        //verify if the new password is empty
        if (txtNewPass.Text.Trim().Equals(""))
        {
            lblError.Text = "input old password";
            txtNewPass.Focus();
            return;
        }
        //verify if the confirm password is empty
        if (txtConfirmPass.Text.Trim().Equals(""))
        {
            lblError.Text = "input confirm password";
            txtConfirmPass.Focus();
            return;
        }
        //verify if the password is different from the confirm password
        if (!txtNewPass.Text.Equals(txtConfirmPass.Text))
        {
            lblError.Text = "password is different from the confirm password";
            txtNewPass.Focus();
            return;
        }
        //update password value in db
        cProcess = new ClassProcess();
        cProcess.updateUserInfo(Session["user"].ToString(), "password", txtNewPass.Text);
        lblError.Text = "Change password successful";
    }

    protected void lnkUpdateInfo_Click(object sender, EventArgs e)
    {
        //verify if the fullname is empty
        if (txtFullname.Text.Trim().Equals(""))
        {
            lblError.Text = "input your fullname";
            txtFullname.Focus();
            return;
        }
        //verify if the email is not empty
        if (!txtEmail.Text.Trim().Equals(""))
        {
            //verify if the email is in valid format
            if (checkEmailInvalid(txtEmail)) return;
        }
        if (!txtPhone.Text.Trim().Equals(""))
        {
            int check;
            if (!int.TryParse(txtPhone.Text.Trim(), out check))
            {
                lblError.Text = "the phone number must be numeric";
                txtPhone.Focus();
                return;
            }
        }
        cProcess = new ClassProcess();
        //update fullname value in db
        cProcess.updateUserInfo(Session["user"].ToString(), "fullname", txtFullname.Text);
        //update contact value in db
        cProcess.updateUserInfo(Session["user"].ToString(), "contact", txtEmail.Text);
        //update phone value in db
        cProcess.updateUserInfo(Session["user"].ToString(), "phone", txtPhone.Text);
        lblError.Text = "Update info successful";
    }
}