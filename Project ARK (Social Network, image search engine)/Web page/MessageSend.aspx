﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web page/Mpage1.master" AutoEventWireup="true" CodeFile="MessageSend.aspx.cs" Inherits="Web_page_MessageSend" %>

<asp:Content ID="Content1" ContentPlaceHolderID="up" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" Runat="Server">
<div id="contentPanel">
    <asp:Table ID="Table1" runat="server" HorizontalAlign="Center">        
        <asp:TableHeaderRow HorizontalAlign="Center">
            <asp:TableCell ColumnSpan="2"><h2>Send the warning message</h2></asp:TableCell>
        </asp:TableHeaderRow>
        <asp:TableRow>
            <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell Width="60px">Send to :</asp:TableCell>
            <asp:TableCell HorizontalAlign="Left"><asp:Label ID="lblUsername" runat="server"></asp:Label></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ColumnSpan="2">
                <asp:TextBox ID="txtContent" TextMode="MultiLine" Width="100%" Rows="3" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow> 
            <asp:TableCell></asp:TableCell>          
            <asp:TableCell HorizontalAlign="center">
                <asp:Button ID="btnSend" runat="server" CssClass="login" OnClick="btnSend_Click" Text="Send" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</div>
</asp:Content>

