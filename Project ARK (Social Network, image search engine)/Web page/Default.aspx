﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web page/Mpage2.master" AutoEventWireup="true" CodeFile="~/Web page/Default.aspx.cs" Inherits="_Default" %>
<asp:Content ID="Content2" ContentPlaceHolderID="up" Runat="Server">
<div id="searchPanel">
<center>
    <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>    
    <asp:DropDownList ID="dropSearch" AutoPostBack="true" runat="server" OnSelectedIndexChanged="dropSearch_SelectedIndexChanged">        
        <asp:ListItem Value="owner" Text="owner"></asp:ListItem>
        <asp:ListItem Value="popular" Text="popularity"></asp:ListItem>
        <asp:ListItem Value="pix" Text="pix info"></asp:ListItem>
    </asp:DropDownList>
    <asp:LinkButton ID="lnkSearch" CssClass="login" Text="Search" runat="server" OnClick="lnkSearch_Click"></asp:LinkButton>    
<br />
    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell>
                <asp:Image ID="img" Height="120" Width="120" runat="server" Visible="false" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right">
                <asp:FileUpload ID="fu" runat="server" Visible="false" />
                <asp:LinkButton ID="btn" runat="server" Visible="false" CssClass="login" Text="Upload" OnClick="btn_CLick"></asp:LinkButton>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</center>
</div>
<br />
<div id="contentPanel">
<table style="vertical-align:top">
    <tr>        
        <td>
            <asp:Label ID="lblResult" runat="server"></asp:Label>
            <asp:Table ID="tableImage" runat="server">
            </asp:Table>
        </td>
        <td>
            <asp:Table ID="tableNews" HorizontalAlign="Center" runat="server">
                <asp:TableHeaderRow>
                    <asp:TableCell><h2>News</h2></asp:TableCell>
                </asp:TableHeaderRow>
            </asp:Table>
        </td>
    </tr>
</table>
</div>
</asp:Content>

