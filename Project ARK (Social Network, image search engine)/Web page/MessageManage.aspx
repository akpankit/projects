﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web page/Mpage1.master" AutoEventWireup="true" CodeFile="~/Web page/MessageManage.aspx.cs" Inherits="Web_page_MessageManage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="up" Runat="Server">
<div id="menuContent">
    <ul>
        <li><asp:HyperLink ID="HyperLink4" Text="My image" runat="server" NavigateUrl="~/Web page/PixManage.aspx"><span>My image</span></asp:HyperLink></li>
        <li><div class="blank"></div></li>
        <li><asp:HyperLink ID="HyperLink5" Text="My Friend" runat="server" NavigateUrl="~/Web page/FriendManage.aspx"><span>My Friend</span></asp:HyperLink></li>
        <li><div class="blank"></div></li>
        <li><asp:HyperLink ID="lnkUser" Visible="false" Text="Manage user" runat="server" NavigateUrl="~/Web page/UserManage.aspx"><span>Manage</span></asp:HyperLink></li>        
    </ul>
</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="main" Runat="Server">
<div id="contentPanel">
    <asp:Table ID="tableResponse" BorderColor="#497f35" BorderStyle="Inset" BorderWidth="1" GridLines="Both" runat="server">
        <asp:TableHeaderRow>
            <asp:TableCell ColumnSpan="4"><h2>Warning Message Response</h2></asp:TableCell>
        </asp:TableHeaderRow>
    </asp:Table><br />    
    <asp:Table ID="tableInbox" runat="server">
        <asp:TableHeaderRow>
            <asp:TableCell ColumnSpan="4">
                <h2>Inbox</h2>
            </asp:TableCell>
        </asp:TableHeaderRow>
    </asp:Table>
    <asp:Table ID="tableSent" runat="server">
        <asp:TableHeaderRow>
            <asp:TableCell ColumnSpan="3">
                <h2>Sent</h2>
            </asp:TableCell>
        </asp:TableHeaderRow>
    </asp:Table>
</div>
</asp:Content>

