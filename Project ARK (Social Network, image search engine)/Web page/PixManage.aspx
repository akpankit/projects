﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web page/Mpage1.master" AutoEventWireup="true" CodeFile="~/Web page/PixManage.aspx.cs" Inherits="PixManage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="up" runat="server">
<div id="menuContent">
    <ul>
        <li><asp:HyperLink ID="HyperLink5" Text="My Friend" runat="server" NavigateUrl="~/Web page/FriendManage.aspx"><span>My Friend</span></asp:HyperLink></li>
        <li><div class="blank"></div></li>
        <li><asp:HyperLink ID="HyperLink6" Text="Message" runat="server" NavigateUrl="~/Web page/MessageManage.aspx"><span>Message</span></asp:HyperLink></li>        
        <li><div class="blank"></div></li>
        <li><asp:HyperLink ID="lnkUser" Visible="false" Text="Manage user" runat="server" NavigateUrl="~/Web page/UserManage.aspx"><span>Manage</span></asp:HyperLink></li>        
    </ul>
</div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="main" Runat="Server">
<div id="contentPanel">
    <table>
        <tr>
            <td>
                <asp:Table ID="tableUpload" runat="server" HorizontalAlign="Center">
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                            <asp:Label ID="Label1" Text="Upload new pix" Font-Size="Medium" runat="server"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                            <asp:Label ID="lblError" runat="server"></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>                    
                    <asp:TableRow>
                        <asp:TableCell>Title:</asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left"><asp:TextBox ID="txtTitle" runat="server"></asp:TextBox></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>Place:</asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left"><asp:TextBox ID="txtPlace" runat="server"></asp:TextBox></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>Time:</asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left"><asp:Calendar ID="calTime" runat="server"></asp:Calendar></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>Annotation:</asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left"><asp:TextBox ID="txtAnnotation" runat="server"></asp:TextBox></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>File: </asp:TableCell>
                        <asp:TableCell><asp:FileUpload ID="fu1" runat="server" /></asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>Public type:</asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">
                            <asp:DropDownList ID="dropType" runat="server"></asp:DropDownList>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell></asp:TableCell>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:LinkButton ID="lnkUpload" runat="server" Text="Upload" OnClick="lnkUpload_Click" class="login"></asp:LinkButton>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
            <td>
                <asp:Table ID="tableImage" HorizontalAlign="Center" runat="server">
                </asp:Table>
            </td>
        </tr>
    </table>    

    
</div>
</asp:Content>

