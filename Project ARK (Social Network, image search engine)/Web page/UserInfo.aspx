﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web page/Mpage1.master" AutoEventWireup="true" CodeFile="~/Web page/UserInfo.aspx.cs" Inherits="UserInfo" %>
<asp:Content ID="Content2" ContentPlaceHolderID="up" runat="server">
<div id="menuContent">
    <ul>
        <li><asp:HyperLink ID="HyperLink4" Text="My image" runat="server" NavigateUrl="~/Web page/PixManage.aspx"><span>My image</span></asp:HyperLink></li>
        <li><div class="blank"></div></li>
        <li><asp:HyperLink ID="HyperLink5" Text="My Friend" runat="server" NavigateUrl="~/Web page/FriendManage.aspx"><span>My Friend</span></asp:HyperLink></li>
        <li><div class="blank"></div></li>
        <li><asp:HyperLink ID="HyperLink6" Text="Message" runat="server" NavigateUrl="~/Web page/MessageManage.aspx"><span>Message</span></asp:HyperLink></li>        
        <li><div class="blank"></div></li>
        <li><asp:HyperLink ID="lnkUser" Visible="false" Text="Manage user" runat="server" NavigateUrl="~/Web page/UserManage.aspx"><span>Manage</span></asp:HyperLink></li>        
    </ul>
</div>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="main" Runat="Server">
<div id="contentPanel">
    <asp:Table ID="table" Width="300px" runat="server" BorderWidth="1" Font-Bold="true" Font-Size="small" HorizontalAlign="Center">        
        <asp:TableRow>
            <asp:TableCell ColumnSpan="3" ForeColor="blue" HorizontalAlign="Center">
                <asp:Label ID="lblError" runat="server"></asp:Label>
            </asp:TableCell>
        </asp:TableRow>
        
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right">
                <asp:Label ID="Label2" runat="server" >Old Password:</asp:Label>
            </asp:TableCell>
            <asp:TableCell></asp:TableCell>
            <asp:TableCell HorizontalAlign="left">
                <asp:TextBox ID="txtOldPass" runat="server" TextMode="Password"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right">
                <asp:Label ID="Label1" runat="server" >New Password:</asp:Label>
            </asp:TableCell>
            <asp:TableCell></asp:TableCell>
            <asp:TableCell HorizontalAlign="left">
                <asp:TextBox ID="txtNewPass" runat="server" TextMode="Password"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right">
                <asp:Label ID="Label3" runat="server">Confirm password:</asp:Label>
            </asp:TableCell>
            <asp:TableCell></asp:TableCell>
            <asp:TableCell HorizontalAlign="left">
                <asp:TextBox ID="txtConfirmPass" runat="server" TextMode="Password"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>

        <asp:TableRow>
            <asp:TableCell></asp:TableCell>
            <asp:TableCell></asp:TableCell>
            <asp:TableCell HorizontalAlign="right">
                <asp:Button ID="lnkChangePass" runat="server" CssClass="login" OnClick="lnkChangePass_Click" Text="Change pass" />
            </asp:TableCell>
        </asp:TableRow>
        
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right">
                <asp:Label ID="Label4" runat="server">Fullname:</asp:Label>
            </asp:TableCell>
            <asp:TableCell></asp:TableCell>
            <asp:TableCell HorizontalAlign="left">
                <asp:TextBox ID="txtFullname" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right">
                <asp:Label ID="Label5" runat="server">Email:</asp:Label>
            </asp:TableCell>
            <asp:TableCell></asp:TableCell>
            <asp:TableCell HorizontalAlign="left">
                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right">
                <asp:Label ID="Label6" runat="server">Phone</asp:Label>
            </asp:TableCell>
            <asp:TableCell></asp:TableCell>
            <asp:TableCell HorizontalAlign="left">
                <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        
        <asp:TableRow>
            <asp:TableCell></asp:TableCell>
            <asp:TableCell></asp:TableCell>
            <asp:TableCell HorizontalAlign="right">
                <asp:Button ID="lnkUpdateInfo" runat="server" CssClass="login" OnClick="lnkUpdateInfo_Click" Text="Update info" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</div>
</asp:Content>

