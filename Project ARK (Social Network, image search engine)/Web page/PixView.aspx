﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web page/Mpage1.master" AutoEventWireup="true" CodeFile="~/Web page/PixView.aspx.cs" Inherits="PixView" %>
<asp:Content ID="Content2" ContentPlaceHolderID="up" runat="server">
<div id="menuContent">
    <ul>
        <li><asp:HyperLink ID="HyperLink4" Text="My image" runat="server" NavigateUrl="~/Web page/PixManage.aspx"><span>My image</span></asp:HyperLink></li>
        <li><div class="blank"></div></li>
        <li><asp:HyperLink ID="HyperLink5" Text="My Friend" runat="server" NavigateUrl="~/Web page/FriendManage.aspx"><span>My Friend</span></asp:HyperLink></li>
        <li><div class="blank"></div></li>
        <li><asp:HyperLink ID="HyperLink6" Text="Message" runat="server" NavigateUrl="~/Web page/MessageManage.aspx"><span>Message</span></asp:HyperLink></li>
        <li><div class="blank"></div></li>
        <li><asp:HyperLink ID="lnkUser" Visible="false" Text="Manage user" runat="server" NavigateUrl="~/Web page/UserManage.aspx"><span>Manage</span></asp:HyperLink></li>        
        <li><div class="blank"></div></li>
        <li><asp:LinkButton ID="lnkComplain" Text="Complain" runat="server" OnClick="lnkComplain_Click"></asp:LinkButton></li>
    </ul>
</div>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="main" Runat="Server">
    <div id="contentPanel">    
    <asp:Table ID="tableImage" HorizontalAlign="Center" runat="server">
        <asp:TableHeaderRow>
            <asp:TableCell ColumnSpan="3">
                <center><asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label></center>    
            </asp:TableCell>
        </asp:TableHeaderRow>
        <asp:TableRow>
            <asp:TableCell RowSpan="100" Width="150px">
                <center><h2>Image info</h2></center>
                Title:<asp:TextBox ID="txtTitle" Enabled="false" runat="server"></asp:TextBox><br />
                Place:<asp:TextBox ID="txtPlace" Enabled="false" runat="server"></asp:TextBox><br />
                Public type:<asp:DropDownList ID="dropType" Enabled="false" runat="server"></asp:DropDownList>
                <asp:LinkButton ID="btnUpdate" CssClass="login" runat="server" Visible="false" Text="Update" ></asp:LinkButton>
                <h2>Average ratings: <asp:Label ID="lblRate" runat="server"></asp:Label></h2><br />
                <h2>Download: <asp:Label ID="lblDownload" runat="server"></asp:Label></h2>
                <h2>Owner visit: <asp:Label ID="lblOwner" runat="server"></asp:Label></h2><br />
                <h2>OU visit: <asp:Label ID="lblOU" runat="server"></asp:Label></h2><br />
                <h2>VI visit: <asp:Label ID="lblVI" runat="server"></asp:Label></h2><br />
                <h2>Total visit: <asp:Label ID="lblTotal" runat="server"></asp:Label></h2>
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="Center" Width="350px">
                Rate:
                <asp:LinkButton ID="lnk1" Font-Underline="false" runat="server" Text="1" OnClick="lnkRate_Click"></asp:LinkButton>&nbsp;
                <asp:LinkButton ID="lnk2" Font-Underline="false" runat="server" Text="2" OnClick="lnkRate_Click"></asp:LinkButton>&nbsp;
                <asp:LinkButton ID="lnk3" Font-Underline="false" runat="server" Text="3" OnClick="lnkRate_Click"></asp:LinkButton>&nbsp;
                <asp:LinkButton ID="lnk4" Font-Underline="false" runat="server" Text="4" OnClick="lnkRate_Click"></asp:LinkButton>&nbsp;
                <asp:LinkButton ID="lnk5" Font-Underline="false" runat="server" Text="5" OnClick="lnkRate_Click"></asp:LinkButton>&nbsp;
            </asp:TableCell>
            <asp:TableCell RowSpan="4" HorizontalAlign="Right">
                <asp:Image runat="server" ID="img" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow HorizontalAlign="Center">
            <asp:TableCell ID="cell"></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center">
                <asp:TextBox ID="txtPath" runat="server"></asp:TextBox>
                <asp:Button ID="btnDownload" runat="server" Text="Download" CssClass="login" OnClick="btnDownload_Click"/>
            </asp:TableCell>            
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Label ID="lblComment" runat="server" Text="Comment"></asp:Label><br />
                <asp:TextBox ID="txtComment" TextMode="MultiLine" Width="100%" Rows="3" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right">
                <asp:Button ID="btnPost" runat="server" Text="Post" CssClass="login" OnClick="btnPost_Click"/>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</div>
</asp:Content>

