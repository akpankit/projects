﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Mpage1 : System.Web.UI.MasterPage
{
    ClassProcess cProcess = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session.Count != 0)
        {
            lnkUsername.Text = Session["user"].ToString();
            lnkSignOut.Visible = true;
            lnkUsername.Visible = true;
        }
    }

    protected void lnkUsername_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Web page/UserInfo.aspx");
    }

    protected void lnkSignOut_Click(object sender, EventArgs e)
    {
        lnkSignOut.Visible = false;
        lnkUsername.Visible = false;
        cProcess = new ClassProcess();
        if (int.Parse(cProcess.getOutstanding(Session["user"].ToString())) >= 2)
        {
            cProcess.deleteRow("Visit", "username", Session["user"].ToString());
            cProcess.deleteRow("Rate", "username", Session["user"].ToString());
            cProcess.deleteRow("Comment", "username", Session["user"].ToString());
            cProcess.deleteRow("Pix", "owner", Session["user"].ToString());
            cProcess.deleteRow("Message", "receiver", Session["user"].ToString());
            cProcess.deleteRow("Relation", "owner", Session["user"].ToString());
            cProcess.deleteRow("Relation", "friend", Session["user"].ToString());
            cProcess.deleteRow("User", "username", Session["user"].ToString());
        }
        Session.RemoveAll();
        Session.Abandon();
        Response.Redirect("~/Web page/Default.aspx");
    }


}
