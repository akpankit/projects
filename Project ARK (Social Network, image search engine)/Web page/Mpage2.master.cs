﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Web_page_Mpage2 : System.Web.UI.MasterPage
{
    ClassProcess cProcess = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session.Count == 0)
            changeViewState(false);
        else
        {
            changeViewState(true);
            lnkUsername.Text = Session["user"].ToString();
        }
        cProcess = new ClassProcess();
        // init top user
        DataTable tableOwner = cProcess.getVisitDueToOwner();
        if (tableOwner != null && tableOwner.Rows.Count != 0)
        {
            // store visit of each user
            int[] owners = new int[tableOwner.Rows.Count];
            DataTable tablePix = cProcess.getVisitDueToPix();
            for (int i = 0; i < tableOwner.Rows.Count; i++)
            {
                string owner = tableOwner.Rows[i]["owner"].ToString();
                if (tablePix != null && tablePix.Rows.Count != 0)
                {
                    for (int j = 0; j < tablePix.Rows.Count; j++)
                    {
                        string pixid = tablePix.Rows[j]["pixid"].ToString();
                        // verify if the current image belongs to owner
                        if (cProcess.getInfo("Pix", "owner", "pixid", "owner", pixid, owner).Equals(""))
                            continue;
                        // store the visit of pix
                        owners[i] += int.Parse(tablePix.Rows[j]["numofvisit"].ToString());
                    }
                }
                else tableImage.Visible = false;
                owners[i] += int.Parse(tableOwner.Rows[i]["numofvisit"].ToString());
            }
            TableRow row = new TableRow();
            // get 3 max user
            for (int i = 0; i < 3; i++)
            {
                int index = findMax(owners);
                if (owners[index] != 0)
                {
                    owners[index] = 0;

                    // init cell
                    TableCell cell = new TableCell();
                    //init username link
                    Image img = new Image();
                    img.ImageUrl = new ClassProcess().getInfo("user", "profile", "username", tableOwner.Rows[index]["owner"].ToString());
                    img.Height = img.Width = 90;
                    HyperLink lnk = new HyperLink();
                    lnk.NavigateUrl = "~/Web page/FriendPix.aspx?username=" + tableOwner.Rows[index]["owner"].ToString();
                    lnk.Controls.Add(img);
                    cell.Controls.Add(lnk);

                    // add cell to row and row to table
                    row.Controls.Add(cell);

                }
            }
            tableUser.Controls.Add(row);
        }
        else tableUser.Visible = false;

        DataTable mytable = new ClassProcess().getImageList("%");
        if (mytable != null && mytable.Rows.Count != 0)
        {
            //store pixid of pix
            string[] pixids = new string[mytable.Rows.Count];
            //store number of visit on pix
            int[] visits = new int[mytable.Rows.Count];
            //get the value for 2 above array
            for (int i = 0; i < mytable.Rows.Count; i++)
            {
                if (new ClassProcess().getSumOfVisit(mytable.Rows[i]["pixid"].ToString()).Equals(""))
                    visits[i] = int.Parse(mytable.Rows[i]["visit"].ToString());
                else visits[i] = int.Parse(new ClassProcess().getSumOfVisit(mytable.Rows[i]["pixid"].ToString()))
                    + int.Parse(mytable.Rows[i]["visit"].ToString());
                pixids[i] = mytable.Rows[i]["pixid"].ToString();
            }
            TableRow row = new TableRow();
            for (int i = 0; i < 3; i++)
            {
                int pixIndex = findMax(visits);
                if (visits[pixIndex] != 0)
                {
                    visits[pixIndex] = 0;

                    // init cell
                    TableCell cell = new TableCell();
                    //init image link
                    Image img = new Image();
                    img.ImageUrl = cProcess.getPixPath(pixids[pixIndex]);
                    img.Height = 90;
                    img.Width = 90;
                    img.ToolTip = cProcess.getInfo("Pix", "title", "pixid", pixids[pixIndex]);

                    //init link of image
                    HyperLink lnk = new HyperLink();
                    lnk.Controls.Add(img);
                    lnk.NavigateUrl = "~/Web page/FriendPix.aspx?username=" +
                        cProcess.getInfo("Pix", "owner", "pixid", pixids[pixIndex]);
                    cell.Controls.Add(lnk);

                    // add cell to row and row to table
                    row.Controls.Add(cell);
                }
            }
            tableImage.Controls.Add(row);
        }
        else tableImage.Visible = false;

        //init accepted user
        DataTable table = new ClassProcess().getAcceptedUserList();
        if (table != null && table.Rows.Count != 0)
        {
            TableRow row;
            TableCell cell;
            for (int i = 0; i < table.Rows.Count; i++)
            {
                row = new TableRow();
                //init cell
                cell = new TableCell();
                string username = table.Rows[i]["username"].ToString();
                string contact = table.Rows[i]["contact"].ToString();
                cell.Text = (i + 1).ToString() + ". " + username + " " + contact;
                row.Controls.Add(cell);
                table1.Controls.Add(row);
            }
        }
        else table1.Visible = false;
    }

    private int findMax(int[] owners)
    {
        int max = owners[0];
        int index = 0;
        for (int i = 1; i < owners.Length; i++)
        {
            if (max < owners[i])
            {
                max = owners[i];
                index = i;
            }
        }
        return index;
    }

    private void changeViewState(bool state)
    {
        lnkSignOut.Visible = state;
        lnkUsername.Visible = state;
        lblTitle.Visible = !state;
        txtUser.Visible = !state;
        txtPass.Visible = !state;
        lnkLogin.Visible = !state;
        lnkRegister.Visible = !state;
    }

    protected void lnkUsername_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Web page/UserInfo.aspx");
    }

    protected void lnkSignOut_Click(object sender, EventArgs e)
    {
        changeViewState(false);
        txtUser.Text = "User Name";
        txtPass.Text = "Password";
        cProcess = new ClassProcess();
        if (int.Parse(cProcess.getOutstanding(Session["user"].ToString())) >= 2)
        {
            cProcess.deleteRow("Visit", "username", Session["user"].ToString());
            cProcess.deleteRow("Rate", "username", Session["user"].ToString());
            cProcess.deleteRow("Comment", "username", Session["user"].ToString());
            cProcess.deleteRow("Pix", "owner", Session["user"].ToString());
            cProcess.deleteRow("Message", "receiver", Session["user"].ToString());
            cProcess.deleteRow("Relation", "owner", Session["user"].ToString());
            cProcess.deleteRow("Relation", "friend", Session["user"].ToString());
            cProcess.deleteRow("User", "username", Session["user"].ToString());
        }
        Session.RemoveAll();
        Session.Abandon();
        Response.Redirect("~/Web page/Default.aspx");
    }

    protected void lnkLogin_Click(object sender, EventArgs e)
    {
        cProcess = new ClassProcess();
        if (cProcess.isLogIn(ref lnkUsername, txtUser.Text, txtPass.Text))
        {
            changeViewState(true);
            Session.Add("name", cProcess.getInfo("User", "fullname", "username", lnkUsername.Text));
            Session.Add("user", lnkUsername.Text);
            lnkUsername.Text = Session["user"].ToString();
            if (cProcess.isSU(txtUser.Text))
                Response.Redirect("~/Web page/UserManage.aspx");
            Response.Redirect("~/Web page/Profile.aspx");
            return;
        }
        lnkUsername.Text = "invalid user and pass";
        lnkUsername.Visible = true;
    }
}
