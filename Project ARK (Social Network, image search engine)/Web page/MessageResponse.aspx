﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Web page/Mpage1.master" AutoEventWireup="true" CodeFile="~/Web page/MessageResponse.aspx.cs" Inherits="Web_page_MessageResponse" %>

<asp:Content ID="Content1" ContentPlaceHolderID="up" Runat="Server">
<div id="menuContent">
    <ul>
        <li><asp:HyperLink ID="HyperLink4" Text="My image" runat="server" NavigateUrl="~/Web page/PixManage.aspx"><span>My image</span></asp:HyperLink></li>
        <li><div class="blank"></div></li>
        <li><asp:HyperLink ID="HyperLink5" Text="My Friend" runat="server" NavigateUrl="~/Web page/FriendManage.aspx"><span>My Friend</span></asp:HyperLink></li>
        <li><div class="blank"></div></li>
        <li><asp:HyperLink ID="HyperLink6" Text="Message" runat="server" NavigateUrl="~/Web page/MessageManage.aspx"><span>Message</span></asp:HyperLink></li>        
    </ul>
</div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="main" Runat="Server">
<div id="contentPanel">
    <asp:Table ID="Table1" runat="server" HorizontalAlign="Center">        
        <asp:TableHeaderRow HorizontalAlign="Center">
            <asp:TableCell ColumnSpan="2"><h2>Response to the warning message</h2></asp:TableCell>
        </asp:TableHeaderRow>
        <asp:TableRow>
            <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ColumnSpan="2">
                <asp:Label ID="lblUsername" runat="server"></asp:Label> :
                <asp:Label ID="lblContent" runat="server"></asp:Label>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ColumnSpan="2">
                <asp:TextBox ID="txtResponse" TextMode="MultiLine" Width="100%" Rows="3" runat="server"></asp:TextBox>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow> 
            <asp:TableCell></asp:TableCell>          
            <asp:TableCell HorizontalAlign="center">
                <asp:Button ID="btnResponse" runat="server" CssClass="login" OnClick="btnResponse_Click" Text="Response" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</div>
</asp:Content>

