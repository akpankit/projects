﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text.RegularExpressions;

public partial class FriendPix : System.Web.UI.Page
{
    private ClassProcess cProcess = null;
    protected void Page_Load(object sender, EventArgs e)
    {
        int type = 1;
        lblProfile.Text = Request["username"] + "'s profile";
        txtFullname.Text = new ClassProcess().getInfo("user", "fullname", "username", Request["username"]);
        txtContact.Text = new ClassProcess().getInfo("user", "contact", "username", Request["username"]);
        txtPhone.Text = new ClassProcess().getInfo("user", "phone", "username", Request["username"]);        
        if (Session.Count == 0) type = 1;
        else 
        {
            if (new ClassProcess().isSU(Session["user"].ToString()))
            {
                lnkUser.Visible = true;
                txtFullname.Enabled = true;
                txtContact.Enabled = true;
                txtPhone.Enabled = true;
                btnUpdate.Visible = true;
            }
            if(new ClassProcess().getInfo("Relation", "welcome", "friend", Session["user"].ToString()).ToLower().Equals("true"))
                type=3;
            //check if the current user is a friend of owner to view add friend link
            if (new ClassProcess().getInfo("Relation", "welcome", "owner", "friend", Session["user"].ToString(), Request["username"]).Equals(""))
                lnkFriend.Visible = true;
            else lnkFriend.Visible = false;
            // check if this is the last time user can log in
            if (int.Parse(new ClassProcess().getOutstanding(Session["user"].ToString())) >= 2)
            {
                lnkFriend.Enabled = false;
            }
        }                
        //load the image
        DataTable tabImage = new ClassProcess().getImageList(Request["username"], type);
        if (tabImage != null && tabImage.Rows.Count != 0)
        {
            TableCell cell = new TableCell();
            for (int j = 0; j < tabImage.Rows.Count; j++)
            {   
                ImageButton image = new ImageButton();
                string imagePath = tabImage.Rows[j]["path"].ToString();
                image.ImageUrl = imagePath;
                image.Width = 90;
                image.Height = 90;
                image.ToolTip = tabImage.Rows[j]["title"].ToString();
                HyperLink lnk = new HyperLink();
                lnk.Controls.Add(image);
                lnk.NavigateUrl = "~/Web page/PixView.aspx?pixid=" + tabImage.Rows[j]["pixid"].ToString() +"&username=" + Request["username"];
                lnk.Font.Underline = false;
                cell.Controls.Add(lnk);
            }
            TableRow row = new TableRow();
            row.Controls.Add(cell);
            tableImage.Controls.Add(row);
        }
    }

    protected void lnkAdd_Click(object sender, EventArgs e)
    {
        new ClassProcess().insertFriend(Session["user"].ToString(), Request["username"], true);
        lblError.Text = "You have added this user as friend";
        lblError.Visible = true;
    }

    public bool checkEmailInvalid(TextBox email)
    {
        //define the valid format
        string reg = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
        Regex emailRegex = new Regex(reg);
        Match m = emailRegex.Match(email.Text);
        //verify if the email is not in valid format
        if (!m.Success)
        {
            lblError.Text = ("Email is invalid");
            email.Focus();
            return true;
        }
        return false;
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        //verify if the fullname is empty
        if (txtFullname.Text.Trim().Equals(""))
        {
            lblError.Text = "input your fullname";
            txtFullname.Focus();
            return;
        }
        //verify if the email is not empty
        if (!txtContact.Text.Trim().Equals(""))
        {
            //verify if the email is in valid format
            if (checkEmailInvalid(txtContact)) return;
        }
        if (!txtPhone.Text.Trim().Equals(""))
        {
            int check;
            if (!int.TryParse(txtPhone.Text.Trim(), out check))
            {
                lblError.Text = "the phone number must be numeric";
                txtPhone.Focus();
                return;
            }
        }
        cProcess = new ClassProcess();
        //update fullname value in db
        cProcess.updateUserInfo(Session["user"].ToString(), "fullname", txtFullname.Text);
        //update contact value in db
        cProcess.updateUserInfo(Session["user"].ToString(), "contact", txtContact.Text);
        //update phone value in db
        cProcess.updateUserInfo(Session["user"].ToString(), "phone", txtPhone.Text);
        lblError.Text = "Update info successful";
    }
}