using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public class DAL
{
    //db connection
    private SqlConnection sqlConnection = null;
    //db execute command
    private SqlCommand sqlCommand = null;
    //db storage
    private SqlDataAdapter sqlDataAdapter = null;
    //db connection string
    private string connectstring = null;


    public string getConnectionstring
    {
        //get the connection string
        get { return connectstring; }
    }

    public bool isConnected
    {
        get
        {
            //verify if the db is not connected
            if (sqlConnection == null) return false;
            //return the true / false value when comparing the state of sql connection to the open state
            return (sqlConnection.State == ConnectionState.Open);
        }
    }

    public ConnectionState state
    {
        //return the sql connection state
        get { return sqlConnection.State; }
    }

    public DAL()
    {
        //get the connection string from the app config
        connectstring = ConfigurationManager.AppSettings["Connectionstring"];
        //init the connection with the given connection stringtao ket noi thong qua cau ket noi vua lay
        sqlConnection = new SqlConnection(getConnectionstring);
        //init the execute command
        sqlCommand = new SqlCommand();
        sqlCommand.Connection = sqlConnection;
        //store the data in the adapter
        sqlDataAdapter = new SqlDataAdapter(sqlCommand);
    }

    public void open()
    {
        try
        {
            //open the connection
            sqlConnection.Open();
        }
        catch 
        {
            return;
        }
    }

    public void dispose()
    {
        //remove all variable
        if (sqlConnection != null)
        {
            sqlConnection.Close();
            sqlConnection.Dispose();
            sqlConnection = null;
        }
        if (sqlDataAdapter != null)
        {
            sqlDataAdapter.Dispose();
            sqlDataAdapter = null;
        }
        if (sqlCommand != null)
        {
            sqlCommand.Dispose();
            sqlCommand = null;
        }
        GC.SuppressFinalize(this);
    }

    public void setCommandText(string sql)
    {
        //init sql query
        sqlCommand.Parameters.Clear();
        sqlCommand.CommandText = sql;
        sqlCommand.CommandType = CommandType.Text;
    }

    public void addParam(string param, object value)
    {
        //add parameter into the query
        SqlParameter p = new SqlParameter("@" + param, value);
        sqlCommand.Parameters.Add(p);
    }

    public DataTable executeQuery()
    {
        DataTable dataTable = new DataTable();
        //fill the data stored in the adapter in the table
        sqlDataAdapter.Fill(dataTable);
        return dataTable;
    }

    public DataTable executeQueryTable(string TableName)
    {
        DataTable dataTable = new DataTable(TableName);
        sqlDataAdapter.Fill(dataTable);
        return dataTable;
    }

    public DataSet executeQuery(string TableName)
    {
        DataSet dataSet = new DataSet();
        sqlDataAdapter.Fill(dataSet, TableName);
        return dataSet;
    }

    public int executeNonQuery()
    {
        return sqlCommand.ExecuteNonQuery();
    }

    public void close()
    {
        sqlConnection.Close();
    }
}