﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for ClassProcess
/// </summary>
public class ClassProcess
{
    private DAL dal = null;
    public ClassProcess()
    {
        dal = new DAL();
    }

    public DataTable getPixType()
    {
        DataTable table = null;
        try
        {
            dal.open();
            string sql = "select name from [Type]";
            dal.setCommandText(sql);
            if (dal.executeQuery().Rows.Count != 0)
                table = dal.executeQuery();
        }
        catch { }
        finally { dal.close(); }
        return table;
    }

    /// <summary>
    /// get path of the pix
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public string getPixPath(string value)
    {
        string index = "";
        try
        {
            dal.open();
            string sql = "select path from [Pix] where pixid=@value";
            dal.setCommandText(sql);
            dal.addParam("value", value);
            DataTable tab = dal.executeQuery();
            index = tab.Rows[0][0].ToString();
        }
        catch { }
        finally { dal.close(); }
        return index;
    }

    /// <summary>
    /// get sum of visit of pix
    /// </summary>
    /// <param name="pixid"></param>
    /// <returns></returns>
    public string getSumOfVisit(string pixid)
    {
        string index = "";
        try
        {
            dal.open();
            string sql = "select sum(visit) from [Visit] where pixid=@value";
            dal.setCommandText(sql);
            dal.addParam("value", pixid);
            DataTable tab = dal.executeQuery();
            index = tab.Rows[0][0].ToString();
        }
        catch { }
        finally { dal.close(); }
        return index;
    }

    /// <summary>
    /// get sum of ratings
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public string getSumOfRate(string value)
    {
        string index = "0";
        try
        {
            dal.open();
            string sql = "select sum(rate) from [Rate] where pixid=@value";
            dal.setCommandText(sql);
            dal.addParam("value", value);
            DataTable tab = dal.executeQuery();
            index = tab.Rows[0][0].ToString();
        }
        catch { }
        finally { dal.close(); }
        return index;
    }

    /// <summary>
    /// get number of ratings
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public string getCountOfRate(string value)
    {
        string index = "";
        try
        {
            dal.open();
            string sql = "select count(rate) from [Rate] where pixid=@value";
            dal.setCommandText(sql);
            dal.addParam("value", value);
            DataTable tab = dal.executeQuery();
            index = tab.Rows[0][0].ToString();
        }
        catch { }
        finally { dal.close(); }
        return index;
    }

    /// <summary>
    /// get the value of column in table
    /// </summary>
    /// <param name="table"></param>
    /// <param name="needColumn"></param>
    /// <param name="conditionColumn"></param>
    /// <param name="value"></param>
    /// <returns>need column value</returns>
    public string getInfo(string table, string needColumn, string conditionColumn, string value)
    {
        string index = "";
        try
        {
            dal.open();
            string sql = "select " + needColumn + " from [" + table + "] where " + conditionColumn + "=@value";
            dal.setCommandText(sql);
            dal.addParam("value", value);
            DataTable tab = dal.executeQuery();
            index = tab.Rows[0][0].ToString();
        }
        catch { }
        finally { dal.close(); }
        return index;
    }

    /// <summary>
    /// get the value of column in table
    /// </summary>
    /// <param name="table"></param>
    /// <param name="needColumn"></param>
    /// <param name="conditionColumn"></param>
    /// <param name="value"></param>
    /// <returns>need column value</returns>
    public string getInfo(string table, string needColumn, string conditionColumn, string conditionColumn2, string value, string value2)
    {
        string index = "";
        try
        {
            dal.open();
            string sql = "select " + needColumn + " from [" + table + "] where " + conditionColumn + "=@value and " + conditionColumn2 + "=@value2";
            dal.setCommandText(sql);
            dal.addParam("value", value);
            dal.addParam("value2", value2);
            DataTable tab = dal.executeQuery();
            index = tab.Rows[0][0].ToString();
        }
        catch { }
        finally { dal.close(); }
        return index;
    }

    /// <summary>
    /// get max index of the specific column in the table
    /// </summary>
    /// <param name="tableName"></param>
    /// <param name="needColumn"></param>
    /// <returns>max index</returns>
    public string getMaxIndex(string tableName, string needColumn)
    {
        string index = "1";
        try
        {
            dal.open();
            string sql = "select top 1 " + needColumn + " from [" + tableName + "] order by " + needColumn + " desc";
            dal.setCommandText(sql);
            DataTable table = dal.executeQuery();
            if (table.Rows.Count != 0)
                index = (int.Parse(table.Rows[0][0].ToString().Substring(0)) + 1).ToString();
        }
        catch { }
        finally { dal.close(); }
        return index;
    }

    /// <summary>
    /// get max index of the specific column in the table
    /// </summary>
    /// <param name="tableName"></param>
    /// <param name="needColumn"></param>
    /// <returns>max index of pix name</returns>
    public string getMaxIndexOfName(string tableName, string needColumn)
    {
        string index = "image1";
        try
        {
            dal.open();
            string sql = "select top 1 " + needColumn + " from [" + tableName + "] order by len(" + needColumn + ") desc," + needColumn + " desc";
            dal.setCommandText(sql);
            DataTable table = dal.executeQuery();
            if (table.Rows.Count != 0)
                index = "image" + (int.Parse(table.Rows[0][0].ToString().Substring(5)) + 1).ToString();
        }
        catch { }
        finally { dal.close(); }
        return index;
    }

    /// <summary>
    /// get image list
    /// </summary>
    /// <param name="username"></param>
    /// <returns>table contains image list</returns>
    public DataTable getImageList(string username)
    {
        DataTable table = null;
        try
        {
            dal.open();
            string sql = "select * from [Pix] where owner like @username";
            dal.setCommandText(sql);
            dal.addParam("username", username);
            if (dal.executeQuery().Rows.Count != 0)
                table = dal.executeQuery();
        }
        catch { }
        finally { dal.close(); }
        return table;
    }

    /// <summary>
    /// get image list with the image type
    /// </summary>
    /// <param name="username"></param>
    /// <returns>table contains image list</returns>
    public DataTable getImageList(string username, int type)
    {
        DataTable table = null;
        try
        {
            dal.open();
            string sql = "select * from [Pix] where owner=@username and type<=@type";
            dal.setCommandText(sql);
            dal.addParam("username", username);
            dal.addParam("type", type);
            if (dal.executeQuery().Rows.Count != 0)
                table = dal.executeQuery();
        }
        catch { }
        finally { dal.close(); }
        return table;
    }

    /// <summary>
    /// get matched password user
    /// </summary>
    /// <param name="password"></param>
    /// <returns></returns>
    public DataTable getMatchedUserList(string password)
    {
        DataTable table = null;
        try
        {
            dal.open();
            string sql = "select * from [User] where password=@password and inuse<>0";
            dal.setCommandText(sql);
            dal.addParam("password", password);
            if (dal.executeQuery().Rows.Count != 0)
                table = dal.executeQuery();
        }
        catch { }
        finally { dal.close(); }
        return table;
    }

    /// <summary>
    /// get the friend with the blacklist status
    /// </summary>
    /// <param name="username"></param>
    /// <param name="blacklist"></param>
    /// <returns></returns>
    public DataTable getFriendList(string username, bool blacklist)
    {
        DataTable table = null;
        try
        {
            dal.open();
            string sql = "select friend from [Relation] b where b.owner=@username and b.welcome=@blacklist";
            dal.setCommandText(sql);
            dal.addParam("username", username);
            dal.addParam("blacklist", blacklist);
            if (dal.executeQuery().Rows.Count != 0)
                table = dal.executeQuery();
        }
        catch { }
        finally { dal.close(); }
        return table;
    }

    /// <summary>
    /// get friend request list
    /// </summary>
    /// <param name="username"></param>
    /// <returns></returns>
    public DataTable getFriendRequestList(string username)
    {
        DataTable table = null;
        try
        {
            dal.open();
            string sql = "select owner from [Relation] where friend=@username";
            dal.setCommandText(sql);
            dal.addParam("username", username);
            if (dal.executeQuery().Rows.Count != 0)
                table = dal.executeQuery();
        }
        catch { }
        finally { dal.close(); }
        return table;
    }
    
    /// <summary>
    /// get comment list on a pix
    /// </summary>
    /// <param name="pixid"></param>
    /// <returns></returns>
    public DataTable getCommentList(string pixid)
    {
        DataTable table = null;
        try
        {
            dal.open();
            string sql = "select username, comment, post from [Comment] where pixid=@pixid order by post desc";
            dal.setCommandText(sql);
            dal.addParam("pixid", pixid);
            if (dal.executeQuery().Rows.Count != 0)
                table = dal.executeQuery();
        }
        catch { }
        finally { dal.close(); }
        return table;
    }

    /// <summary>
    /// get message list
    /// </summary>
    /// <param name="username"></param>
    /// <returns>table contains image list</returns>
    public DataTable getMessageList(string column, string username)
    {
        DataTable table = null;
        try
        {
            dal.open();
            string sql = "select * from [Message] where "+column+"=@username and reject='false' order by sent desc";
            dal.setCommandText(sql);
            dal.addParam("username", username);
            if (dal.executeQuery().Rows.Count != 0)
                table = dal.executeQuery();
        }
        catch { }
        finally { dal.close(); }
        return table;
    }

    /// <summary>
    /// get message list with condition
    /// </summary>
    /// <param name="username"></param>
    /// <returns>table contains image list</returns>
    public DataTable getSentMessageList(string username)
    {
        DataTable table = null;
        try
        {
            dal.open();
            string sql = "select * from [Message] where (receiver=@username and responsecontent<>'') or sender=@username order by sent desc";
            dal.setCommandText(sql);
            dal.addParam("username", username);
            if (dal.executeQuery().Rows.Count != 0)
                table = dal.executeQuery();
        }
        catch { }
        finally { dal.close(); }
        return table;
    }

    /// <summary>
    /// get sum of visit due to owner
    /// </summary>
    /// <param name="username"></param>
    /// <returns>table of owner and visit</returns>
    public DataTable getVisitDueToOwner()
    {
        DataTable table = null;
        try
        {
            dal.open();
            string sql = "select owner, sum(visit) as numofvisit FROM [Pix] group by owner";
            dal.setCommandText(sql);
            if (dal.executeQuery().Rows.Count != 0)
                table = dal.executeQuery();
        }
        catch { }
        finally { dal.close(); }
        return table;
    }

    /// <summary>
    /// get number of visit on image
    /// </summary>
    /// <returns></returns>
    public DataTable getVisitDueToPix()
    {
        DataTable table = null;
        try
        {
            dal.open();
            string sql = "select pixid, sum(visit) as numofvisit FROM [Visit] group by pixid";
            dal.setCommandText(sql);
            if (dal.executeQuery().Rows.Count != 0)
                table = dal.executeQuery();
        }
        catch { }
        finally { dal.close(); }
        return table;
    }

    /// <summary>
    /// get accepted user list
    /// </summary>
    /// <returns></returns>
    public DataTable getAcceptedUserList()
    {
        DataTable table = null;
        try
        {
            dal.open();
            string sql = "select username, contact from [User] where inuse=0";
            dal.setCommandText(sql);
            if (dal.executeQuery().Rows.Count != 0)
                table = dal.executeQuery();
        }
        catch { }
        finally { dal.close(); }
        return table;
    }

    /// <summary>
    /// get user's registered request
    /// </summary>
    /// <returns></returns>
    public DataTable getRegisteredUserList()
    {
        DataTable table = null;
        try
        {
            dal.open();
            string sql = "select * from [User] where type=1";
            dal.setCommandText(sql);
            if (dal.executeQuery().Rows.Count != 0)
                table = dal.executeQuery();
        }
        catch { }
        finally { dal.close(); }
        return table;
    }

    /// <summary>
    /// get number of warning message
    /// </summary>
    /// <param name="username"></param>
    /// <returns></returns>
    public string getOutstanding(string username)
    {
        string value = "0";
        try
        {
            dal.open();
            string sql = "select count(*) from [Message] where receiver=@username and outstanding=1 and responsecontent<>''";
            dal.setCommandText(sql);
            dal.addParam("username", username);
            value = dal.executeQuery().Rows[0][0].ToString();
        }
        catch { }
        finally { dal.close(); }
        return value;
    }

    /// <summary>
    /// get all pix uploaded by username
    /// </summary>
    /// <param name="username"></param>
    /// <returns></returns>
    public DataTable getPixByOwner(string username)
    {
        DataTable table = null;
        try
        {
            dal.open();
            string sql = "select pixid, path from [Pix] where owner like @username order by time desc";
            dal.setCommandText(sql);
            dal.addParam("username", username);
            if (dal.executeQuery().Rows.Count != 0)
                table = dal.executeQuery();
        }
        catch{ }
        finally { dal.close(); }
        return table;
    }

    /// <summary>
    /// get all user
    /// </summary>
    /// <returns></returns>
    public DataTable getUser(string username)
    {
        DataTable table = null;
        try
        {
            dal.open();
            string sql = "select * from [User] where type<>3 and username like @username";
            dal.setCommandText(sql);
            dal.addParam("username", username);
            if (dal.executeQuery().Rows.Count != 0)
                table = dal.executeQuery();
        }
        catch { }
        finally { dal.close(); }
        return table;
    }

    /// <summary>
    /// get search result due to the params array
    /// </summary>
    /// <param name="p"></param>
    /// <returns></returns>
    public DataTable getSearchResult(params string[] p)
    {
        DataTable table = null;
        try
        {
            dal.open();
            string sql = "select * from [Pix] where ";
            for (int i = 0; i < p.Length; i++)
            {
                sql += "title like @param" + i.ToString() + " or ";
                sql += "place like @param" + i.ToString() + " or ";
                sql += "annotation like @param" + i.ToString() + " or ";
                sql += "time like @param" + i.ToString();
                if (i != p.Length - 1)
                {
                    sql += " or ";
                }
            }
            dal.setCommandText(sql);
            for (int i = 0; i < p.Length; i++)
            {
                dal.addParam("param" + i.ToString(), "%" + p[i] + "%");
            }
            if (dal.executeQuery().Rows.Count != 0)
                table = dal.executeQuery();
        }
        catch { }
        finally { dal.close(); }
        return table;
    }

    /// <summary>
    /// check if the user is existent
    /// </summary>
    /// <param name="username"></param>
    /// <param name="password"></param>
    /// <returns></returns>
    public string userExisted(string username, string password)
    {
        dal.open();
        string sql = "select username from [User] where username=@username and password=@password";
        dal.setCommandText(sql);
        dal.addParam("username", username);
        dal.addParam("password", password);
        DataTable table = dal.executeQuery();
        dal.close();
        if (table.Rows.Count != 0)
        {
            return table.Rows[0]["username"].ToString();
        }
        return "";
    }

    /// <summary>
    /// check if the current user is logged in
    /// </summary>
    /// <param name="lnkUsername"></param>
    /// <param name="username"></param>
    /// <param name="password"></param>
    /// <returns>true if current user is logged in</returns>
    public bool isLogIn(ref LinkButton lnkUsername, string username, string password)
    {
        try
        {
            dal.open();
            string sql = "select username, inuse, ext from [User] where username=@username and password=@password and type>1";
            dal.setCommandText(sql);
            dal.addParam("username", username);
            dal.addParam("password", password);
            DataTable table = dal.executeQuery();
            dal.close();
            if (table.Rows.Count != 0)
            {
                lnkUsername.Text = table.Rows[0]["username"].ToString();
                if (table.Rows[0]["inuse"].ToString().Equals("0"))
                    updateInfo("User", "inuse", "username", "2", table.Rows[0]["username"].ToString());
                if (!table.Rows[0]["ext"].ToString().Equals(""))
                {
                    updateInfo("User", "ext", "username", "", table.Rows[0]["username"].ToString());
                    updateInfo("User", "inuse", "username", "2", table.Rows[0]["username"].ToString());
                }
                return true;
            }
        }
        catch (Exception ex)
        {
            lnkUsername.Visible = true;
            lnkUsername.Text = ex.Message;
            return false;
        }
        return false;
    }

    /// <summary>
    /// check if current user is SU
    /// </summary>
    /// <param name="username"></param>
    /// <returns>true if current user is SU</returns>
    public bool isSU(string username)
    {
        try
        {
            dal.open();
            string sql = "select username from [User] where username=@username and type=3";
            dal.setCommandText(sql);
            dal.addParam("username", username);
            DataTable table = dal.executeQuery();
            dal.close();
            if (table.Rows.Count != 0)
            {
                return true;
            }
        }
        catch { }
        return false;
    }

    /// <summary>
    /// insert user into db in which default is visitor
    /// </summary>
    /// <param name="username"></param>
    /// <param name="password"></param>
    /// <param name="fullname"></param>
    /// <param name="contact"></param>
    /// <param name="phone"></param>
    public void insertUser(string username, string password, string fullname, string contact, string phone, string profilePix)
    {
        try
        {
            dal.open();
            string sql = "insert into [User] values(@username,@password,@fullname,@contact,@phone,1,0,'',@profile)";
            dal.setCommandText(sql);
            dal.addParam("username", username);
            dal.addParam("password", password);
            dal.addParam("fullname", fullname);
            dal.addParam("contact", contact);
            dal.addParam("phone", phone);
            dal.addParam("profile", profilePix);
            dal.executeNonQuery();
        }
        catch { }
        finally { dal.close(); }
    }
    
    /// <summary>
    /// insert pix into db
    /// </summary>
    /// <param name="title"></param>
    /// <param name="place"></param>
    /// <param name="annotation"></param>
    /// <param name="type"></param>
    /// <param name="owner"></param>
    /// <param name="path"></param>
    public void insertPix(string title, string place, string time, string annotation, string type, string owner, string path)
    {
        try
        {
            string pixid = getMaxIndex("Pix", "pixid");
            string name = getMaxIndexOfName("Pix", "name");
            dal.open();
            string sql = "insert into [Pix] values(@pixid,@title,@place,@time,@annotation,0,@type,@owner,0,@path,@name)";
            dal.setCommandText(sql);
            dal.addParam("pixid", pixid);
            dal.addParam("title", title);
            dal.addParam("place", place);
            dal.addParam("time", time);
            dal.addParam("annotation", annotation);
            dal.addParam("type", type);
            dal.addParam("owner", owner);
            dal.addParam("path", path);
            dal.addParam("name", name);
            dal.executeNonQuery();
        }
        catch { }
        finally { dal.close(); }
    }

    /// <summary>
    /// insert comment on a pix into db
    /// </summary>
    /// <param name="username"></param>
    /// <param name="pixid"></param>
    /// <param name="content"></param>
    public void insertComment(string username, string pixid, string content)
    {
        try
        {
            dal.open();
            string sql = "insert into [Comment] values(@pixid,@username,@comment,'" + DateTime.Now.ToString() + "')";
            dal.setCommandText(sql);
            dal.addParam("username", username);
            dal.addParam("pixid", pixid);
            dal.addParam("comment", content);
            dal.executeNonQuery();
        }
        catch { }
        finally { dal.close(); }
    }

    /// <summary>
    /// insert rate on pix into db
    /// </summary>
    /// <param name="username"></param>
    /// <param name="pixid"></param>
    /// <param name="rate"></param>
    public void insertRate(string username, string pixid, string rate)
    {
        try
        {
            dal.open();
            string sql = "insert into [Rate] values(@pixid,@username,@rate)";
            dal.setCommandText(sql);
            dal.addParam("username", username);
            dal.addParam("pixid", pixid);
            dal.addParam("rate", rate);
            dal.executeNonQuery();
        }
        catch { }
        finally { dal.close(); }
    }

    /// <summary>
    /// insert visit on pix into db
    /// </summary>
    /// <param name="username"></param>
    /// <param name="pixid"></param>
    /// <param name="rate"></param>
    public void insertVisit(string username, string pixid)
    {
        try
        {
            dal.open();
            string sql = "insert into [Visit] values(@pixid,@username,1)";
            dal.setCommandText(sql);
            dal.addParam("username", username);
            dal.addParam("pixid", pixid);
            dal.executeNonQuery();
        }
        catch { }
        finally { dal.close(); }
    }

    /// <summary>
    /// insert friend into db
    /// </summary>
    /// <param name="owner"></param>
    /// <param name="pixid"></param>
    /// <param name="rate"></param>
    public void insertFriend(string owner, string friend, bool welcome)
    {
        try
        {
            dal.open();
            string sql = "insert into [Relation] values(@owner,@friend,@welcome)";
            dal.setCommandText(sql);
            dal.addParam("owner", owner);
            dal.addParam("friend", friend);
            dal.addParam("welcome", welcome);
            dal.executeNonQuery();
        }
        catch { }
        finally { dal.close(); }
    }

    /// <summary>
    /// insert a warning message into db
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="receiver"></param>
    /// <param name="content"></param>
    public void insertMessage(string sender, string receiver, string content)
    {
        try
        {
            string maxIndex = getMaxIndex("Message", "id");
            dal.open();
            string sql = "insert into [Message] values(@sender,@receiver,@content,0,'" + DateTime.Now.ToString() + "','',@id,0)";
            dal.setCommandText(sql);
            dal.addParam("sender", sender);
            dal.addParam("receiver", receiver);
            dal.addParam("content", content);
            dal.addParam("id", maxIndex);
            dal.executeNonQuery();
        }
        catch { }
        finally { dal.close(); }
    }

    /// <summary>
    /// update user info column
    /// </summary>
    /// <param name="username">the user name</param>
    /// <param name="column">the column name to update</param>
    /// <param name="value">the value to update</param>
    public void updateUserInfo(string username, string column, string value)
    {
        try
        {
            dal.open();
            string sql = "update [User] set "+column+" = @value where username=@username";
            dal.setCommandText(sql);
            dal.addParam("username", username);
            dal.addParam("value", value);
            dal.executeNonQuery();
        }
        catch { }
        finally { dal.close(); }
    }

    /// <summary>
    /// set need column to value where the condition column is equal to condition value
    /// </summary>
    /// <param name="dbName"></param>
    /// <param name="needColumn"></param>
    /// <param name="conditionColumn"></param>
    /// <param name="value"></param>
    /// <param name="conditionValue"></param>
    public void updateInfo(string dbName, string needColumn, string conditionColumn, string value, string conditionValue)
    {
        try
        {
            dal.open();
            string sql = "update ["+dbName+"] set " + needColumn + " = @value where "+ conditionColumn+"=@conditionValue";
            dal.setCommandText(sql);
            dal.addParam("conditionValue", conditionValue);
            dal.addParam("value", value);
            dal.executeNonQuery();
        }
        catch { }
        finally { dal.close(); }
    }

    /// <summary>
    /// increase visit by 1
    /// </summary>
    /// <param name="dbName"></param>
    /// <param name="conditionColumn"></param>
    /// <param name="conditionValue"></param>
    public void updateVisit(string dbName, string conditionValue)
    {
        try
        {
            dal.open();
            string sql = "update [Pix] set visit=visit+1 where pixid=@conditionValue";
            dal.setCommandText(sql);
            dal.addParam("conditionValue", conditionValue);
            dal.executeNonQuery();
        }
        catch { }
        finally { dal.close(); }
    }

    /// <summary>
    /// increase visit by 1
    /// </summary>
    /// <param name="dbName"></param>
    /// <param name="conditionColumn"></param>
    /// <param name="conditionValue"></param>
    public void updateVisit(string dbName, string conditionValue, string conditionValue2)
    {
        try
        {
            dal.open();
            string sql = "update [Visit] set visit=visit+1 where pixid=@conditionValue and username=@conditionValue2";
            dal.setCommandText(sql);
            dal.addParam("conditionValue", conditionValue);
            dal.addParam("conditionValue2", conditionValue2);
            dal.executeNonQuery();
        }
        catch { }
        finally { dal.close(); }
    }

    /// <summary>
    /// set need column to value where the condition column is equal to condition value and the condition column 2 is equal to condition value 2
    /// </summary>
    /// <param name="dbName"></param>
    /// <param name="needColumn"></param>
    /// <param name="conditionColumn"></param>
    /// <param name="conditionColumn2"></param>
    /// <param name="value"></param>
    /// <param name="conditionValue"></param>
    /// <param name="conditionValue2"></param>
    public void updateInfo(string dbName, string needColumn, string conditionColumn, string conditionColumn2, 
        string value, string conditionValue, string conditionValue2)
    {
        try
        {
            dal.open();
            string sql = "update [" + dbName + "] set " + needColumn + " = @value where " + conditionColumn + "=@conditionValue and "
                + conditionColumn2+"=@conditionValue2";
            dal.setCommandText(sql);
            dal.addParam("conditionValue", conditionValue);
            dal.addParam("conditionValue2", conditionValue2);
            dal.addParam("value", value);
            dal.executeNonQuery();
        }
        catch { }
        finally { dal.close(); }
    }

    /// <summary>
    /// update friend status (blacklist or friend)
    /// </summary>
    /// <param name="owner"></param>
    /// <param name="username"></param>
    /// <param name="welcome"></param>
    public void updateFriendStatus(string owner, string username, bool welcome)
    {
        try
        {
            dal.open();
            string sql = "update [Relation] set welcome=@welcome where friend=@username and owner=@owner";
            dal.setCommandText(sql);
            dal.addParam("owner", owner);
            dal.addParam("username", username);
            dal.addParam("welcome", welcome);
            dal.executeNonQuery();
        }
        catch { }
        finally { dal.close(); }
    }

    /// <summary>
    /// delete the rows fix with the condition
    /// </summary>
    /// <param name="dbName"></param>
    /// <param name="condition"></param>
    /// <param name="value"></param>
    public void deleteRow(string dbName, string condition, string value)
    {
        try
        {
            dal.open();
            string sql = "delete [" + dbName + "] where " + condition + "=@value";
            dal.setCommandText(sql);
            dal.addParam("value", value);
            dal.executeNonQuery();
        }
        catch { }
        finally { dal.close(); }
    }

    /// <summary>
    /// delete the rows fix with the condition
    /// </summary>
    /// <param name="dbName"></param>
    /// <param name="condition"></param>
    /// <param name="value"></param>
    public void deleteRow(string dbName, string condition, string condition2, string value, string value2)
    {
        try
        {
            dal.open();
            string sql = "delete [" + dbName + "] where " + condition + "=@value and " + condition2 + "=@value2";
            dal.setCommandText(sql);
            dal.addParam("value", value);
            dal.addParam("value2", value2);
            dal.executeNonQuery();
        }
        catch { }
        finally { dal.close(); }
    }
}