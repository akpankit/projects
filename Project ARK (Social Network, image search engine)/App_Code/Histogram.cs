﻿// Histogram.cs
// compile with: /unsafe
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;

/// <summary>
/// Summary description for Histogram
/// </summary>
public class Histogram
{
    private Bitmap bmpImg;
    public int[] histogram;
	public Histogram()
	{
		//
		// TODO: Add constructor logic here
		//
	}
        
    public void setImage(Bitmap bmp)
    {
        bmpImg = (Bitmap)bmp.Clone();
    }

    public Bitmap getImage()
    {
        return (Bitmap)bmpImg.Clone();
    }

    public void calcHisto(System.Web.UI.WebControls.Image img)
    {
        BitmapData data = bmpImg.LockBits(new System.Drawing.Rectangle(0, 0, bmpImg.Width, bmpImg.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

        unsafe
        {
            byte* ptr = (byte*)data.Scan0;

            int remain = data.Stride - data.Width * 3;

            histogram = new int[256];
            for (int i = 0; i < histogram.Length; i++)
                histogram[i] = 0;

            for (int i = 0; i < data.Height; i++)
            {
                for (int j = 0; j < data.Width; j++)
                {
                    int mean = ptr[0] + ptr[1] + ptr[2];
                    mean /= 3;

                    histogram[mean]++;
                    ptr += 3;
                }

                ptr += remain;
            }

            drawHistogram(histogram, img);
        }

        bmpImg.UnlockBits(data);
    }

    public void drawHistogram(int[] histogram, System.Web.UI.WebControls.Image img)
    {

        Bitmap bmp = new Bitmap(histogram.Length + 10, 310);
        bmp.Save(img.AlternateText +".bmp");
        img.ImageUrl = "~" + img.AlternateText.Substring(img.AlternateText.IndexOf(@"\Upload Images")) + ".bmp";

        BitmapData data = bmp.LockBits(new System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

        unsafe
        {
            int remain = data.Stride - data.Width * 3;
            byte* ptr = (byte*)data.Scan0;

            for (int i = 0; i < data.Height; i++)
            {

                for (int j = 0; j < data.Width; j++)
                {
                    ptr[0] = ptr[1] = ptr[2] = 150;
                    ptr += 3;
                }
                ptr += remain;

            }

            int max = 0;
            for (int i = 0; i < histogram.Length; i++)
            {

                if (max < histogram[i])
                    max = histogram[i];

            }

            for (int i = 0; i < histogram.Length; i++)
            {
                ptr = (byte*)data.Scan0;
                ptr += data.Stride * (305) + (i + 5) * 3;

                int length = (int)(1.0 * histogram[i] * 300 / max);

                for (int j = 0; j < length; j++)
                {
                    ptr[0] = 255;
                    ptr[1] = ptr[2] = 0;
                    ptr -= data.Stride;
                }

            }

        }

        bmp.UnlockBits(data);
        bmp.Save(img.AlternateText + ".bmp");
        img.ImageUrl = "~" + img.AlternateText.Substring(img.AlternateText.IndexOf(@"\Upload Images")) + ".bmp";
    }

    public bool compareTo(int[]hist)
    {
        int sum = 0;
        for (int i = 0; i < histogram.Length; i++)
        {
            sum += Math.Abs(histogram[i] - hist[i]);
        }
        if (sum <= 2 * 256) return true;
        return false;
    }
}