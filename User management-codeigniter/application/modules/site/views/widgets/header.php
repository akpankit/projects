<head>

	<meta charset="utf-8">
	<title><?php echo $this->template->title; ?> &middot; <?php echo $site_name; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	
	<!-- Le styles -->
	<link href="<?php echo base_url();?>css/misty/misty-bootstrap.css" rel="stylesheet">
	<link href="<?php echo base_url();?>css/misty/misty-style.css" rel="stylesheet">
	<link href="<?php echo base_url();?>css/misty/misty-font-awesome.min.css" rel="stylesheet">
	<?php echo $this->template->stylesheet; ?>
	
	<!-- Le fav icon -->
	<link rel="shortcut icon" href="<?php echo base_url().'favicon.png';?>">
	
	<!-- Le scripts -->
	<script type="text/javascript">
	    var CI = {
	       'base_url': '<?php echo base_url(); ?>'
	    };
	</script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="<?php echo base_url();?>js/jquery-1.9.1.min.js"><\/script>')</script>
	<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
	<?php echo $this->template->javascript; ?>
	
</head>