<!DOCTYPE html>
<html lang="en">

	<?php echo $this->template->widget('error_header'); ?>

	<body>
	
		<?php echo $this->template->widget("site_navigation"); ?>
		
		<div class="container">
		
			<div class="hero-unit">
				<h2>Premium Page</h2>	
				<p>Sorry, this is a premium page.</p>
				<p>You have to be granted access to it by the administrator.</p>
				<a href="javascript:history.go(-1);">Go back from whence you came</a>
			</div>
			
		</div>
		
	</body>
	
</html>