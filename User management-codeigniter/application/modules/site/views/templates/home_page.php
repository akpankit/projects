<!DOCTYPE html>
<html lang="en" class="no-js">

	<?php echo $this->template->widget('site_header'); ?>

	<body>
	
		<?php echo $this->template->widget("site_navigation"); ?>
		
		<?php if(!empty($message)) { ?>
			<div class="alert alert-error">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?php echo $message; ?>
			</div>
		<?php } if($this->session->flashdata('message')) { ?>
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?php echo $this->session->flashdata('message'); ?>
			</div>
		<?php } if($this->session->flashdata('error')) { ?>
			<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $this->session->flashdata('error'); ?>
			</div>
		<?php } ?>
		
		<div class="container">
			
			<?php echo $this->template->widget("site_breadcrumbs_and_subnav"); ?>
			
			<?php echo $this->template->content; ?>
			
		</div>
		
		<?php echo $this->template->widget("site_footer"); ?>
		
	</body>
	
</html>