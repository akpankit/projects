<div class="row">
	<!-- start: Page section -->
    <section id="page-sidebar" class="span6 pull-left">
    	<div class="page-inner">
			<div class="sub-inner">
				<div class="row-fluid">
					<div id="content">
						<div class="content-bottom">
						
							<?php echo $details->page_content_left; ?>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- end: Page section -->
	
	<!-- start: Page section -->
    <section id="page-sidebar" class="span6 pull-right">
    	<div class="page-inner">
			<div class="sub-inner">
				<div class="row-fluid">
					<div id="content">
						<div class="content-bottom">
						
							<?php echo $details->page_content_right; ?>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- end: Page section -->
	
</div>