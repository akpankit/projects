<?php
class Site extends MX_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','html','url'));
		$this->load->library(array('form_validation','route_lib','auth_lib'));
		$this->load->model('admin/pages_model');
		$this->load->model('site_model');
	}
	
	/**
	* Load the homepage of the system
	* Based on the settings. Any page can be loaded as the homepage
	* 
	*/
	function index()
	{
		redirect('site/home');
	}
	
	/**
	* Generate the main nav for the frontend of the system
	* 
	*/
	function show_main_nav()
	{
		$page = explode("/",uri_string());
		$page_urls = $this->site_model->get_parent_page_details();
		foreach($page_urls as $page_url) {
			if(isset($page[2]) && $page[2] == $page_url->page_url || isset($page[2]) && $this->site_model->is_my_child($page_url->page_id,$page[2]) ||
				$page[1] == $page_url->page_url ||  $this->site_model->is_my_child($page_url->page_id,$page[1])) {
				if($children = $this->site_model->page_has_children($page_url->page_url)) {
					echo("<li class='active hidden-desktop'>".anchor("$page_url->page_path/$page_url->page_url",$page_url->page_title));
					echo("<li class='active dropdown hidden-tablet hidden-phone'>".anchor("$page_url->page_path/$page_url->page_url",$page_url->page_title."<b class='caret'></b>",array('class'=>'active','data-toggle'=>''))."");
					echo("<ul class='dropdown-menu'>");
					foreach($children as $child)
						echo("<li>".anchor("$child->page_path/$child->page_url",$child->page_title)."</li>");
					echo("</ul></li>");
				} else {
					echo("<li class='active'>".anchor("$page_url->page_path/$page_url->page_url",$page_url->page_title)."</li>");
				}
			} else {
				if($children = $this->site_model->page_has_children($page_url->page_url)) {
					echo("<li class='hidden-desktop'>".anchor("$page_url->page_path/$page_url->page_url",$page_url->page_title));
					echo("<li class='dropdown hidden-tablet hidden-phone'>".anchor("$page_url->page_path/$page_url->page_url",$page_url->page_title."<b class='caret'></b>",array('class'=>'','data-toggle'=>''))."");
					echo("<ul class='dropdown-menu'>");
					foreach($children as $child)
						echo("<li>".anchor("$child->page_path/$child->page_url",$child->page_title)."</li>");
					echo("</ul></li>");
				} else {
					echo("<li>".anchor("$page_url->page_path/$page_url->page_url",$page_url->page_title)."</li>");
				}
			}
        }
	}
	
	/**
	 * Generate the subnav for a page
	 *
	 */
	function show_subnav()
	{
		$page = explode("/",uri_string());
		if(isset($page[2]))
			$page_url = $page[2];
		else
			$page_url = $page[1];
		if($children = $this->site_model->page_has_children($page_url)) {
			echo 'Submenu: ';
			foreach($children as $child)
				echo anchor("$child->page_path/$child->page_url",$child->page_title).' / ';
		}
	}
	
	/**
	* Load the homepage of the system
	* 
	*/
	function home()
	{
		if(!$this->auth_lib->is_logged_in())
			   $this->route_lib->redirect_with_message('site/auth/login',$this->lang->line('auth_login_first'));
			else
				if(!$this->pages_model->has_access('home',$this->session->userdata('user_id'),$this->session->userdata('user_level')))
					redirect('site/unauthorised');
					
		// Set the template to use for this page
		$this->template->set_template('templates/home_page');
		
		// Set the title of the page
        $this->template->title->set('Home');
		
		// Get the details to be displayed on the page	
		$data['details'] = $this->site_model->get_page_details('home');
		
		if(!is_null($data['details']) && $this->site_model->is_page_active('home')) {
			$this->template->content->view($data['details']->layout_name, $data);
			$this->template->publish();
			
			// Register a new page view
			//$this->site_model->increment_page_view($data['details']->page_uuid);
		} else {
			$this->template->title->set('Error');
			$this->load->view('page_not_found');
		}
	}
	
	/**
	* Load a particular page based on the URL field of the page
	* This is for pages generated in the backend not the default bundled pages
	* Those use the URL directly e.g. site/home
	* 
	* @param string page_url
	* 
	*/
	function page($page_url)
	{
		if($this->site_model->is_premium_page($page_url)) {
			if(!$this->auth_lib->is_logged_in())
			   $this->route_lib->redirect_with_message('site/auth/login',$this->lang->line('auth_login_first'));
			else
				if(!$this->pages_model->has_access($page_url,$this->session->userdata('user_id'),$this->session->userdata('user_level')))
					redirect('site/unauthorised');
		}
		// Set the template to use for this page
		$this->template->set_template('templates/user_page');
		
		// Get the details to be displayed on the page
		$data['details'] = $this->site_model->get_page_details($page_url);
		
		// Set the title of the page
		$this->template->title->set($data['details']->page_title);
		
		if(!is_null($data['details']) && $this->site_model->is_page_active($page_url)) {
			$this->template->content->view($data['details']->layout_name, $data);
			$this->template->publish();
			
			// Register a new page view
			$this->site_model->increment_page_view($data['details']->page_uuid);
		} else {
			$this->template->title->set('Error');
			$this->template->content->view('premium_page_error');
			$this->template->publish();
		}
	}
	
	/**
	 * Show the unauthorised page if a user does not have permission to access a page
	 *
	 */
	function unauthorised()
	{
		// Set the template to use for this page
		$this->template->set_template('templates/user_page');
		$this->template->title->set('Restricted');
		$this->template->content->view('page_not_found');
		$this->template->publish();
	}
}