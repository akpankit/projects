<?php
class Site_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	/**
	* Get all pages from the DB 
	*/
	function get_pages()
	{
		$this->db->select('*');
		$this->db->from('pages');
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return $query->result();
		else
			return NULL;
	}
	
	/**
	* Get the URLs and titles of pages that are parents
	* Used when displaying the frontend main nav
	*
	*/
	function get_parent_page_details()
	{
		$this->db->select('page_id,page_path,page_url,page_title');
		$this->db->from('pages');
		$this->db->where('active',1);
		$this->db->where('parent',0);
		$this->db->order_by('page_order','asc');
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return $query->result();
		else
			return NULL;
	}
	
	/**
	* Get the details of a page for display on the frontend 
	* 
	* @param string page_url
	* @return object
	* 
	*/
	function get_page_details($page_url)
	{
		$this->db->select('p.page_uuid,p.page_title,p.page_content_left,p.page_content_right,p.page_url,pl.layout_name');
		$this->db->from('pages as p');
		$this->db->join('page_layouts as pl','p.page_layout = pl.layout_id','INNER');
		$this->db->where('p.page_url',$page_url);
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return $query->row();
		else
			return NULL;
	}
	
	/**
	* Get the homepage to be loaded when the user opens the system
	* 
	* @return string
	*
	*/
	function get_homepage_url()
	{
		$this->db->select('s.value,p.page_path,p.page_url');
		$this->db->from('settings as s');
		$this->db->where('s.code','homepage');
		$this->db->join('pages as p','s.value = p.page_url','INNER');
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return $query->row('page_path').'/'.$query->row('page_url');
		else
			return NULL;
	}
	
	/**
	* Get contact details for use in the frontend
	* 
	* @return string
	* 
	*/
	function get_contact_details()
	{
		$this->db->select('code,value');
		$this->db->from('settings');
		$this->db->where('code','address');
		$this->db->or_where('code','phone');
		$this->db->or_where('code','fax');
		$this->db->or_where('code','contact_email');
		$query = $this->db->get();
		if($query->num_rows() != 0) {
			foreach($query->result() as $row)
				$data[$row->code] = $row->value;
			return $data;
		} else {
			return NULL;
		}
	}
	
	/**
	* Get the details of a post
	* For display etc.
	*  
	* @param int $post_id
	* 
	*/
	function get_post_details($post_url)
	{
		$this->db->select('p.post_id,p.post_uuid,p.post_title,p.post_url,p.post_content,p.submitted,u.fullname');
		$this->db->from('posts as p');
		$this->db->join('users as u','p.submitted_by = u.user_id','INNER');
		$this->db->where('p.post_url',$post_url);
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return $query->row();
		else
			return NULL;
	}
	
	/**
	* Confirm that a page is activated
	*
	* @param string page_url
	* 
	*/
	function is_page_active($page_url)
	{
		$this->db->select("1",FALSE);
		$this->db->from('pages');
		$this->db->where('page_url',$page_url);
		$this->db->where('active',1);
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return TRUE;
		else
			return FALSE;
	}
	
	/**
	* Get the contact email address
	* Can be used when sending mail from the contact form_button
	*
	* @return string
	*
	*/
	function get_contact_email()
	{
		$this->db->select('value');
		$this->db->from('settings');
		$this->db->where('code','contact_email');
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return $query->row('value');
		else
			return NULL;	
	}
	
	/**
	* Get the name of the site from the DB for display on pages
	* 
	* @return string 
	* 
	*/
	function get_site_name()
	{
		$this->db->select('setting_value');
		$this->db->from('auth_settings');
		$this->db->where('short_name','website_name');
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return $query->row('setting_value');
		else
			return NULL;
	}
	
	/**
	* Increment page view statistics
	* Called every time a page is loaded to give page view insights
	* 
	* @param int page_uuid
	* 
	*/
	function increment_page_view($page_uuid)
	{
		$this->db->select('views');
		$this->db->from('page_views');
		$this->db->where('page',$page_uuid);
		$views = $this->db->get()->row('views');
		$data = array('views'=>$views+1);
		$this->db->where('page',$page_uuid);
		$this->db->update('page_views',$data);
	}
	
	/**
	 * Get the ID of a particular page given its URL
	 * Used when constructing the frontend navigation
	 *
	 * @param string page_url
	 * @return int page_id
	 *
	 */
	function get_page_id($page_url)
	{
		$this->db->select('page_id');
		$this->db->from('pages');
		$this->db->where('page_url',$page_url);
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return $query->row('page_id');
		else
			return NULL;
	}
	
	/**
	 * Check if a page has children
	 * If so return some information about all the children
	 *
	 * @param string page_url
	 * @return array
	 *
	 */
	function page_has_children($page_url)
	{
		$page_id = $this->get_page_id($page_url);
		$this->db->select('page_title,page_url,page_path');
		$this->db->from('pages');
		$this->db->where('parent',$page_id);
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return $query->result();
		else
			return NULL;
	}
	
	/**
	 * Check if a page is a child of another page based on it URL and the ID of the parent page
	 *
	 * @param int parent_id The ID of the parent page
	 * @param string page_url The URL to be tested
	 * @return bool
	 *
	 */
	function is_my_child($parent_id,$child_url)
	{
		$child_id = $this->get_page_id($child_url);
		$this->db->select('parent');
		$this->db->from('pages');
		$this->db->where('page_id',$child_id);
		$this->db->where('parent',$parent_id);
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return TRUE;
		else
			return FALSE;
	}
	
	/**
	 * Check if a page is a premium page given the page URL
	 *
	 * @param string page_url
	 * @return bool
	 *
	 */
	function is_premium_page($page_url)
	{
		$this->db->select('premium')->from('pages')->where('premium',1)->where('page_url',$page_url);
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return TRUE;
		else
			return FALSE;
	}
}