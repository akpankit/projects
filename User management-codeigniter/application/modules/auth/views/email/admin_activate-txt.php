Hi Admin,

This is to inform you that <?php echo $fullname; ?> has registered on the system.
Please <?php echo anchor('auth/admin_email_activate/'.$user_key,' click here ');?> to activate their account.

Thank you for using the system.

The IT Team.