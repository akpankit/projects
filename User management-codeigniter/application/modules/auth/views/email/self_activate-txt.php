Hi <?php echo $fullname; ?>,

Thank you for registering with us. Below is the account activation code:
Please <?php echo anchor("auth/email_activate/".$user_key, "click here");?> to activate your account.
Link doesn't work? Copy and paste this link into your browser address bar:

<?php echo anchor(base_url().'auth/email_activate/'.$user_key,base_url().'auth/email_activate/'.$user_key); ?>


Thank you for using the system.

The IT Team.