Hi <?php echo $fullname; ?>,

Forgot your password huh? No worries:
Please <?php echo anchor("auth/reset_password/".$user_key, "click here");?> to reset it.
Link doesn't work? Copy and paste this link into your browser address bar:

<?php echo anchor(base_url().'auth/reset_password/'.$user_key,base_url().'auth/reset_password/'.$user_key); ?>

Thank you for using the system.

The IT Team.