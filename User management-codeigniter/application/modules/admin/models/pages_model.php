<?php
class Pages_model extends CI_Model
{
	private $pages_table = 'pages';	// The pages table
	
	function __construct()
	{
		parent::__construct();
	}
	
	/**
	* Return a list of all details about all pages in the system
	* Excludes pages created by plugins
	* 
	* @param int num
	* @param int offset
	* @return object
	*  
	*/
	function get_all_pages($num,$offset)
	{
		$this->db->select('*');
		$this->db->from($this->pages_table);
		$this->db->where('plugin_id',0);
		$query = $this->db->get('',$num,$offset);
		if($query->num_rows() != 0)
			return $query->result();
		else
			return NULL;
	}
	
	/**
	* Return a list of all pages and their authors
	* Excludes pages created by plugins
	* 
	* @param int num
	* @param int offset
	* @return object
	* 
	*/
	function get_all_pages_list($num,$offset)
	{
		$this->db->select('p.page_uuid,p.page_title,p.page_path,p.page_url,p.active,p.page_content_left,p.page_content_right,p.bundled,p.created,p.modified,u.fullname');
		$this->db->from('pages as p');
		$this->db->where('p.plugin_id',0);
		$this->db->join('users as u','p.author = u.user_id','INNER');
		$query = $this->db->get('',$num,$offset);
		if($query->num_rows() != 0)
			return $query->result();
		else
			return NULL;
	}
	
	/**
	* Get all details about a particular page
	* 
	* @param int page_uuid
	* @return object
	* 
	*/
	function get_page_details($page_uuid)
	{
		$this->db->select('*');
		$this->db->from($this->pages_table);
		$this->db->where('page_uuid',$page_uuid);
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return $query->row();
		else
			return NULL;
	}
	
	/**
	* Save the content of a new page to the database
	* 
	*/
	function save_page()
	{
		do
			$uuid = $this->_generate_uuid(7);
		while(!$this->is_uuid_unique($uuid));
		$title = explode(" ",strtolower($this->input->post('page_title')));
		$ct = count($title);
		$url = '';
		for($i=0;$i<$ct;$i++)
			if($i == 0)
				$url .= $title[$i];
			else
				$url .= '_'.$title[$i];
		// Convert accented characters for standards compliance
		$url = convert_accented_characters($url);
		$data = array('page_uuid'=>$uuid,'page_order'=>$this->input->post('page_order'),'page_layout'=>$this->input->post('page_layout'),
			'page_title'=>$this->input->post('page_title'),'page_content_left'=>$this->input->post('page_content_left'),
			'page_content_right'=>$this->input->post('page_content_right'),'page_path'=>'site/page','page_url'=>$url,
			'parent'=>$this->input->post('parent'),'author'=>$this->session->userdata('user_id'),'created'=>date('Y-m-d H:i:s'));
		$this->db->insert($this->pages_table,$data);// Insert page details into the DB
		$views = array('page'=>$uuid);
		$this->db->insert('page_views',$views);		// Create a page view record
	}
	
	/**
	* Activate a page so that it is accessible to all
	*
	* @param int page_uuid
	* 
	*/
	function activate_page($page_uuid)
	{
		$data = array('active'=>1);
		$this->db->where('page_uuid',$page_uuid);
		$this->db->update('pages',$data);
	}
	
	/**
	* Deactivate a page so that it is not accessible to anybody
	* 
	* @param int page_uuid
	* 
	*/
	function deactivate_page($page_uuid)
	{
		$data = array('active'=>0);
		$this->db->where('page_uuid',$page_uuid);
		$this->db->update('pages',$data);
	}
	
	/**
	* Update the content of a particular page 
	* 
	* @param int page_uuid
	* 
	*/
	function edit_page($page_uuid)
	{
		$title = explode(" ",strtolower($this->input->post('page_title')));
		$ct = count($title);
		$url = '';
		for($i=0;$i<$ct;$i++)
			if($i == 0)
				$url .= $title[$i];
			else
				$url .= '_'.$title[$i];
		// Convert accented characters for standards compliance
		$url = convert_accented_characters($url);
		$data = array('page_order'=>$this->input->post('page_order'),'page_layout'=>$this->input->post('page_layout'),
			'page_title'=>$this->input->post('page_title'),'page_content_left'=>$this->input->post('page_content_left'),
			'parent'=>$this->input->post('parent'),'page_content_right'=>$this->input->post('page_content_right'),
			'page_path'=>'site/page','page_url'=>$url,'premium'=>$this->input->post('premium_page'));
		$this->db->where('page_uuid',$page_uuid);
		$this->db->update($this->pages_table,$data);
	}
	
	/**
	* Update the content of a system page 
	* 
	* @param int page_uuid
	* 
	*/
	function edit_system_page($page_uuid)
	{
		$data = array('page_order'=>$this->input->post('page_order'),'page_layout'=>$this->input->post('page_layout'),
			'page_content_left'=>$this->input->post('page_content_left'),'page_content_right'=>$this->input->post('page_content_right'));
		$this->db->where('page_uuid',$page_uuid);
		$this->db->update($this->pages_table,$data);
	}
	
	/**
	* Delete a page from the DB
	* 
	* @param int page_uuid
	* 
	*/
	function delete_page($page_uuid)
	{
		if($this->is_bundled_page($page_uuid)) {
			$this->route_lib->redirect_with_error('pages','Sorry. You can not delete this page.');
		} else {
			$this->db->delete($this->pages_table,array('page_uuid'=>$page_uuid)); 	// Delete page details
			$this->db->delete('page_views',array('page'=>$page_uuid)); 				// Delete page view records
		}
	}
	
	/**
	* Generate a random uuid of given length
	* 
	* @param int length (of the random number to be generated)
	* @return string 
	* 
	*/
	function _generate_uuid($length)
	{
		$random = '';
		for($i=0;$i<$length;$i++)
			$random .= mt_rand(0,9);
		return $random;
	}
	
	/**
	* Check if a generated uuid is duplicated in the DB
	* Just to be safe
	* 
	* @param int uuid
	* @return bool
	* 
	*/
	function is_uuid_unique($page_uuid)
	{
		$this->db->select('1',FALSE);
		$this->db->from($this->pages_table);
		$this->db->where('page_uuid',$page_uuid);
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return FALSE;
		else
			return TRUE;
	}
	
	/**
	* Check if a page is a bundled (system) page
	* 
	* @param int page_uuid
	* @return bool
	* 
	*/
	function is_bundled_page($page_uuid)
	{
		$this->db->select('1',FALSE);
		$this->db->from($this->pages_table);
		$this->db->where('page_uuid',$page_uuid);
		$this->db->where('bundled',1);
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return TRUE;
		else
			return FALSE;
	}
	
	/**
	* Get a list of all available page layouts
	* 
	* @return object 
	* 
	*/
	function get_page_layouts()
	{
		$this->db->select('*');
		$this->db->from('page_layouts');
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return $query->result();
		else
			return NULL;
	}
	
	/**
	 * Get all pages that have no parents
	 * These are pages that can be assigned children that will then appear in a dropdown
	 * Called when creating and editing a page
	 *
	 * @return array
	 *
	 */
	function get_parent_pages()
	{
		$this->db->select('page_id,page_title');
		$this->db->from('pages');
		$this->db->where('parent',0);
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return $query->result();
		else
			return NULL;
	}
	
	/**
	 * Get all access permissions for a page
	 *
	 * @param int page_uuid
	 * @return array
	 *
	 */
	function get_page_permissions($page_uuid)
	{
		$this->db->select('*');
		$this->db->fron('page_permissions');
		$this->db->where('page_uuid',$page_uuid);
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return $query->result();
		else
			return NULL;
	}
	
	/**
	 * Get a list of all roles/user levels in the system
	 *
	 * @return array
	 *
	 */
	function get_all_roles()
	{
		$this->db->select('*');
		$this->db->from('user_level');
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return $query->result();
		else
			return NULL;
	}
	
	/**
	 * Get a list of all users in the system
	 *
	 * @return array
	 *
	 */
	function get_all_users()
	{
		$this->db->select('user_id,fullname');
		$this->db->from('users');
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return $query->result();
		else
			return NULL;
	}
	
	/**
	 * Get a list of all user roles/levels that have explicit page rules tied to them
	 *
	 * @return array
	 * 
	 */
	function get_all_roles_with_rules()
	{
		$this->db->distinct('pr.role_id');
		$this->db->select('pr.role_id,ul.user_level');
		$this->db->from('page_rules as pr');
		$this->db->join('user_level as ul','pr.role_id = ul.u_level_id','INNER');
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return $query->result();
		else
			return NULL;
	}
	
	/**
	 * Get all rules tied to a particular role by role_id
	 *
	 * @param int role_id
	 * @return array
	 *
	 */
	function get_role_rules($role_id)
	{
		$this->db->select('pr.rule_id,pr.rule,ul.user_level,ul.parent,p.page_id,p.page_title,p.page_url,arv.value');
		$this->db->from('page_rules as pr');
		$this->db->where('pr.user_id',NULL);
		$this->db->where('pr.role_id',$role_id);
		$this->db->join('user_level as ul','pr.role_id = ul.u_level_id','INNER');
		$this->db->join('pages as p','pr.page_id = p.page_id','INNER');
		$this->db->join('acl_rule_values as arv','pr.rule = arv.rule','INNER');
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return $query->result();
		else
			return NULL;
	}
	
	/**
	 * Get all rules tied to a particular user by user_id
	 *
	 * @param int user_id
	 * @return array
	 *
	 */
	function get_user_rules($user_id)
	{
		$this->db->select('pr.rule_id,pr.rule,u.fullname,p.page_id,p.page_title,p.page_url,arv.value');
		$this->db->from('page_rules as pr');
		$this->db->where('pr.user_id',$user_id);
		$this->db->where('pr.role_id',NULL);
		$this->db->join('users as u','pr.user_id = u.user_id','INNER');
		$this->db->join('pages as p','pr.page_id = p.page_id','INNER');
		$this->db->join('acl_rule_values as arv','pr.rule = arv.rule','INNER');
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return $query->result();
		else
			return NULL;
	}
	
	/**
	 * Get a list of all users that have explicit page rules tied to them
	 *
	 * @return array
	 * 
	 */
	function get_all_users_with_rules()
	{
		$this->db->distinct('pr.user_id');
		$this->db->select('pr.user_id,u.fullname');
		$this->db->from('page_rules as pr');
		$this->db->join('users as u','pr.user_id = u.user_id','INNER');
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return $query->result();
		else
			return NULL;
	}
	
	/**
	 * Get a list of all pages that are children of a particular page
	 *
	 * @param int page_id
	 * @return array
	 *
	 */
	function get_page_children($page_id)
	{
		$this->db->select('page_id,page_title,page_url');
		$this->db->from('pages');
		$this->db->where('parent',$page_id);
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return $query->result();
		else
			return NULL;
	}
	
	/**
	 * Check if a user has access by both user and user level
	 *
	 * @param string page_url
	 * @param int user_id
	 * @param int role_id
	 * @return bool
	 *
	 */
	function has_access($page_url,$user_id,$role_id)
	{
		if($this->user_has_access($page_url,$user_id) || $this->role_has_access($page_url,$role_id))
			return TRUE;
		else
			return FALSE;
	}
	
	/**
	*	Check if a role has access to a page
	* 
	*	@param string page_url
	*	@param int role_id
	* 
	*/
	function role_has_access($page_url,$role_id)
	{
		$parents = json_decode($this->get_role_parents($role_id));
		$inherited_access = 0;
		if($parents != '0'){
			$cp = count($parents);
			for($i=0;$i<$cp;$i++){
				$inherited[] = $this->get_inherited_role_rules($parents[$i]);
			}
			if(!empty($inherited)){
				$ci = count($inherited);
				for($i=0;$i<$ci;$i++){
					if($inherited[$i]){
						foreach($inherited[$i] as $rules){
							if($rules->rule == 1 AND $rules->url == $page_url){
								$inherited_access = 1;
							}
						}
					}
				}
			}
		}
		$page_id = $this->get_page_id($page_url);
		if(!empty($page_id)){
			/* 	
			*	Check if the user's role has access to the resource  
			*	param int $role_id
			*/
			$this->db->select('rule');
			$this->db->from('page_rules');
			$this->db->where('page_id',$page_id);
			$this->db->where('rule','1');
			$this->db->where('role_id',$role_id);
			$role_access = $this->db->get();
			if($role_access->num_rows() != 0 OR $inherited_access == 1){
				return TRUE;
			} else {
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}
	
	/**
	* Check if a user has access to page
	*
	* @param string page_url
	* @param int user_id
	*
	*/
	function user_has_access($page_url,$user_id)
	{
		if($page_id = $this->get_page_id($page_url)){
			/* 	
			*	Check if the user has access to the resource 
			*	param int $user_id 
			*/
			$this->db->select('rule_id');
			$this->db->from('page_rules');
			$this->db->where('page_id',$page_id);
			$this->db->where('rule',1);
			$this->db->where('user_id',$user_id);
			$user_access = $this->db->get();
			if($user_access->num_rows() != 0){
				return TRUE;
			}else{
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Get the id of a page given its url
	 *
	 * @param string page_url
	 * @return int page_id
	 *
	 */
	function get_page_id($page_url)
	{
		$this->db->select('page_id');
		$this->db->from('pages');
		$this->db->where('page_url',$page_url);
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return $query->row('page_id');
		else
			return NULL;
	}
	
	/**
	 * Get the parents of a user role/level
	 *
	 * @param int role_id
	 * @return array
	 *
	 */
	function get_role_parents($role_id)
	{
		$this->db->select('parent');
		$this->db->from('user_level');
		$this->db->where('u_level_id',$role_id);
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return $query->row('parent');
		else
			return NULL;
	}
	
	/**
	 * Get all rules that a role inherits from its parent
	 *
	 * @param int role_id
	 * @return array
	 *
	 */
	function get_inherited_role_rules($role_id)
	{
		$this->db->select('pr.rule_id,pr.role_id,pr.rule,ul.user_level,ul.parent,ar.resource_name,ar.url,arv.value');
		$this->db->from('page as pr');
		$this->db->where('pr.user_id',NULL);
		$this->db->where('pr.role_id',$role_id);
		$this->db->join('user_level as ul','pr.role_id = ul.u_level_id','INNER');
		$this->db->join('pages as p','p.page_id = ar.resource_id','INNER');
		$this->db->join('acl_rule_values as arv','ac.rule = arv.rule','INNER');
		$query = $this->db->get();
		if($query->num_rows() != 0)
			return $query->result();
		else
			return NULL;
	}
	
	/**
	 * Add a new rule that is tied to a particular role
	 *
	 */
	function add_role_rule()
	{
		$pages = $_POST['page_id'];
		$counter = count($pages);
		for($i=0;$i<$counter;$i++){
			$this->db->select('rule_id');
			$this->db->from('page_rules');
			$this->db->where('page_id',$pages[$i]);
			$this->db->where('role_id',$this->input->post('role_id'));
			$query = $this->db->get();
			if($query->num_rows() != 0){
				$data = array('rule'=>$this->input->post('rule'));
				$this->db->where('page_id',$resources[$i]);
				$this->db->where('role_id',$this->input->post('role_id'));
				$this->db->update('page_rules',$data);
			}else{
				$data = array('role_id'=>$this->input->post('role_id'),'page_id'=>$pages[$i],'rule'=>$this->input->post('rule'));
				$this->db->insert('page_rules',$data);
			}
		}
	}
	
	/**
	 * Add a new rule that is tied to a particular user
	 *
	 */
	function add_user_rule()
	{
		$pages = $_POST['page_id'];
		$counter = count($pages);
		for($i=0;$i<$counter;$i++){
			$this->db->select('rule_id');
			$this->db->from('page_rules');
			$this->db->where('page_id',$pages[$i]);
			$this->db->where('user_id',$this->input->post('user_id'));
			$query = $this->db->get();
			if($query->num_rows() != 0){
				$data = array('rule'=>$this->input->post('rule'));
				$this->db->where('page_id',$resources[$i]);
				$this->db->where('user_id',$this->input->post('user_id'));
				$this->db->update('page_rules',$data);
			}else{
				$data = array('user_id'=>$this->input->post('user_id'),'page_id'=>$pages[$i],'rule'=>$this->input->post('rule'));
				$this->db->insert('page_rules',$data);
			}
		}
	}
	
	/**
	 * Allow a user rule
	 *
	 * @param int rule_id
	 *
	 */
	function allow_user_rule($role_id)
	{
		$this->db->where('rule_id',$role_id);
		$this->db->update('page_rules',array('rule'=>1));
	}
	
	/**
	 * Deny a user rule
	 *
	 * @param int rule_id
	 *
	 */
	function deny_user_rule($role_id)
	{
		$this->db->where('rule_id',$role_id);
		$this->db->update('page_rules',array('rule'=>0));
	}
	
	/**
	 * Allow a role rule
	 *
	 * @param int rule_id
	 *
	 */
	function allow_role_rule($role_id)
	{
		$this->db->where('rule_id',$role_id);
		$this->db->update('page_rules',array('rule'=>1));
	}
	
	/**
	 * Deny a role rule
	 *
	 * @param int rule_id
	 *
	 */
	function deny_role_rule($role_id)
	{
		$this->db->where('rule_id',$role_id);
		$this->db->update('page_rules',array('rule'=>0));
	}
	
	/**
	 * Delete a rule
	 *
	 * @param int rule_id
	 *
	 */
	function delete_rule($role_id)
	{
		$this->db->delete('acl_rules',array('rule_id'=>$role_id));
	}
}