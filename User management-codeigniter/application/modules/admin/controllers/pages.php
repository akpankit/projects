<?php
require_once "./application/modules/admin/controllers/admin.php";
class Pages extends Admin
{
	function __construct()
	{
		parent::__construct();
		$this->load->library(array('route_lib'));
		$this->load->model('pages_model');
		$this->load->helper('text');
	}
	
	/**
	*  Load the default view for the module
	* 
	*/
	function index()
	{
		// Set the template to use for this page
		$this->template->set_template('templates/admin_template');
		
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/admin/pages/index';
		$config['total_rows'] = $this->db->count_all('pages');
		$config['uri_segment'] = 4;
		$config['per_page'] = 10;
		$config['num_links'] = 5;
		$config['full_tag_open'] = '<p>';
		$config['full_tag_close'] = '</p>';
		$this->pagination->initialize($config);
		
		$this->load->library('table');
		$tmpl = array (
			'table_open'          => '<table style="width:100%" class="table table-striped table-hover">',
			'heading_row_start'   => '<tr>',
			'heading_row_end'     => '</tr>',
			'heading_cell_start'  => '<th scope="col">',
			'heading_cell_end'    => '</th>',
			'row_start'           => '<tr>',
			'row_end'             => '</tr>',
			'cell_start'          => '<td>',
			'cell_end'            => '</td>',
			'table_close'         => '</table>'
		);
		$this->table->set_template($tmpl);
		$axns = array('data'=>'Actions','colspan'=>3);
		$this->table->set_heading('Page Title','Author','Type','Created',$axns);
		$pages = $this->pages_model->get_all_pages_list($config['per_page'],$this->uri->segment(4));
		if(!empty($pages)) {
			foreach($pages as $page) {
				$type = $page->bundled == 1?'System':'User Defined';
				$status = $page->active ==1?anchor("admin/pages/deactivate_page/$page->page_uuid",img(array('src'=>base_url().'/img/icons/16/disabled.png')),array('title' => 'Deactivate')):
					anchor("admin/pages/activate_page/$page->page_uuid",img(array('src'=>base_url().'/img/icons/16/enabled.png')),array('title' => 'Activate'));
				if($page->bundled == 1) {
					$this->table->add_row($page->page_title,$page->fullname,$type,date("jS M Y",strtotime($page->created)),
					anchor("admin/pages/edit_page/$page->page_uuid",img(array('src'=>base_url().'/img/icons/16/form_edit.png')),array('title' => 'Edit')),
					anchor("site/$page->page_path/$page->page_url",img(array('class'=>'icon-eye-open')),array('title'=>'Preview Page','target'=>'frontend')));
				} else {
					$this->table->add_row($page->page_title,$page->fullname,$type,date("jS M Y",strtotime($page->created)),
					anchor("admin/pages/edit_page/$page->page_uuid",img(array('src'=>base_url().'/img/icons/16/form_edit.png')),array('title' => 'Edit')),
					anchor("site/$page->page_path/$page->page_url",img(array('class'=>'icon-eye-open')),array('title'=>'Preview Page','target'=>'frontend')),$status,
					anchor("admin/pages/delete_page/$page->page_uuid",img(array('src'=>base_url().'/img/icons/16/delete.gif')),array('title' => 'Delete','onClick'=>'return confirm(\'Do you really want to delete this page?\')')));
				}
			}
		} else {
			$this->table->clear();
			$this->table->add_row('There are no pages added to the system. Would you like to add a new page??');
		}
		$this->template->content->view('pages/dashboard');
		$this->template->publish();
	}
	
	/**
	*  Add a page to the system
	* 
	*/
	function add_page()
	{
		// Set the template to use for this page
		$this->template->set_template('templates/admin_template');
		$this->template->javascript->add('//js.nicedit.com/nicEdit-latest.js');
		
		$this->form_validation->set_message('min_length','The %s field is required.');
		$this->form_validation->set_message('is_natural_no_zero','The %s field is required.');
		$this->form_validation->set_rules('page_title','Page Title','trim|required|xss_clean');
		$this->form_validation->set_rules('page_order','Page Order','trim|required|is_natural_no_zero|xss_clean');
		$this->form_validation->set_rules('page_layout','Page Layout','trim|required|is_natural_no_zero|xss_clean');
		$this->form_validation->set_rules('parent','Page Parent','trim|required|xss_clean');
		$this->form_validation->set_rules('page_content_left','Left Column Content','required|min_length[11]');
		$this->form_validation->set_rules('page_content_right','Right Column Content','required|min_length[11]');
		if($this->form_validation->run()) {
			$this->pages_model->save_page();
			redirect('admin/pages');
		} else {
			$this->template->content->view('pages/add_page_form');
			$this->template->publish();
		}
	}
	
	/**
	*  Edit the details of a page
	* 
	*  @param int page_uuid
	* 
	*/
	function edit_page($page_uuid)
	{
		// Set the template to use for this page
		$this->template->set_template('templates/admin_template');
		$this->template->javascript->add('//js.nicedit.com/nicEdit-latest.js');
		
		$data['details'] = $this->pages_model->get_page_details($page_uuid);
		$this->form_validation->set_message('min_length','The %s field is required.');
		$this->form_validation->set_message('is_natural_no_zero','The %s field is required.');
		$this->form_validation->set_rules('page_title','Page Title','trim|required|xss_clean');
		$this->form_validation->set_rules('page_order','Page Order','trim|required|is_natural_no_zero|xss_clean');
		$this->form_validation->set_rules('page_layout','Page Layout','trim|required|is_natural_no_zero|xss_clean');
		$this->form_validation->set_rules('parent','Page Parent','trim|required|xss_clean');
		$this->form_validation->set_rules('page_content_left','Left Column Content','required|min_length[11]');
		$this->form_validation->set_rules('page_content_right','Right Column Content','required|min_length[11]');
		if($this->form_validation->run()) {
			if($data['details']->bundled == 1)
				$this->pages_model->edit_system_page($page_uuid);
			else
				$this->pages_model->edit_page($page_uuid);
			redirect('admin/pages');
		} else {
			$this->template->content->view('pages/edit_page_form',$data);
			$this->template->publish();
		}
	}
	
	/**
	* Permanently delete a page from the system
	* Potentially dangerous. No way to recover from this.
	* Seriously, it's the end of the line for this page 
	* 
	* @param int page_uuid
	* 
	*/
	function delete_page($page_uuid)
	{
		$this->pages_model->delete_page($page_uuid);
		redirect('admin/pages');
	}
	
	/**
	* Activate a page so that it is accessible to all
	*
	* @param int page_uuid
	* 
	*/
	function activate_page($page_uuid)
	{
		$this->pages_model->activate_page($page_uuid);
		redirect('admin/pages');
	}
	
	/**
	* Deactivate a page so that it is not accessible to anybody
	* 
	* @param int page_uuid
	* 
	*/
	function deactivate_page($page_uuid)
	{
		$this->pages_model->deactivate_page($page_uuid);
		redirect('admin/pages');
	}
	
	/**
	* Build the Pages widget for display on the dashboard
	* 
	*/
	function dashboard_widget()
	{
		$this->load->library('pagination');
		$config['base_url'] = base_url().'/admin/pages/index';
		$config['total_rows'] = $this->db->count_all('pages');
		$config['uri_segment'] = 4;
		$config['per_page'] = 10;
		$config['num_links'] = 5;
		$config['full_tag_open'] = '<p>';
		$config['full_tag_close'] = '</p>';
		$this->pagination->initialize($config);
		
		$this->load->library('table');
		$tmpl = array (
			'table_open'          => '<table style="width:100%" class="table table-striped table-hover">',
			'heading_row_start'   => '<tr>',
			'heading_row_end'     => '</tr>',
			'heading_cell_start'  => '<th scope="col">',
			'heading_cell_end'    => '</th>',
			'row_start'           => '<tr>',
			'row_end'             => '</tr>',
			'cell_start'          => '<td>',
			'cell_end'            => '</td>',
			'table_close'         => '</table>'
		);
		$this->table->set_template($tmpl);
		$axns = array('data'=>'Actions','colspan'=>3);
		$this->table->set_heading('Page Title','Author','Type','Created',$axns);
		$pages = $this->pages_model->get_all_pages_list($config['per_page'],$this->uri->segment(4));
		if(!empty($pages)) {
			foreach($pages as $page) {
				$type = $page->bundled == 1?'System':'User Defined';
				$status = $page->active ==1?anchor("admin/pages/deactivate_page/$page->page_uuid",img(array('src'=>base_url().'/img/icons/16/disabled.png')),array('title' => 'Deactivate')):
					anchor("admin/pages/activate_page/$page->page_uuid",img(array('src'=>base_url().'/img/icons/16/enabled.png')),array('title' => 'Activate'));
				if($page->bundled == 1) {
					$this->table->add_row($page->page_title,$page->fullname,$type,date("jS M Y",strtotime($page->created)),
					anchor("admin/pages/edit_page/$page->page_uuid",img(array('src'=>base_url().'/img/icons/16/form_edit.png')),array('title' => 'Edit')),
					anchor("site/$page->page_url",img(array('class'=>'icon-eye-open')),array('title'=>'Preview Page','target'=>'frontend')));
				} else {
					$this->table->add_row($page->page_title,$page->fullname,$type,date("jS M Y",strtotime($page->created)),
					anchor("admin/pages/edit_page/$page->page_uuid",img(array('src'=>base_url().'/img/icons/16/form_edit.png')),array('title' => 'Edit')),
					anchor("site/page/$page->page_url",img(array('class'=>'icon-eye-open')),array('title'=>'Preview Page','target'=>'frontend')),$status,
					anchor("admin/pages/delete_page/$page->page_uuid",img(array('src'=>base_url().'/img/icons/16/delete.gif')),array('title' => 'Delete','onClick'=>'return confirm(\'Do you really want to delete this page?\')')));
				}
			}
		} else {
			$this->table->clear();
			$this->table->add_row('There are no pages added to the system. Would you like to add a new page??');
		}
		echo $this->table->generate();
		echo("<div id='pagination'>".$this->pagination->create_links()."</div>");
	}
	
	/**
	* Edit the system homepage
	* 
	* @param int page_uuid
	* 
	*/
	function edit_home($page_uuid)
	{
		// Set the template to use for this page
		$this->template->set_template('templates/admin_template');
		$this->template->javascript->add('//js.nicedit.com/nicEdit-latest.js');
		
		$data['details'] = $this->pages_model->get_page_details($page_uuid);
		$this->form_validation->set_message('min_length','The %s field is required.');
		$this->form_validation->set_message('is_natural_no_zero','The %s field is required.');
		$this->form_validation->set_rules('page_order','Page Order','trim|required|is_natural_no_zero|xss_clean');
		$this->form_validation->set_rules('page_layout','Page Layout','trim|required|is_natural_no_zero|xss_clean');
		$this->form_validation->set_rules('page_content_left','Left Column Content','required|min_length[11]');
		$this->form_validation->set_rules('page_content_right','Right Column Content','required|min_length[11]');
		if($this->form_validation->run()) {
			$this->pages_model->edit_system_page($page_uuid);
			redirect('admin/pages');
		} else {
			$this->template->content->view('pages/edit_page_form',$data);
			$this->template->publish();
		}
	}
	
	/**
	 * Get all access rules that are tied to user roles/ user levels
	 *
	 */
	function role_rules()
	{
		// Set the template to use for this page
		$this->template->set_template('templates/admin_template');
		
		$this->load->library('table');
		$tmpl = array (
		  'table_open'          => '<table class="table table-striped table-hover">',
		  'heading_row_start'   => '<tr class="heading">',
		  'heading_row_end'     => '</tr>',
		  'heading_cell_start'  => '<th>',
		  'heading_cell_end'    => '</th>',
		  'row_start'           => '<tr>',
		  'row_end'             => '</tr>',
		  'cell_start'          => '<td>',
		  'cell_end'            => '</td>',
		  'row_alt_start'       => '<tr class="alt">',
		  'row_alt_end'         => '</tr>',
		  'cell_alt_start'      => '<td>',
		  'cell_alt_end'        => '</td>',
		  'table_close'         => '</table>'
		);
		$this->table->set_template($tmpl); 
		$data['roles'] = $this->pages_model->get_all_roles_with_rules();
		$this->template->content->view('pages/role_rules',$data);
		$this->template->publish();
	}
	
	/**
	 * Add a rule to the system based on the user role
	 *
	 */
	function add_role_rule()
	{
		// Set the template to use for this page
		$this->template->set_template('templates/admin_template');
		
		$data['roles'] = $this->pages_model->get_all_roles();
		$this->form_validation->set_message('is_natural_no_zero','This field is required');
		$this->form_validation->set_rules('role_id','Role','trim|required|xss_clean');
		$this->form_validation->set_rules('page_id[]','Page','trim|required|is_natural_no_zero|xss_clean');
		$this->form_validation->set_rules('rule','Rule','trim|required|xss_clean');
		if($this->form_validation->run()) {
			$this->pages_model->add_role_rule();
			redirect('admin/pages/role_rules');
		} else {
			$this->template->content->view('pages/add_role_rule_form',$data);
			$this->template->publish();
		}
	}
	
	/**
	 * Get all access rules that are tied to users
	 *
	 */
	function user_rules()
	{
		// Set the template to use for this page
		$this->template->set_template('templates/admin_template');
		
		$this->load->library('table');
		$tmpl = array (
		  'table_open'          => '<table class="table table-striped table-hover">',
		  'heading_row_start'   => '<tr class="heading">',
		  'heading_row_end'     => '</tr>',
		  'heading_cell_start'  => '<th>',
		  'heading_cell_end'    => '</th>',
		  'row_start'           => '<tr>',
		  'row_end'             => '</tr>',
		  'cell_start'          => '<td>',
		  'cell_end'            => '</td>',
		  'row_alt_start'       => '<tr class="alt">',
		  'row_alt_end'         => '</tr>',
		  'cell_alt_start'      => '<td>',
		  'cell_alt_end'        => '</td>',
		  'table_close'         => '</table>'
		);
		$this->table->set_template($tmpl); 
		$data['users'] = $this->pages_model->get_all_users_with_rules();
		$this->template->content->view('pages/user_rules',$data);
		$this->template->publish();
	}
	
	/**
	 * Add a rule to the system based on the user role
	 *
	 */
	function add_user_rule()
	{
		// Set the template to use for this page
		$this->template->set_template('templates/admin_template');
		
		$data['users'] = $this->pages_model->get_all_users();
		$this->form_validation->set_message('is_natural_no_zero','This field is required');
		$this->form_validation->set_rules('user_id','User','trim|required|xss_clean');
		$this->form_validation->set_rules('page_id[]','Page','trim|required|is_natural_no_zero|xss_clean');
		$this->form_validation->set_rules('rule','Rule','trim|required|xss_clean');
		if($this->form_validation->run()) {
			$this->pages_model->add_user_rule();
			redirect('admin/pages/user_rules');
		} else {
			$this->template->content->view('pages/add_user_rule_form',$data);
			$this->template->publish();
		}
	}
	
	/**
	 * Allow a user rule
	 *
	 * @param int rule_id
	 *
	 */
	function allow_user_rule($rule_id)
	{
		$this->pages_model->allow_user_rule($rule_id);
		redirect('admin/pages/user_rules');
	}
	
	/**
	 * Deny a user rule
	 *
	 * @param int rule_id
	 *
	 */
	function deny_user_rule($rule_id)
	{
		$this->pages_model->deny_user_rule($rule_id);
		redirect('admin/pages/user_rules');
	}
	
	/**
	 * Allow a role rule
	 *
	 * @param int rule_id
	 *
	 */
	function allow_role_rule($rule_id)
	{
		$this->pages_model->allow_role_rule($rule_id);
		redirect('admin/pages/role_rules');
	}
	
	/**
	 * Deny role rule
	 *
	 * @param int rule_id
	 *
	 */
	function deny_role_rule($rule_id)
	{
		$this->pages_model->deny_role_rule($rule_id);
		redirect('admin/pages/role_rules');
	}
	
	/**
	 * Delete a rule
	 *
	 * @param int rule_id
	 *
	 */
	function delete_rule($rule_id)
	{
		$this->pages_model->delete_rule($rule_id);
		redirect('admin/pages');
	}
}