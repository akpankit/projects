<?php $this->load->view('includes/header');?>
<?php
$user_group = array('name'=>'user_group','id'=>'user_group','value'=>'','class'=>'input-1','size'=>30);
if($this->uri->segment(3)=='e'){ ?>
	<div class="alert alert-info">
		<p>
		<span class="ico-text ico-alert-info"></span>
		<?php echo "User group cannot be deleted because the user group has users"; ?>
		</p>
	</div>
<?php } ?>
<div id="content">
	<div class="leftside">
	<!-- menu-->
		<div class="box">
			<div class="box_t">
				<div class="box_b">
					<h2>Menu</h2>
					<?php echo modules::run("acl/show_sub_menu");?>
				</div>
			</div>
		</div><br />
		<div class="box">
			<div class="box_t">
				<div class="box_b">
					<h2>Submenu</h2>
					<p><?php echo anchor('acl/add_u_group','Add User Group');?></p>
				</div>
			</div>
		</div><br />
	</div>
	<div class="box-75" style="margin-left: 18px;">
		<div class="box_t">
		<div class="box_b">
			<h2>User Groups</h2> 
			
				<?php if(isset($deactivated)){ ?>
				<div class="alert alert-info">
					<p>
					<span class="ico-text ico-alert-info"></span>
					<?php echo $deactivated; ?>
					</p>
				</div>
				<?php }else{?>
					<?php echo form_open("acl/add_u_group"); ?>
					<div class="box-content" id="pages-2">
					<?php echo form_close();?>
					<?php echo form_open('acl/save_group_bulk'); ?>
					<?php echo $this->table->generate(); ?>
					<?php  echo form_close(); ?>
					<div id="pagination"><?php echo $this->pagination->create_links(); ?></div>
					</div>
				<?php } ?>
			</div>         
		</div>
	</div><!-- end .box-75 -->
	<?php $this->load->view('includes/footer');?>
</div><!-- end #content -->
