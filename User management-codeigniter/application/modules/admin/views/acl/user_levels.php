<?php $this->load->view('includes/header');
$user_level = array('name'=>'user_level','id'=>'user_level','value'=>'','class'=>'input-1','size'=>30);
if($this->uri->segment(3)=='e'){?>
<div class="alert alert-info">
<p>
<span class="ico-text ico-alert-info"></span>
<?php echo "User level cannot be deleted because the user level has users"; ?>
</p>
</div>
<?php }?>
	<div class="container">
		<div class="row">
			<div class="span3">
				<div class="sidenav sidebar-nav">
					<!-- menu-->
					<div class="box">
						<p>
						<ul class="nav nav-list">
							<li class="nav-header"><h4>Menu</h4></li>
							<?php echo modules::run('admin/acl/show_sub_menu'); ?>
						</ul>
						</p>
					</div>
				</div>
			</div>
			<div class="span9">
				<div id="content">
					<h2>User Levels</h2> 
					<?php if(isset($deactivated)){ ?>
					<div class="alert alert-info">
						<p>
						<span class="ico-text ico-alert-info"></span>
						<?php echo $deactivated; ?>
						</p>
					</div>
					<?php }else{?>
					<div class="box-content" id="pages-2">
						<?php echo form_open('acl/save_level_bulk'); ?>
						<?php echo $this->table->generate(); ?>
						<?php  echo form_close(); ?>
						<div id="pagination"><?php echo $this->pagination->create_links(); ?></div>
					</div>
					<?php }?>
				</div>
			</div>
			<?php $this->load->view('includes/footer');?>
		</div>
	</div>
</div>
