<?php $this->load->view('includes/header_admin');?>
<script type="text/javascript">
	$(document).ready(function() {
		var parents = [];
		$("#parents").change(function() {
			var chosenoption=this.options[this.selectedIndex];
			var count = parents.indexOf(chosenoption.value);
			if(count == -1){
				$("#parents-tr").before('<tr class="parent'+chosenoption.value+'"><td><select name="parent[]" id="parent[]"><option value="'+chosenoption.value+'">'+chosenoption.text+'</option></select></td><td><input type="button" id="remove" value="Remove" onclick=removeOption('+chosenoption.value+');></td></tr>');
				parents.push(chosenoption.value);
			}else{
				alert("Already Added");
			}
		});
	});
	function removeOption(option){
		$('.parent'+option).remove();
	};
</script>
<?php
$id = 'id="parents"';
$id2 = 'id="existing"';
$user_level = array('name'=>'user_level','id'=>'user_level','value'=>$level[0]->user_level,'class'=>'input-1','size'=>30);
$parents = $this->profile->get_level_parents();
$arr = '';
foreach($parents as $parent)
	$arr[$parent->u_level_id] = $parent->user_level;
$arr[0] = 'None';
unset($arr[$this->uri->segment(3)]);
foreach($level as $l)
	$paro = json_decode($l->parent);
if($paro)
	$cp = count($paro);
?>
<div id="content">
	<div class="leftside">
	<!-- menu-->
		<div class="box">
			<div class="box_t">
				<div class="box_b">
					<h3>Menu</h3>
					<?php echo modules::run("acl/show_sub_menu");?>
				</div>
			</div>
		</div><br />
	</div>
	<div class="box-75" style="margin-left: 18px;">
	<h2><?php echo $level[0]->user_level; ?></h2>
			<?php echo form_open($this->uri->uri_string()); ?>
			<table>
				<tr>
					<td><?php echo form_label('User Level'); ?></td>
					<td><?php echo form_input($user_level); ?></td>
				</tr>
				<tr>
					<td style="color:red; font-size:10pt;" colspan="2"><?php echo form_error($user_level['name']); ?></td>
				</tr>
				<?php if($paro){ for($i=0;$i<$cp;$i++){ ?>
				<tr>
					<td><?php echo form_label('Parent');?></td>
					<td><?php echo form_dropdown('parent[]',$arr,$paro[$i],$id2); ?></td>
				</tr>
				<?php } } ?>
				<tr id="parents-tr">
					<td><?php echo form_label('Add Parent');?></td>
					<td><?php echo form_dropdown('parents',$arr,'0',$id); ?></td>
				</tr>
				<tr>
					<td><?php echo form_submit('submit', 'Edit Level'); ?></td>
				</tr>
			</table>
			<?php echo form_close();?>
	</div><!-- end .box-75 -->
	<?php $this->load->view('includes/footer');?>
</div><!-- end #content -->
