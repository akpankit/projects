<?php 
if($this->profile->get_setting_value('signup')==0)
	redirect('/profiles/login/');

$this->load->view('includes/header_signup'); ?>

<?php
	if(strlen(set_value('fullname')) > 0){
		$username = array(
			'name'	=> 'username',
			'id'	=> 'username',
			'value' => set_value('username'),
			'class' => 'input-1',
			'maxlength'	=> $this->config->item('username_max_length', 'tank_auth'),
			'size'	=> 30,
		);
	}else{
		$username = array(
			'name'	=> 'username',
			'id'	=> 'username',
			'value' => $fb_username,
			'class' => 'input-1',
			'maxlength'	=> $this->config->item('username_max_length', 'tank_auth'),
			'size'	=> 30,
		);
	}
	if(strlen(set_value('fullname')) > 0){
		$Full_Name = array(
				'name'	=> 'fullname',
				'id'	=> 'fullname',
				'value' => set_value('fullname'),
				'class' => 'input-1',
				'size'	=> 30
		);
	}else{
		$Full_Name = array(
				'name'	=> 'fullname',
				'id'	=> 'fullname',
				'value' => 	$fullname,
				'class' => 'input-1',
				'size'	=> 30
		);
	}
$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'value'	=> set_value('email'),
	'class' => 'input-1',
	'maxlength'	=> 80,
	'size'	=> 30,
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'maxlength'	=> 8,
);


?>
<div id="content" style="right:-25%; left:22%; margin-top:100px; width:600px; position:relative">
    <div class="box corners shadow" style="width:720px;">
        <div class="box-header" style="width:700px; ">
           <h2>Register</h2>
         </div>
                   
        <div class="box-content" id="pages-2">
            <br />
 <?php if(isset($message) && strlen($message)>0){?>
 <table width="700">
  <tr>
    <td><?php echo $message;?></td>
  </tr>
</table>

<?php 
 }else{


echo form_open("profiles/fb_register"); ?>
<table width="700">
  <tr>
    <td>
		<?php echo form_label('Username', $username['id']); ?>
		<?php echo form_input($username); ?>
		<?php if(form_error($username['name']))echo '<div class="form-msg-error-advanced">'.form_error($username['name']).'</div>'; ?><?php echo isset($errors[$username['name']])?$errors[$username['name']]:''; ?>
	&nbsp;</td>
    <td><?php echo form_label('Full Name', $Full_Name['id']); ?>
		<?php echo form_input($Full_Name); ?>
		<?php if(form_error($Full_Name['name']))echo '<div class="form-msg-error-advanced">'.form_error($Full_Name['name']).'</div>'; ?><?php echo isset($errors[$Full_Name['name']])?$errors[$Full_Name['name']]:''; ?>&nbsp;</td>
  </tr>
  <tr>
    <td><?php echo form_label('Email Address', $email['id']); ?>
		<?php echo form_input($email); ?>
		<?php if(form_error($email['name']))echo '<div class="form-msg-error-advanced">'.form_error($email['name']).'</div>'; ?><?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?>&nbsp;</td>
    <td>&nbsp; <?php echo form_hidden('fb_id', $fb_id); ?></td>
  </tr>
  <tr>
    <td></td>
    <td style="float:right; padding-right:30px; padding-top:33px;"><div class="submit-box">
                <?php echo form_submit('register', 'Complete'); ?>
                </div></td>
  </tr>
</table>
        </form>
        </div>                                              
     </div>
 </div>
<?php }?>