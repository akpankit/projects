<div id="content">
	<div class="content-top">
		<h3>Ungrouped Users</h3>
	</div>
	<?php echo modules::run('admin/ungrouped_users_widget'); ?>
</div>
<div id="content">
	<div class="content-top">
		<h3>Deactivated Users</h3>
	</div>
	<?php echo modules::run('admin/deactivated_users_widget'); ?>
</div>