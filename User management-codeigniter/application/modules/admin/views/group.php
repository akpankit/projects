<div id="content">
	<div class="content-top">
		<h3>Ungrouped Users</h3>
	</div>
	<?php echo form_open('admin/search_user',"class='form-inline'");?>
	<input type="text" name="searcht" placeholder="Search Users" />
	<input type="submit" value="Search" name="search" id="search-submit" class="btn" />
	<?php  echo form_close(); ?>
	<?php echo form_open('admin/group_bulk'); ?>
	<?php echo $this->table->generate(); ?>
	<?php  echo form_close(); ?>
	<div id="pagination"><?php echo $this->pagination->create_links(); ?></div>
</div>