<!DOCTYPE html>
<html lang="en">
	
	<?php echo $this->template->widget('admin_header'); ?>
	
	<body>
		
		<?php echo $this->template->widget("admin_navigation"); ?>
		
		<?php if(!empty($message)) { ?>
			<div class="alert alert-error">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?php echo $message; ?>
			</div>
		<?php } if($this->session->flashdata('message')) { ?>
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?php echo $this->session->flashdata('message'); ?>
			</div>
		<?php } if($this->session->flashdata('error')) { ?>
			<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $this->session->flashdata('error'); ?>
			</div>
		<?php } ?>
		
			<div class="container">
				<div class="row">
					<div class="span3">
						
						<?php echo $this->template->widget("admin_submenu"); ?>
						
						<div class="sidenav sidebar-nav">
							<!-- search -->
							<div class="box">
								<div class="box-top">
									<h3>Search</h3>
								</div>
								<?php echo form_open('admin/search_user', array('id'=>'search'));?>
								<p> 
									<input type="text" name="searcht" placeholder="Search Users..." id="search_input" class="span2"/>
									<div id="search_submit"  onclick="$(this).closest('form').submit();"> </div>
								</p> 
								<?php echo form_close(); ?>  
							</div>
						</div>
					
						<?php echo $this->template->widget('dashboard_statistics'); ?>
					</div>
					<div class="span9">
						<?php echo $this->template->content; ?>
					</div>
				</div>
			</div>
	</body>
</html>