<!DOCTYPE html>
<html lang="en">
	
	<?php echo $this->template->widget('admin_header'); ?>
	
	<body>
		
		<?php echo $this->template->widget("admin_navigation"); ?>
		
		<?php if(!empty($message)) { ?>
			<div class="alert alert-error">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?php echo $message; ?>
			</div>
		<?php } if($this->session->flashdata('message')) { ?>
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<?php echo $this->session->flashdata('message'); ?>
			</div>
		<?php } if($this->session->flashdata('error')) { ?>
			<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<?php echo $this->session->flashdata('error'); ?>
			</div>
		<?php } ?>
		<div class="container">
			<div class="row" >
				<div class="span3">
					
					<?php echo $this->template->widget("admin_submenu"); ?>
					
				</div>
				<div class="span9">
					
					<?php echo $this->template->content; ?>
					
				</div>
			</div>
		</div>
	</body>
</html>