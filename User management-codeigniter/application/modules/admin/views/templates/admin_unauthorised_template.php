<!DOCTYPE html>
<html lang="en">
	
	<?php echo $this->template->widget('admin_header'); ?>
	
	<body>
		
		<?php echo $this->template->widget("admin_navigation"); ?>
		
		<div class="container">
			
			<?php echo $this->template->content; ?>
			
		</div>
	</body>
</html>