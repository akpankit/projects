<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xml:lang="en" lang="en">
<head>
<title>My Facebook Login Page</title>
</head>
<body>

   <fb:login-button>Login with Facebook</fb:login-button>
   <div id="fb-root"></div>
   <script src="http://connect.facebook.net/en_US/all.js"></script> 
   <script>
      // DON'T FORGET TO INCLUDE YOUR APP ID
      FB.init({
         appId:'your_app_id',cookie:true,
         status:true,xfbml:true
      });
   </script>
   <?
      // SHOWS LOGIN OR LOGOUT LINKS DEPENDING ON FACEBOOK SESSION
      if ($this->facebook->getSession()) {
         echo 'Logout';
      } else {
         echo 'Login';
      }
   ?>

</body>
</html>