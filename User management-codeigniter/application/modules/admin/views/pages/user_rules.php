<div id="content">
	<div class="content-top">
		<h3>User Rules</h3>
	</div>
	<?php
	if(!empty($users)){
		echo("<div class='accordion' id='accordion'>");
		echo("<div class='accordion-group'>");
		$axns = array('data'=>'Actions','colspan'=>3);
		$this->table->set_heading('Page','URL','Rule',$axns);
		//-- Content Rows
		foreach($users as $user){
			$rules = $this->pages_model->get_user_rules($user->user_id);
			foreach($rules as $rule){
				if($rule->rule == 1){
					$this->table->add_row($rule->page_title,$rule->page_url,$rule->value,
					anchor("admin/pages/deny_user_rule/$rule->rule_id",img(array('src'=>base_url().'/img/icons/16/cancelled.png')),array('title' => 'Deny')),
					anchor("admin/pages/delete_rule/$rule->rule_id",img(array('src'=>base_url().'/img/icons/16/delete.gif')), array('onClick' =>  'return confirm(\'Do you really want to delete this rule?\');', 'title' => 'Delete')));
				}else{
					$this->table->add_row($rule->page_title,$rule->page_url,$rule->value,
					anchor("admin/pages/allow_user_rule/$rule->rule_id",img(array('src'=>base_url().'img/icons/16/accept.png')),array('title' => 'Allow')),
					anchor("admin/pages/delete_rule/$rule->rule_id",img(array('src'=>base_url().'/img/icons/16/delete.gif')), array('onClick' =>  'return confirm(\'Do you really want to delete this rule?\');', 'title' => 'Delete')));
				}
			}
			echo("<div class='accordion-heading'>");
			echo("<a class='accordion-toggle' data-toggle='collapse' data-parent='#accordion' href='#collapse".$user->user_id."'>".$user->fullname."</a>");
			echo("</div>");
			echo("<div id='collapse".$user->user_id."' class='accordion-body collapse'>");
			echo("<div class='accordion-inner'>");
			echo $this->table->generate();
			$this->table->clear();
			echo("</div>");
			echo("</div>");
		}
		echo("</div>");
		echo("</div>");
	}else{
		$this->table->add_row("Nothing to see here :-|");
		echo $this->table->generate();
	}
	?>
</div>