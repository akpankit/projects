<script>
    $(function() {
        $('#page_order').spinner({
            min: 1,
            max: 20,
            step: 1
        });
    });
</script>
<?php
$page_order = array('name'=>'page_order','id'=>'page_order','value'=>$details->page_order);
$layouts = $this->pages_model->get_page_layouts();
$parents = $this->pages_model->get_parent_pages();
$layouts_dropdown = array();
$parents_dropdown = array();
$parents_dropdown[0] = 'No Parent';
foreach($layouts as $layout)
	$layouts_dropdown[$layout->layout_id] = $layout->layout_label;
foreach($parents as $parent)
	if($parent->page_id != $details->page_id)
		$parents_dropdown[$parent->page_id] = $parent->page_title;
if($details->premium == 1)
	$premium_page = array('name'=>'premium_page','id'=>'premium_page','value'=>1,'checked'=>TRUE);
else
	$premium_page = array('name'=>'premium_page','id'=>'premium_page','value'=>1,'checked'=>FALSE);
?>
<script type="text/javascript">
	bkLib.onDomLoaded(function(){
		new nicEditor({iconsPath : CI.base_url+'img/nicEditorIcons.gif',fullPanel : true}).panelInstance('page_content_left');
		new nicEditor({iconsPath : CI.base_url+'img/nicEditorIcons.gif',fullPanel : true}).panelInstance('page_content_right');
	});
</script>
<div id="content">
	<div class="content-top">
		<h3>Edit Page</h3>
	</div>
	<?php echo form_open($this->uri->uri_string(),"class='form-horizontal'"); ?>
		<div class="control-group">
			<label  class="control-label" for="page_title">Page Title</label>
			<div class="controls">
				<?php echo form_input('page_title',$details->page_title);?>
				<span class="help-block">
					<?php echo form_error('page_title');?>
				</span>
			</div>
		</div>
		<div class="control-group">
			<label  class="control-label" for="page_order">Page Order</label>
			<div class="controls">
				<?php echo form_input($page_order);?>
				<span class="help-block">
					<?php echo form_error('page_order');?>
				</span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="resource_type">Page Layout</label>
			<div class="controls">
				<?php echo form_dropdown('page_layout',$layouts_dropdown,$details->page_layout);?>
				<span class="help-block">
					<?php echo form_error('page_layout');?>
				</span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="resource_type">Page Parent</label>
			<div class="controls">
				<?php echo form_dropdown('parent',$parents_dropdown,$details->parent);?>
				<span class="help-block">
					<?php echo form_error('parent');?>
				</span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="premium_page">Premium Page</label>
			<div class="controls">
				<?php echo form_hidden('premium_page','0'); ?>
				<?php echo form_checkbox($premium_page) ?>
			</div>
		</div>
		<div class="control-group">
			<label  class="control-label" for="content">Left Column Content</label>
			<div class="controls">
				<textarea name="page_content_left" id="page_content_left" style="width:100%;"><?php echo $details->page_content_left !='<br>'?$details->page_content_left:''; ?></textarea>
				<span class="help-block">
					<?php echo form_error('page_content_left');?>
				</span>
			</div>
		</div>
		<div class="control-group">
			<label  class="control-label" for="content">Right Column Content</label>
			<div class="controls">
				<textarea name="page_content_right" id="page_content_right" style="width:100%;"><?php echo $details->page_content_right !='<br>'?$details->page_content_right:''; ?></textarea>
				<span class="help-block">
					<?php echo form_error('page_content_right');?>
				</span>
			</div>
		</div>
		<div class="form-actions">
			<button type="submit" class="btn">Save</button>
		</div>
	<?php echo form_close(); ?>
</div>