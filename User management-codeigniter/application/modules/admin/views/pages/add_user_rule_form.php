<?php
$id = 'id="resource"';
$id2 = 'id="user_id"';
$user_id[] = '';
foreach($users as $user)
	$user_id[$user->user_id] = $user->fullname;
$page_ids[] = '';
$pages = $this->pages_model->get_parent_pages();
if($this->uri->segment(4) != '' AND $this->uri->segment(5) != ''){
	$user = $this->uri->segment(4);
	$parent_id = $this->uri->segment(5);
	$page_children = $this->pages_model->get_page_children($parent_id);
}
$parent_id = isset($parent_id)?$parent_id:'';
foreach($pages as $page)
	$page_ids[$page->page_id] = $page->page_title;
$rules = array('1'=>'Allow','0'=>'Deny');
?> 
<script type="text/javascript">
	$(document).ready(function() {
		resources = [];
		$("#resource").change(function() {
			var chosenoption=this.options[this.selectedIndex];
			var redirect = CI.base_url+'admin/pages/add_user_rule/'+document.getElementById("user_id").value+'/'+chosenoption.value;
			 if (chosenoption.value!="nothing"){
				location.href = redirect; 
			}
		});
	});
</script>
<div id="content">
	<div class="content-top">
		<h3>Add User Rule</h3>
	</div>
	<?php echo form_open($this->uri->uri_string(),"class='form-horizontal'");?>
		<div class="control-group">
			<label class="control-label" for="user_id">User</label>
			<div class="controls">
				<?php echo form_dropdown('user_id',$user_id,$user,$id2);?>
				<span class="help-block">
					<?php echo form_error('user_id');?>
				</span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" for="resource_id">Page:</label>
			<div class="controls">
				<?php echo form_dropdown('page_id[]',$page_ids,isset($parent_id)?$parent_id:'',$id);?>
				<span class="help-block">
					<?php echo form_error('resource_id');?>
				</span>
			</div>
		</div>
		<?php if(!empty($page_children)) { foreach($page_children as $kid) { if($this->pages_model->user_has_access($kid->page_url,$user)){ ?>
		<div class="control-group">
			<label class="control-label" for="page_id[]"><?php echo $kid->page_title; ?>:</label>
			<div class="controls">
				<?php echo form_checkbox('page_id[]',$kid->page_id,TRUE);?>
			</div>
		</div>	
		<?php }else{ ?>
		<div class="control-group">
			<label class="control-label" for="page_id[]"><?php echo $kid->page_title; ?>:</label>
			<div class="controls">
				<?php echo form_checkbox('page_id[]',$kid->page_id);?>
			</div>
		</div>
		<?php } } } ?>
		<div class="control-group">
			<label class="control-label" for="rule">Rule:</label>
			<div class="controls">
				<?php echo form_dropdown('rule',$rules,$this->input->post('rule'));?>
			</div>
			<span class="help-block">
					<?php echo form_error('rule');?>
			</span>
		</div>
		<div class="form-actions">
			<button type="submit" class="btn">Save</button>
		</div>
	<?php echo form_close();?>
</div>