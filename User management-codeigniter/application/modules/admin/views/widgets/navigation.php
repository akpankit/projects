<div class="navbar  navbar-fixed-top" >
	<div class="navbar-inner">
		<div class="container">
			<button type="button" class="btn btn-navbar collapsed hidden-desktop" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="brand" href="<?php echo base_url().'admin/index'; ?>"><?php echo $site_name; ?></a>
			<p class="navbar-text pull-right">
				<a href="<?php echo base_url(); ?>" target="frontend" title="View Frontend"><i class="icon-eye-open icon-white"></i></a>
				<a href="<?php echo base_url().'admin/my_profile';?>" class="navbar-link"><i class="icon-user icon-white"></i><?php echo ucfirst($this->session->userdata('username')); ?></a>
				<a href="<?php echo base_url().'auth/logout'; ?>" class="navbar-link"><i class="icon-off icon-white"></i>Logout</a>
			</p>
			<div class="nav-collapse collapse hidden-desktop" style="height: 0px;">
				<ul class="nav">
					<?php echo modules::run('admin/acl/show_main_menu'); ?>
				</ul>
			</div>
			<div class="nav-collapse collapse hidden-tablet hidden-phone" style="height: 0px;">
				<ul class="nav">
					<?php echo modules::run('admin/acl/show_fancy_main_menu'); ?>
				</ul>
			</div>
		</div>
	</div>
</div>