<div class="sidenav sidebar-nav">
	<!-- menu-->
	<div class="box">
		<div class="box-top">
			<h3>Statistics</h3>
		</div>
		<h4>Users</h4>
		<p>                      
			<span class="stats"><?php echo $this->admin_model->get_no_of_users(); ?></span>Total Users<br />
			<span class="stats"><?php echo $this->admin_model->get_no_of_deactivated_users(); ?></span>Deactivated Users<br />
			<span class="stats"><?php echo $this->admin_model->no_of_admins(); ?></span>Administrators
		</p>
		<h4>Recent Logins</h4>
		<p>                        
			<?php echo modules::run('admin/recent_logins'); ?>
		</p>
	</div>
</div>