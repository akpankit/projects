<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
*	Exclude paths
*
*	These are the URLs that you would like excluded from the access rights check
*	e.g acl, acl/index, controller/function
*
*	Caution: Only place here URLs to pages or actions that do not need securing
*/
$config['exclude_paths'] = array('acl/has_access','acl/unauthorised','dashboard/index',
							'dashboard','profiles/login','profiles/logout','','screen','screen/away','screen/dtn');
