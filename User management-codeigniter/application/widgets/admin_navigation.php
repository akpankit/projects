<?php
class Admin_navigation extends Widget 
{
    public function display()
	{
		$data['site_name'] = $this->auth_model->get_site_name();
        $this->view('widgets/navigation',$data);
    }
}