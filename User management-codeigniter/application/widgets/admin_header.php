<?php
class Admin_header extends Widget 
{
    public function display()
	{
		$data['site_name'] = $this->auth_model->get_site_name();
        $this->view('widgets/header',$data);
    }
}