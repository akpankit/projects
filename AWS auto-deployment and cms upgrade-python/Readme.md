### Tool to manage AWS S3 Bucket

Python wrapper to use AWS sdk to sync your project assets on S3 and configure the S3 bucket the way you want

### Main File

upgrade.py

#### Features

Create Cloud distribution,
Create S3 bucket,
Create temporary iam,
Sync local assets to S3,
Move assets from local to S3,
Add security policy to S3,
Remove security policy to S3,
Update security policy to S3.
