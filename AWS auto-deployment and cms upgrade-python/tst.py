import boto
from boto import cloudfront
from boto.cloudfront import distribution
from boto.cloudfront import origin, logging
from boto.cloudfront.signers import TrustedSigners


if __name__ == "__main__":

    #masked due to security reasons
    AWS_ACCESS_K = "************"
    #masked due to security reasons
    AWS_SECRET_K = "*************"
    c = boto.cloudfront.CloudFrontConnection(AWS_ACCESS_K, AWS_SECRET_K)
    origin = origin.S3Origin('107-20-162-93*****.s3.amazonaws.com')

    cfd = c.create_distribution(origin=origin, enabled=False, caller_reference='107-20-162-93-504-2013-09-27', comment='Created programatically')
    disconfig = distribution.DistributionConfig(origin=origin, enabled=False, caller_reference='107-20-162-93-504-2013-09-27', comment='Created programatically', default_root_object='index.html')
    set = c.set_distribution_config(cfd.id, cfd.etag, disconfig)
    getcon = c.get_distribution_config(cfd.id)
    print getcon.to_xml()
    print distribution.DistributionSummary(id=cfd.id).to_json()

