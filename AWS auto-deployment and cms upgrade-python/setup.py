__author__ = 'apatel1'

from setuptools import setup



requires = ['paramiko', 'MySQLdb', 'boto', 'pyaml']

setup(name='Hebs_Paramiko',
      version='0.1.1',
      description='Wrapper around paramiko adding pbs style functionality',
      long_description='Python wrapper to upgrade from 5.3 to 6.0',
      license="Hebs",
      classifiers=[
        "Programming Language :: Python 2.7"
        ],
      author='Ankit',
      keywords='ssh paramiko',
      packages=['Hebs_Paramiko'],
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      )


