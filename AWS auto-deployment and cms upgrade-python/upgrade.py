#!/usr/bin/python
# -*- coding: UTF-8 -*-

__author__ = 'apatel1'

from connect import *
from sys import *
import string
import MySQLdb
import subprocess
import os
import time
import re
from createBucket import *
from messages import *

# This imports required to give effects on messages in terminal
from termcolor import colored, cprint


# global param required
remote_db = {}
rds = {}

ENV = 'dev'
FLAG = None

#dev
if ENV is 'dev':
    #masked due to security reasons
    AWS_ACCESS_K = "AK********************"
    #masked due to security reasons
    AWS_SECRET_K = "SX***********************7"
    FLAG = '-d'

if ENV is 'production':
    # production
    #masked due to security reasons
    AWS_ACCESS_K = "AK*******************"
    #masked due to security reasons
    AWS_SECRET_K = "9**********************qV"
    FLAG = '-p'


def set_params(ssh, ini, domain):

    """
    Read the file on remote server and get the contents using basic cat command instead using sftp.
    """
    look_for = ["host", "username", "password", "port", "dbname"]
    temp_list = []
    global remote_db

    # instead parsing just grep the ini to get required params.

    x = ssh.cat(ini+'application.ini | grep "resources.db.*"')
    if x:
        printMessage(DB_CONFIG)
    else:
        printError(DB_CONFIG_ERR)
        exit()

    for item in x:
        temp = item.replace('resources.db.params.', '')
        temp = string.strip(temp)
        temp_list.append(temp)

    for items in temp_list:

        cfg = string.split(items, '=')

       # Remove quotes and extra spaces

        key = string.strip(cfg[0])

        if key not in look_for:
            continue
        else:
            # Clean up the string
            value = cfg[1][2:-1]
            # populate it to global dictionary
            remote_db[key] = string.strip(value)
            # printMessage(remote_db[key]+"==>"+string.strip(value))

    # print remote_db
    if not remote_db:
        exit()
    else:
        set_upDB(ssh, domain)


def set_upDB(ssh, domain):

    """
    generate mysql string with params

    """
    cmd    = None
    global rds
    global remote_db

    global ENV

    if ENV is 'dev':
        # for development use same host
        #masked due to security reasons
        rds['host']     = 'hebstes************ce.************.us-east-1.rds.amazonaws.com'
        #masked due to security reasons
        rds['user'] = '***********'
        #masked due to security reasons
        rds['password'] = '*******'

    if ENV is 'production':
        # for live use different host
        rds['host'] = 'hebsm**********.cw*********m7qc.us-east-1.rds.amazonaws.com'
        #masked due to security reasons
        rds['user'] = '*************'
        #masked due to security reasons
        rds['password'] = '************'

    # setting the migrated db name

    timestamp = int(time.time())
    migration_name = remote_db['dbname']+'_60_'+ str(timestamp)
    rds['dbname'] = migration_name


    # get the commands respectively
    # convert 'username'  to 'user' to match mysql command

    remote_db['user'] = remote_db['username']
    remote_db.pop('username')

    dbdump    = makeCmd(remote_db)
    rdsimport = makeCmd(rds, 'mysql')

    cmd = ' | '.join([dbdump, rdsimport])

    # Before migration create DB on rds with migration name
    db = MySQLdb.connect(host=rds['host'], user=rds['user'], passwd=rds['password'])
    cursor = db.cursor()
    sql = "CREATE DATABASE IF NOT EXISTS "+rds['dbname']
    cursor.execute(sql)
    db.close()

    # Now we are ready to clone to db
    # print cmd
    printMessage(CLONE_DB)

    proc = ssh.mysqldump(cmd)

    # proc = None, exit status should be 0
    # If response was empty that means it successfully cloned the DB
    if not proc:
        # Execute Deltas
        upgradeDB(rds, migration_name, ssh, domain)
    else:
        printError(proc)


def upgradeDB(host, migration_name, ssh, domain):

    # get mysql command to import.

    deltas = os.getcwd() + '/deltaupgrade.sql'

    cmd = makeCmd(host, 'mysql')
    cmd = cmd + ' < ' + deltas

    printMessage("Starting to upgrade "+ migration_name + " database to 6.0 schema")

    # execute deltas from deployment server, no need of ssh now.
    ex = subprocess.call(cmd, shell=True)
    # ex = 0
    if ex is 0:
        printMessage(UPGRADE_SUCCESS)
        initAWS(domain, ssh)
    else:
        printError(str(ex))


def initAWS(domain, ssh):

    global rds
    global remote_db
    #Start creating AWS user and new S3 bucket
    printMessage(CREATE_AWS)
    x = Hebs_createAWS(domain, AWS_ACCESS_K, AWS_SECRET_K)
    want = x.iamUser(1)

    try:
        db = MySQLdb.connect(host=rds['host'], user=rds['user'], passwd=rds['password'], db=rds['dbname'])
        cursor = db.cursor()
        sql  = "update `*****` set `value` ='%s' where `module`='cloud' and `option`='aws_accesskey'" % want['updated_info']['access_key']
        cursor.execute(sql)
        sql = "update `*****` set `value` ='%s' where `module`='cloud' and `option`='aws_secretkey'" % want['updated_info']['secret_key']
        cursor.execute(sql)
        sql = "update `*****` set `value` ='%s' where `module`='cloud' and `option`='bucket_name'" % want['updated_info']['new_bucket']
        cursor.execute(sql)
        sql = "update `******` set `value` ='%s' where `module`='application' and `option`='domain'" % want['updated_info']['cfd']
        cursor.execute(sql)
        sql = "update `*****` set `value` ='%s' where `module`='cloud' and `option`='storage_adapter_type'" % 'S3'
        cursor.execute(sql)

        # Update the privileges for 5.3 DB user to access new cloned 6.0 DB on rds
        printMessage(AUTHORIZING_MYSQL_USER)
        sql  = "GRANT ALL PRIVILEGES ON %s.* TO '%s'@'%s' IDENTIFIED BY '%s' WITH GRANT OPTION" % (rds['dbname'], remote_db['user'], '%', remote_db['password'])
        # print sql + "\n"
        try:
            cursor.execute(sql)
            cursor.execute('FLUSH PRIVILEGES;')
        except MySQLdb.Error, ex:
            db.rollback()
            try:
                err = "MySQL Error [%d]: %s" % (ex.args[0], ex.args[1])
                printError(err)
            except IndexError:
                err = "MySQL Error: %s" % str(ex)
                printError(err)
        #commit changes to database
        db.commit()

        #once committed close the database connection
        db.close()
        printMessage("Cloud"+PRAM_UPDATED)
    except MySQLdb.Error, e:
        db.rollback()
        try:
            err = "MySQL Error [%d]: %s" % (e.args[0], e.args[1])
            printError(err)
        except IndexError:
            err = "MySQL Error: %s" % str(e)
            printError(err)

    #Now update the DB with imagesizes
    try:
        printMessage(IPOOL_PARAM_UPDATE)
        if FLAG is None:
            printError(IMAGE_PROCESS_INIT_ERR)
        else:
            print ssh.adminexec('cd /var/www/html; php processimgs.php '+FLAG+' '+rds['dbname'])
    except Exception, e:
        printError(IPOOL_PARAM_UPDATE_ERR)
        print e

    # Once done with user and bucket set up,initiate assets transfer
    printMessage(ASSETS_TRNSFR)

    if want['success'] is 1:
        # return
        #start moving assets to s3
        # print user['cmd']
        try:
            # Don't use "adminexec" unless you are sure about it
            # to get rid of output use  /dev/null;
            ssh.adminexec('cd /var/www/html; nohup s3put '+want['cmd']+' > /tmp/tos.log')
            # print "skip"
        except Exception, e:
            printError(TRANSFR_FAILED)
            print e
        finally:
            printMessage(ASSETS_FINISHED)


def makeCmd(info, prefix=None):

    cmd_params = ["host", "user", "password", "dbname"]
    cmd = ''

    for param in cmd_params:
        if param != "dbname":

            cmd += '--' + param+'='+info[param] + ' '

    cmd = cmd + info['dbname']

    if prefix:
        return prefix+' '+cmd
    else:
        return cmd


def printError(msg):

    text = colored(msg+"\n", 'red', attrs=['reverse', 'blink'])
    print text
    return True


def printMessage(msg):

    text = colored(msg+"\n", 'green', attrs=['bold'])
    print u'\u2713 ' + text
    return True


def printWarn(msg):

    msg = "WARNING!!! \n\n"+msg+"\n"
    text = cprint(msg, 'red', 'on_blue', attrs=['underline', 'bold'])
    print text
    return True


if __name__ == "__main__":

    domain = argv[1]
    """
    Initiate the ssh connection

    """
    ini = None
    ssh = Hebs_Paramiko(domain, 'ec2-user')
    ini_path = '/var/www/html/core/application/configs/'

    """
    Check if the path exist, if not throw exception and shell output
    """
    config_list = ssh.ls(ini_path)
    if not config_list:
        printError(INI_LOCATION_ERR)
        exit()
    else:
        if 'application.ini' in config_list:
            print colored("-------------Status:---------------\n", 'green', attrs=['bold'])
            printMessage(INI_FOUND)
            ini = True
            set_params(ssh, ini_path, domain)
            # initAWS(domain, ssh)
        else:
            printError(INI_FILE_ERR)
            ini = False
            exit()
