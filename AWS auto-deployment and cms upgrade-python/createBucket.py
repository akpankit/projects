__author__ = 'apatel1'

import boto
from boto.s3.connection import Location
from boto.s3.tagging import TagSet, Tags
import json
import time, datetime
import hashlib
import re
import ujson
from sys import *
import inspect
from upgrade import printError, printWarn, printMessage
from random import randrange
from messages import *
from boto import cloudfront
from boto.cloudfront import origin, distribution


class Hebs_createAWS:

    def __init__(self, domain, aws_access, aws_secret):

        self.AWS_ACCESS_K = aws_access
        self.AWS_SECRET_K = aws_secret
        self.region = 'us-east-1'

        # regex on domain name based on type of host
        is_valid_ip = re.match("^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$", domain)

        # is_valid_ip = 1

        if is_valid_ip is None:
            self.client = str(domain).replace("www.", "")
        else:
            self.client = str(domain)

        # set bucket name
        self.bucket = self.client.replace('.', '-')

        #generate username
        self.username = self.bucket+str(int(time.time()))+'-user'

        # set bucket name
        self.bucket = self.client.replace('.', '-')+"-"+str(randrange(1000))+"-"+str(datetime.date.today())
        # self.bucket = "www-hebsdigital-com"
        
        # policy name
        self.policyname = self.username+'--policy--'+str(datetime.date.today())

        printMessage("S3 Bucket: "+self.bucket)
        printMessage("IAM User: "+self.username)

    def iamUser(self, custom=1):

        #self.username = self.bucket+'-user'

        # Connect to IAM with boto
        iam = boto.connect_iam(self.AWS_ACCESS_K, self.AWS_SECRET_K)

        # S3 connection
        s3conn = boto.connect_s3(self.AWS_ACCESS_K, self.AWS_SECRET_K)

        # First check if bucket with the name exist
        if self.bucketExist(s3conn, self.bucket) is not None:
            printWarn(BUCKET_EXIST_WARN)
            #hash bucket name
            bucket_hash = hashlib.sha1()
            bucket_hash.update(self.bucket+str(time.time()))

            #assign hash to bucket name
            self.bucket = str(self.bucket+'-'+bucket_hash.hexdigest()[:15])
            printMessage("New bucket name:-->") + self.bucket

        # print self.bucket
        # exit()
        # Create user
        try:
            user_response = iam.create_user(self.username)
        except Exception, e:
            printError(AWS_USER_FAIL)
            print e
            print json.dumps(user_response, indent=2)

        # now set policy to user
        try:
            set = iam.put_user_policy(self.username, self.policyname, self.custom_policy(custom))
        except Exception, e:
            printError(AWS_USER_POLICY_FAIL)
            print e
            print json.dumps(set, indent=2)

        # create access keys on successfully creation of user
        if set:
            # Generate new access key pair for 'melvins'
            try:
                key_response = iam.create_access_key(self.username)

                #optimized json decoder to decode aws response
                key_response = ujson.dumps(key_response)
                key_response = ujson.loads(key_response)
                self.new_access_k = key_response['create_access_key_response']['create_access_key_result']['access_key']['access_key_id']
                self.new_secret_k = key_response['create_access_key_response']['create_access_key_result']['access_key']['secret_access_key']
                self.new_bucket = self.bucket

                print json.dumps(key_response, indent=2)
            except Exception, e:
                printError(AWS_ACCESS_KEY_FAIL)
                print e
                print key_response
            # Now request for new bucket
            printMessage(S3_BUCKET)
            try:
                news3 = self.createS3(1)
                return news3
            except Exception, e:
                printError(S3_BUCKET_FAILED)
                print e



    def custom_policy(self, default, policy=None):

        policy_json = {}
        # Limit access with IAM policy
        if default is 1:
            policy_json['user'] = '''{
                "Statement":[
                        {
                          "Action": [
                            "s3:ListAllMyBuckets"
                          ],
                          "Effect": "Allow",
                          "Resource": "arn:aws:s3:::*"
                        },
                        {
                          "Action": "s3:*",
                          "Effect": "Allow",
                          "Resource": ["arn:aws:s3:::%s", "arn:aws:s3:::%s/*"]
                        }
                      ]
            }''' % (self.bucket, self.bucket)

            policy_json['s3'] = '''{
                "Version":"2008-10-17",
                "Statement":[
                {
                    "Sid":"PublicReadForGetBucketObjects",
                    "Effect":"Allow",
                    "Principal": {
                        "AWS": "*"
                     },
                  "Action":["s3:GetObject"],
                  "Resource":["arn:aws:s3:::%s/*"]
                }
              ]
        }''' % self.bucket

        elif policy:
            policy_json = policy
        else:
            printError(MISSING_POLICY)
            exit()

        print "------------"+self.get_caller()+"---------------"
        # return the policy based on which function is requesting for the policy
        if self.get_caller() is "iamUser":
            return policy_json['user']
        elif self.get_caller() is "createS3":
            return policy_json['s3']
        else:
            return policy_json

    def custom_S3tags(self):

        tags = {
                    "Stack": "Production",
                    "CMSVersion": "cms60",
                    "Billing": "Yes",
                    "CreateDate": datetime.date.today(),
                    "Domain": self.client,
                    "Client": "test"
                }
        return tags

    def createS3(self, custom=1):

        # try connecting to s3
        try:
            s3conn = boto.connect_s3(self.AWS_ACCESS_K, self.AWS_SECRET_K)
        except Exception, e:
            printError(S3_CONN)
            print e
            exit()

        # create bucket in region us-east-1
        try:

            # policy=self.custom_policy(custom)
            newbucket = s3conn.create_bucket(self.bucket, policy='public-read')
            newbucket.set_policy(self.custom_policy(custom))

            # set tags once bucket is created
            bucket = s3conn.get_bucket(self.bucket)

            # get list of tags
            taglist = self.custom_S3tags()

            # Create a tag set object
            tagset = boto.s3.tagging.TagSet()

            # create set of tags from taglist using tagset object
            for key, value in taglist.iteritems():
                tagset.add_tag(key, value)

            # Once the tagset object has created a set, apply it to the bucket
            tag = boto.s3.tagging.Tags()
            tag.add_tag_set(tagset)
            newbucket.set_tags(tag)

            ## Create cloud front
            try:
                printMessage("Creating Cloud front distribution")

                c = boto.cloudfront.CloudFrontConnection(self.AWS_ACCESS_K, self.AWS_SECRET_K)

                # create origin of s3 type
                orig = origin.S3Origin(self.bucket+'.s3.amazonaws.com')

                #create distribution
                cfd = c.create_distribution(origin=orig, enabled=True, caller_reference=self.bucket, comment='Created programatically on '+str(datetime.date.today()))

                #get cfd domain
                self.cfd = cfd.domain_name

                #instnatiate configuration of cfd
                disconfig = distribution.DistributionConfig(origin=orig, enabled=True, caller_reference=self.bucket, comment='Created programatically on '+str(datetime.date.today()), default_root_object='index.html')

                #set the config to created Distribution
                set = c.set_distribution_config(cfd.id, cfd.etag, disconfig)

                #Get the distribution info
                # getcon = c.get_distribution_config(cfd.id)

                # print cfd url
                print self.cfd

            except Exception, e:
                printError(CFD_FAILED)
                print e

        except Exception, e:
            printError(S3_BUCKET_ERR)
            print e
        finally:
            printMessage(S3_BUCKET_S)

            #update db with new values
            printMessage(CLOUD_PARAM_UPDATE)

            info = {'access_key': self.new_access_k, 'secret_key': self.new_secret_k, 'new_bucket': self.new_bucket, 'cfd': self.cfd}

            #Create assets command and return true, 1 is true and 0 is false
            response = {'success': 1, 'cmd': self.assetsCmd(), 'updated_info': info}
            return response

    def bucketExist(self, conn, bucketname):

        buckets = conn.get_all_buckets()
        b_set = set()
        for item in buckets:
            b_set.add(item.name)

        if bucketname in b_set:
            return 1
        else:
            return None


    def assetsCmd(self):

        args = {'a': self.AWS_ACCESS_K, 's': self.AWS_SECRET_K, 'p': "/var/www/html", 'b': self.bucket}
        cmd = " ".join('-%s "%s"' % item for item in args.iteritems()) + " cms/"

        return cmd

    def get_caller(self):

        current_frame = inspect.currentframe()
        outerframe = inspect.getouterframes(current_frame, 2)

        # Returning the 2nd frame to get the name of method called by self,
        # normally the firs frame contains the name of the caller function

        return outerframe[2][3]

# if __name__ == "__main__":
#
#     host = argv[1]
#     x = Hebs_createAWS(host, '************', '************************7')
#     x.iamUser(1)



