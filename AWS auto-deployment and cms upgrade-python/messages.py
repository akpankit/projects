__author__ = 'apatel1'

# Module start messages

INI_LOCATION_ERR = "Cannot locate the path to ini directory"
INI_FOUND = "Found INI"
INI_FILE_ERR = "Missing INI file"
EXIT = "Exiting..."

# DB messages

DB_CONFIG = "Found DB config"
DB_CONFIG_ERR = "DB config missing or not found in INI file, exiting..."

CLONE_DB = "Cloning databse to RDS"
UPGRADE_SUCCESS = 'Successfully upgraded to 6.0 database schema, please verify the results'

AUTHORIZING_MYSQL_USER = "Authorizing mysql user to access new database"

IPOOL_PARAM_UPDATE = "Updating ipool images parameters for 6.0, please wait"

IMAGE_PROCESS_INIT_ERR = "Environment flag missing : Cannot process images on server"

CFD_FAILED = "Problem in creating cloud front distribution"

# AWS messages

CREATE_AWS = "Creating AWS user and bucket"

CLOUD_PARAM_UPDATE  =  "Please wait, updating db with new cloud settings"

PRAM_UPDATED = " parameters updated"

MISSING_POLICY = "NO policy to apply, exiting.."

IPOOL_PARAM_UPDATE_ERR = "Could not update the image height,width,size parameters, exiting"

ASSETS_TRNSFR = "Assets transfer started, please wait."

TRANSFR_FAILED = "Ooops!! something went wrong during transfer"

ASSETS_FINISHED = "Assets transfer done, please verify the contents on s3 bucket"

BUCKET_EXIST_WARN = "\nWARNING: Bucket Exists, adding hash to the bucket name"

AWS_USER_FAIL = "Creating AWS user failed !!!"

AWS_USER_POLICY_FAIL ="Failed to set policy for user"

AWS_ACCESS_KEY_FAIL = "Failed to create new access key for user"

S3_BUCKET = "Requesting new S3 bucket."

S3_BUCKET_FAILED = "Creation of new S3 bucket failed"

S3_CONN = "Connection to S3 Failed"

S3_BUCKET_ERR = "Unknown error occured while creating S3 bucket"

S3_BUCKET_S = "Successfully created S3 bucket with policy and tags"