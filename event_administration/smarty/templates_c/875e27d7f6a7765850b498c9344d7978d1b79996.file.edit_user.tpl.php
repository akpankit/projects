<?php /* Smarty version Smarty-3.1.5, created on 2011-12-15 03:12:20
         compiled from "./smarty/templates/edit/edit_user.tpl" */ ?>
<?php /*%%SmartyHeaderCode:10047478484ee803a5c66013-10710066%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '875e27d7f6a7765850b498c9344d7978d1b79996' => 
    array (
      0 => './smarty/templates/edit/edit_user.tpl',
      1 => 1323936735,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '10047478484ee803a5c66013-10710066',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.5',
  'unifunc' => 'content_4ee803a5ce479',
  'variables' => 
  array (
    'logged_in' => 0,
    'error_flag' => 0,
    'errors2' => 0,
    'ename' => 0,
    'elname' => 0,
    'email' => 0,
    'emajor' => 0,
    'egender' => 0,
    'elevel' => 0,
    'ephone' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_4ee803a5ce479')) {function content_4ee803a5ce479($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>User Registration</title>
		<link rel="stylesheet" href="./layout/css/960.css" type="text/css" media="screen" charset="utf-8" />
		<!--<link rel="stylesheet" href="css/fluid.css" type="text/css" media="screen" charset="utf-8" />-->
		<link rel="stylesheet" href="./layout/css/template.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="./layout/css/colour.css" type="text/css" media="screen" charset="utf-8" />
		<script src="./layout/js/jquery-1.4.2.js" type="text/javascript"></script>
		<script src="./layout/js/jquery-ui-1.8.1.js" type="text/javascript"></script>
		<script type="text/javascript">
		$(document).ready(function()
	    		{
					$(".verify").click(function(){
						alert("Updated,Thank you");
					});


	    		});
		</script>
	</head>
	<body>
	<h1 id="head">Welcome to Event Administration Portal</h1>
	<?php if ($_smarty_tpl->tpl_vars['logged_in']->value==0){?>
	<ul id="navigation">
			
			<li><a href="./main.php">Login</a></li>
			<li><a class="active" href="#">Register</a></li>
			
		</ul>
		<?php }else{ ?>
		<ul id="navigation">
			
			<li><a href="./logout.php">Logout</a></li>
			<li><a class="active" href="./registration.php">Register</a></li>
			<li><a class="active" href="./management.php">Manage</a></li>
		</ul>
		
		<?php }?>
			<div id="content" class="container_16 clearfix">
				<div class="grid_16">
					<h2>Edit User Information</h2>
					</div></div>
				<div id="content" class="container_16 clearfix">
				<form method="post" name="edit_user"  id="edit_u" action="./test.php">	
		
				<?php if ($_smarty_tpl->tpl_vars['error_flag']->value==1){?>
					<p class="error"><?php echo $_smarty_tpl->tpl_vars['errors2']->value;?>
</p>
				<?php }?>
						
				<div class="grid_5">
					<p>
					
						<label for="title">First Name <small> Only letters are allowed.</small></label>
						
							<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['ename']->value;?>
"  name="first_name"/>
									
					</p>
				</div>
				
				<div class="grid_5">
					<p>
					
						<label for="title">Last Name <small> Only letters are allowed.</small></label>
							<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['elname']->value;?>
" name="last_name" />
						
					</p>
				</div>
				<hr style="visibility:hidden;"></hr>
				<div class="grid_5">
					<p>
						<label for="title">E-mail<small>Please provide valid e-mail address</small></label>
						
						<input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['email']->value;?>
" name="e-mail" disabled="disabled"  />
						
						
					</p>
				</div>
				
				
				<div class="grid_5">
					<p>
					
						<label for="title">Major</label>
							<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['emajor']->value;?>
" name="major" />
						
						
					</p>
				</div>
				
				<hr style="visibility:hidden;"></hr>
				<div class="grid_5">
					<p>
						<label>Gender</label>
						<select name="gender">
							<?php if ($_smarty_tpl->tpl_vars['egender']->value=='m'){?>						
							<option value="male" selected>Male</option>
							<?php }else{ ?>
							<option value="male" >Male</option>
							<?php }?>
							<?php if ($_smarty_tpl->tpl_vars['egender']->value=='f'){?>
							<option value="female" selected>Female</option>
							<?php }else{ ?>
							<option value="female" >Female</option>
							<?php }?>
							
						</select>
					</p>
				</div>
				<div class="grid_5">
					<p>
						<label>Grade Level</label>
						<select name="level">
							<?php if ($_smarty_tpl->tpl_vars['elevel']->value=='sophmore'){?>	
							<option  value="sophmore" selected>Sophmore</option>
							<?php }else{ ?>
							<option  value="sophmore" >Sophmore</option>
							<?php }?>
							<?php if ($_smarty_tpl->tpl_vars['elevel']->value=='junior'){?>
							<option  value="junior" selected>Junior</option>
							<?php }else{ ?>
							<option  value="junior" >Junior</option>
							<?php }?>
							<?php if ($_smarty_tpl->tpl_vars['elevel']->value=='senior'){?>
							<option  value="senior" selected>Senior</option>
							<?php }else{ ?>
							<option  value="senior" >Senior</option>
							<?php }?>
						</select>
					</p>
				</div>
				<div class="grid_5">
					<p>
					
						<label for="title">Phone<small>(e.g. 123-456-7890 or 1234567890).</small></label>
						
						<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['ephone']->value;?>
" name="phone" />
						
						
					</p>
				</div>
				
				<div class="grid_16">
					<p class="submit">
						
						<input type="submit" class="verify" value="Modify" name="updt" />
					</p>
				</div>
				</form>
			</div>

			<<div id="foot">
					<a href="#">Contact Me at <b>yopresent@gmail.com</b></a>
				
		</div>


</body>
</html>
				
				
				
			
			<?php }} ?>