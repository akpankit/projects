<?php /* Smarty version Smarty-3.1.5, created on 2011-12-15 02:36:36
         compiled from "./smarty/templates/users/login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3162040604ee6c49a64d7e8-13047957%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'be7d6c91fb4a5b1d806bdda4805d8a2f3631872a' => 
    array (
      0 => './smarty/templates/users/login.tpl',
      1 => 1323934565,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3162040604ee6c49a64d7e8-13047957',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.5',
  'unifunc' => 'content_4ee6c49a78b3a',
  'variables' => 
  array (
    'notfo' => 0,
    'notfound' => 0,
    'notappr' => 0,
    'notapprs' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_4ee6c49a78b3a')) {function content_4ee6c49a78b3a($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>CCNY Event Administration Portal</title>
		<link rel="stylesheet" href="./layout/css/960.css" type="text/css" media="screen" charset="utf-8" />
		<!--<link rel="stylesheet" href="css/fluid.css" type="text/css" media="screen" charset="utf-8" />-->
		<link rel="stylesheet" href="./layout/css/template.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="./layout/css/colour.css" type="text/css" media="screen" charset="utf-8" />
	</head>
	<body>
		
					<h1 id="head" style="text-align:center;">Welcome to CCNY Event Administration Portal</h1>
		
		<ul id="navigation">
			<li><span class="active"><marquee>Hundred's of new and life-changing events are happening at CCNY. If you are member of CCNY we strongly encourage you to sign up to our new event monitoring system.</marquee></span></li>
			
		</ul>
		
			<div id="content" class="container_16 clearfix">
			<div class="grid_16">
					<h2>Log In</h2>
				</div>
				<form method="post" id="signin" action="./main.php">
				<div class="grid_5">
				<?php if ($_smarty_tpl->tpl_vars['notfo']->value==1&&$_smarty_tpl->tpl_vars['notfound']->value!==''){?>
				<p class="error"><?php echo $_smarty_tpl->tpl_vars['notfound']->value;?>
</p><?php }?>
				<?php if ($_smarty_tpl->tpl_vars['notappr']->value==1&&$_smarty_tpl->tpl_vars['notapprs']->value!==''){?>
				<p class="error"><?php echo $_smarty_tpl->tpl_vars['notfound']->value;?>
</p><?php }?>
					<p>
									
						<label>Username: </label> <input type="text" name="username" id="username"/>
						<label>Password: </label> <input type="password" name="password" id="password"/>
						
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>&nbsp;</label>
						<input type="submit" value="Log In" name="login" />
						<a href="./registration.php">Click here to <b>Register</b> if you are new user</a>
					</p>
				</div>
				</form>
				</div>
			
		
		<div id="foot">
					<a href="#">Contact Me at <b>yopresent@gmail.com</b></a>
				
		</div>
	</body>
</html><?php }} ?>