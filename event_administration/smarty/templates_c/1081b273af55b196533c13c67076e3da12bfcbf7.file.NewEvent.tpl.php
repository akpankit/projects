<?php /* Smarty version Smarty-3.1.5, created on 2011-12-15 02:18:15
         compiled from "./smarty/templates/events/NewEvent.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1573219584ee8377cb45532-12962088%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1081b273af55b196533c13c67076e3da12bfcbf7' => 
    array (
      0 => './smarty/templates/events/NewEvent.tpl',
      1 => 1323932985,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1573219584ee8377cb45532-12962088',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.5',
  'unifunc' => 'content_4ee8377cbd1ea',
  'variables' => 
  array (
    'logged_in' => 0,
    'eventerrs' => 0,
    'eventer' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_4ee8377cbd1ea')) {function content_4ee8377cbd1ea($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>New Event</title>
		<link rel="stylesheet" href="./layout/css/960.css" type="text/css" media="screen" charset="utf-8" />
		<!--<link rel="stylesheet" href="css/fluid.css" type="text/css" media="screen" charset="utf-8" />-->
		<link rel="stylesheet" href="./layout/css/template.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="./layout/css/colour.css" type="text/css" media="screen" charset="utf-8" />
	</head>
	<body>
	<h1 id="head">Event Administration Portal</h1>
	<?php if ($_smarty_tpl->tpl_vars['logged_in']->value==1){?>
	<ul id="navigation">
			<li><a href="./management.php">Manage</a></li>
			<li><a href="./logout.php">Logout</a></li>
			<li><a href="./registration.php">Register</a></li>
			
		</ul>
		<?php }else{ ?>
		<ul id="navigation">
			<li><a href="./management.php">Manage</a></li>
			<li><a href="./main.php">Login</a></li>
			<li><a href="./registration.php">Register</a></li>
			
		</ul>
		<?php }?>
			<div id="content" class="container_16 clearfix">
				<div class="grid_16">
					<h2>New Event</h2>
				</div>
				</div>
				<div id="content" class="container_16 clearfix">
				<?php if ($_smarty_tpl->tpl_vars['eventerrs']->value!==''&&$_smarty_tpl->tpl_vars['eventer']->value==1){?>
				<p class="error" style="width:400px;margin-left:auto;margin-right:auto;"><?php echo $_smarty_tpl->tpl_vars['eventerrs']->value;?>
 --please check the required Numeric entries,make sure you have enter number in field where number is required, checkyour dates.</p>
				<?php }?>
				<form method="post" name="newevent" action="./new_event.php">
				
				<div class="grid_5">
					<p>
						<label for="title">Title <small>Only letters and numbers allowed.</small></label>
						<input type="text" name="title" />
					</p>
				</div>
				
			
				<div class="grid_6">
					<p>
						<label for="title">Category</label>
						<select name="paisa">
							<option>Paid</option>
							<option>Internship</option>
							<option>Volunteer</option>
						</select>
					</p>
				</div>
				<hr style="visibility:hidden;"></hr>
				<div class="grid_5">
					<p>
						<label for="title">Starting Time:  </label>
						<select style="width:45px;" name="strt_time1"><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option><option>9</option><option>10</option><option>11</option><option>12</option></select> : <select style="width:45px;" name="strt_time2"><option>10</option><option>20</option><option>30</option><option>40</option><option>50</option><option>60</option></select> : <select style="width:65px;" name="strt_time3"><option>AM</option><option>PM</option></select>
						
					</p>
				</div>
				
				<div class="grid_5">
					<p>
						<label for="title">Ending Time: </label>
						<select style="width:45px;" name="end_time1"><option>1</option><option>2</option><option>3</option><option>4</option><option>5</option><option>6</option><option>7</option><option>8</option><option>9</option><option>10</option><option>11</option><option>12</option></select> : <select style="width:45px;" name="end_time2"><option>10</option><option>20</option><option>30</option><option>40</option><option>50</option><option>60</option></select> : <select style="width:65px;" name="end_time3"><option>AM</option><option>PM</option></select>
					</p>
				</div>
				<div class="grid_5">
					<p>
						<label for="title">Date: <small>(Please enter  >2000, DD-MM-YY)</small> </label>
						<input type="text" name="date1" maxlength="2" style="width:30px;" /> / <input type="text" maxlength="2"  name="date2" style="width:30px;" /> / <input type="text" maxlength="4"  name="date3" style="width:60px;" />
					</p>
				</div>
			   	<hr style="visibility:hidden;"></hr>
				<div class="grid_5">
					<p>
						<label for="title">Primary Contact: <small>Only numbers allowed.</small></label>
						<input type="text" name="prim_contact" maxlength="10"/>
					</p>
				</div>
				
				<div class="grid_5">
					<p>
						<label for="title" style="width:400px">Secondary Contact: <small>Only numbers allowed.</small></label>
						<input type="text"  name="sec_contact" maxlength="10" />
					</p>
				</div>
				<hr style="visibility:hidden;"></hr>
				
			    <div class="grid_5">
					<p>
						<label for="title" >Address: <small>Enter a valid address for event.</small></label>
					</p>
				</div>
				<hr style="visibility:hidden;"></hr>
			    <div class="grid_5">
					<p>
						<label for="title" >Street: </label>
						<input type="text" name="street" style="width:200px;" />
					</p>
				</div>
				
				 <div class="grid_5">
					<p>
						<label for="title" >City: </label>
						<input type="text" name="city"  style="width:200px"/>
					</p>
				</div>
				
				<div class="grid_5">
					<p>
						<label for="title" >State: </label>
						<input type="text" name="state" style="width:200px"/>
					</p>
				</div>
					
				<div class="grid_5">
					<p>
						<label for="title" >Zip Code: </label>
						<input type="text" name="zip" style="width:200px"/>
					</p>
				</div>
					
				
				
				<div class="grid_16">
					<p>
						<label>Summary <small>Short description about event, No more than 250 characters are allowed</small></label>
						
						<textarea maxlength="250" name="summary" style="width:400px;height:120px;"></textarea>
					</p>
				</div>
				
				

				<div class="grid_16">
					<p class="submit">
						<input type="reset" value="Reset" />
						<input type="submit" value="Create Event" name="create" />
					</p>
				</div>
				</form>
			</div>
		
		<div id="foot">
					<a href="#">Contact Me</a>
		</div>
	</body>
</html><?php }} ?>