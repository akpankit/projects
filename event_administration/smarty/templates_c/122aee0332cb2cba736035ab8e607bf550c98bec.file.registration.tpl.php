<?php /* Smarty version Smarty-3.1.5, created on 2011-12-15 00:11:43
         compiled from "./smarty/templates/registration/registration.tpl" */ ?>
<?php /*%%SmartyHeaderCode:16159083754ee6cd046563a6-02646115%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '122aee0332cb2cba736035ab8e607bf550c98bec' => 
    array (
      0 => './smarty/templates/registration/registration.tpl',
      1 => 1323925866,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '16159083754ee6cd046563a6-02646115',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.5',
  'unifunc' => 'content_4ee6cd046e4e6',
  'variables' => 
  array (
    'login_flag' => 0,
    'error_flag' => 0,
    'errors' => 0,
    'fname' => 0,
    'lname' => 0,
    'email' => 0,
    'major' => 0,
    'phone' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_4ee6cd046e4e6')) {function content_4ee6cd046e4e6($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>User Registration</title>
		<link rel="stylesheet" href="./layout/css/960.css" type="text/css" media="screen" charset="utf-8" />
		<!--<link rel="stylesheet" href="css/fluid.css" type="text/css" media="screen" charset="utf-8" />-->
		<link rel="stylesheet" href="./layout/css/template.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="./layout/css/colour.css" type="text/css" media="screen" charset="utf-8" />
	</head>
	<body>
	<h1 id="head">Welcome to Event Administration Portal</h1>
	<?php if ($_smarty_tpl->tpl_vars['login_flag']->value==1){?>
	<ul id="navigation">
			
			<li><a href="./logout.php">Logout</a></li>
			<li><a class="active" href="#">Register</a></li>
			<li><a class="active" href="./management.php">Manage</a></li>
	</ul>
	<?php }else{ ?>
	<ul id="navigation">
			
			<li><a href="./main.php">Login</a></li>
			<li><a class="active" href="#">Register</a></li>
	</ul>
	<?php }?>
	
			<div id="content" class="container_16 clearfix">
				<div class="grid_16">
					<h2>New Volunteer Registration</h2>
					<?php if ($_smarty_tpl->tpl_vars['error_flag']->value==1){?>
					<span class="error"><marquee>To get best of our service, please provide valid details in your registration process to avoid further delays. Thank You and HAPPY HOLIDAYS!!</marquee></span>
					<?php }?>
					</div></div>
				<div id="content" class="container_16 clearfix">
				<form method="post" name="registration" action="./registration.php">	
				<?php $_smarty_tpl->tpl_vars["errors"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['errors']->value)===null||$tmp==='' ? '' : $tmp), null, 0);?>
				<?php if ($_smarty_tpl->tpl_vars['errors']->value['name']!==''&&$_smarty_tpl->tpl_vars['error_flag']->value==1&&$_smarty_tpl->tpl_vars['errors']->value['all']==''){?>
					<p class="error"><?php echo $_smarty_tpl->tpl_vars['errors']->value['name'];?>
</p>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['errors']->value['name1']!==''&&$_smarty_tpl->tpl_vars['error_flag']->value==1&&$_smarty_tpl->tpl_vars['errors']->value['all']==''){?>
					<p class="error"><?php echo $_smarty_tpl->tpl_vars['errors']->value['name1'];?>
</p>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['errors']->value['email']!==''&&$_smarty_tpl->tpl_vars['error_flag']->value==1&&$_smarty_tpl->tpl_vars['errors']->value['all']==''){?>
					<p class="error"><?php echo $_smarty_tpl->tpl_vars['errors']->value['email'];?>
</p>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['errors']->value['password']!==''&&$_smarty_tpl->tpl_vars['error_flag']->value==1&&$_smarty_tpl->tpl_vars['errors']->value['all']==''){?>
					<p class="error"><?php echo $_smarty_tpl->tpl_vars['errors']->value['password'];?>
</p>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['errors']->value['major']!==''&&$_smarty_tpl->tpl_vars['error_flag']->value==1&&$_smarty_tpl->tpl_vars['errors']->value['all']==''){?>
					<p class="error"><?php echo $_smarty_tpl->tpl_vars['errors']->value['major'];?>
</p>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['errors']->value['phone']!==''&&$_smarty_tpl->tpl_vars['error_flag']->value==1&&$_smarty_tpl->tpl_vars['errors']->value['all']==''){?>
					<p class="error"><?php echo $_smarty_tpl->tpl_vars['errors']->value['phone'];?>
</p>
				<?php }?>
				<?php if ($_smarty_tpl->tpl_vars['errors']->value['all']!==''&&$_smarty_tpl->tpl_vars['error_flag']->value==1){?>
					<p class="error"><?php echo $_smarty_tpl->tpl_vars['errors']->value['all'];?>
</p>
				<?php }?>
				<div class="grid_5">
					<p>
					
						<label for="title">First Name <small> Only letters are allowed.</small></label>
						<?php if ($_smarty_tpl->tpl_vars['errors']->value['name']==''&&$_smarty_tpl->tpl_vars['error_flag']->value==1){?>
						<input type="text"  value="<?php echo $_smarty_tpl->tpl_vars['fname']->value;?>
" name="first_name"/>
						<?php }else{ ?>
						<input type="text"  name="first_name"/>
						<?php }?>
						
					</p>
				</div>
				
				<div class="grid_5">
					<p>
					
						<label for="title">Last Name <small> Only letters are allowed.</small></label>
						<?php if ($_smarty_tpl->tpl_vars['errors']->value['lname']==''&&$_smarty_tpl->tpl_vars['error_flag']->value==1){?>
						<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['lname']->value;?>
" name="last_name" />
						<?php }else{ ?>
						<input type="text" name="last_name" />
						<?php }?>
					</p>
				</div>
				<hr style="visibility:hidden;"></hr>
				<div class="grid_5">
					<p>
						<label for="title">E-mail<small>Please provide valid e-mail address</small></label>
						<?php if ($_smarty_tpl->tpl_vars['errors']->value['email']==''&&$_smarty_tpl->tpl_vars['error_flag']->value==1){?>
						<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['email']->value;?>
" name="e-mail" />
						<?php }else{ ?>
						<input type="text"  name="e-mail" />
						<?php }?>
						
					</p>
				</div>
				
				<div class="grid_5">
					<p>
					
						<label for="title">Password<small>Atleast 4 characters long .</small></label>
						<input type="password"   name="password" />
					</p>
				</div>
				<div class="grid_5">
					<p>
					
						<label for="title">Major</label>
						<?php if ($_smarty_tpl->tpl_vars['errors']->value['major']==''&&$_smarty_tpl->tpl_vars['error_flag']->value==1){?>
						<input type="text" value="<?php echo $_smarty_tpl->tpl_vars['major']->value;?>
"  name="major" />
						<?php }else{ ?>
						<input type="text" name="major" />
						<?php }?>
						
					</p>
				</div>
				
				<hr style="visibility:hidden;"></hr>
				<div class="grid_5">
					<p>
						<label>Gender</label>
						<select name="gender">
												
							<option value="male"  >Male</option>
							<option value="female"  >Female</option>
							
						</select>
					</p>
				</div>
				<div class="grid_5">
					<p>
						<label>Grade Level</label>
						<select name="level">
								
							<option  value="sophmore">Sophmore</option>
							<option  value="junior">Junior</option>
							<option  value="senior">Senior</option>
						</select>
					</p>
				</div>
				<div class="grid_5">
					<p>
					
						<label for="title">Phone<small>(e.g. 123-456-7890).</small></label>
						<?php if ($_smarty_tpl->tpl_vars['errors']->value['phone']==''&&$_smarty_tpl->tpl_vars['error_flag']->value==1){?>
						<input type="text"   value="<?php echo $_smarty_tpl->tpl_vars['phone']->value;?>
" name="phone" />
						<?php }else{ ?>
						<input type="text" name="phone" />
						<?php }?>
						
					</p>
				</div>
				
				<div class="grid_16">
					<p class="submit">
						<input type="reset" value="Reset" />
						<input type="submit" name="approve" value="Get Approved" />
					</p>
				</div>
				</form>
			</div>

			<div id="foot">
					<a href="#">Contact Me</a>
			</div>


</body>
</html>
				
				
				
			
			<?php }} ?>