<?php /* Smarty version Smarty-3.1.5, created on 2011-12-15 03:08:33
         compiled from "./smarty/templates/management/manage_users.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12526458904ee79d8f259916-38767604%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b818af4f795a758cb2d0ffc5a4b378afbe09f7f0' => 
    array (
      0 => './smarty/templates/management/manage_users.tpl',
      1 => 1323936469,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12526458904ee79d8f259916-38767604',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.5',
  'unifunc' => 'content_4ee79d8f2c978',
  'variables' => 
  array (
    'logged_in' => 0,
    'data' => 0,
    'user' => 0,
    'key' => 0,
    'value' => 0,
    'name_id' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_4ee79d8f2c978')) {function content_4ee79d8f2c978($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>Management</title>
		<link rel="stylesheet" href="./layout/css/960.css" type="text/css" media="screen" charset="utf-8" />
		<!--<link rel="stylesheet" href="css/fluid.css" type="text/css" media="screen" charset="utf-8" />-->
		<link rel="stylesheet" href="./layout/css/template.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="./layout/css/colour.css" type="text/css" media="screen" charset="utf-8" />
		
		
		<script src="./layout/js/jquery-1.4.2.js" type="text/javascript"></script>
		<script src="./layout/js/jquery-ui-1.8.1.js" type="text/javascript"></script>
		<script type="text/javascript">
		$(document).ready(function()
	    		{
	    	
		
		$("a.edit").click(function(){
			//alert("edit clicked");
			//alert($(this).attr('name'));
			$.post("./edit_user.php", { edit_user:$(this).attr('name')},
					function(data){
				 console.log(data); 
				  // alert(data);
				  window.location.replace("./test.php");
				   });
			});
		
		$("a.delete").click(function(){
			//alert("delete clicked");
			//alert($(this).attr('name'));
			$.post("./user_functions.php", { delete_user:$(this).attr('name')},
					function(data){
				 console.log(data); 
				   //alert(data);
				   });
			});

		$("a.approve").click(function(){
			//alert("approve clicked");
			//alert($(this).attr('name'));
			$.post("./user_functions.php", { approve_user:$(this).attr('name')},
					function(data){
				 console.log(data); 
				  //alert(data);
				  });
			});
		


		
	    		});
		</script>
		
		
		
		
		
	</head>
	<body>
		
					<h1 id="head">Event Administration Portal</h1>
	<?php if ($_smarty_tpl->tpl_vars['logged_in']->value==1){?>	
		<ul id="navigation">
			<li><a href ="./management.php"><span class="active">Manage Users</span></a></li>
			<li><a href="./manage_event.php">Manage Events</a></li>
			<li><a href="./logout.php">Logout</a></li>
			
		</ul>
		<?php }?>
			<div id="content" class="container_16 clearfix">
			<div class="grid_16">
					<h2>Management</h2>
				</div>
				<form method="post" name="search" action="./management.php">
				<div class="grid_5">
					<p>
						<label>Email Address</label>
						<input type="text" name="search_email" />
					</p>
				</div>
				<div class="grid_5">
					<p>
						<label>Category</label>
						<select name="search_level" >
							<option >select</option>
							<option>Sophmore</option>
							<option>Junior</option>
							<option>Senior</option>
						</select>
					</p>
				</div>
				<div class="grid_2">
					<p>
						<label>&nbsp;</label>
						<input type="submit" value="Search" name="searches" />
					</p>
				</div>
				</form>
				<div class="grid_2">
					<p>
						<label>&nbsp;</label>
						<a href="./registration.php"><input type="submit" name="add_user" value="Add Volunteer" /></a>
					</p>
				</div>
				<div class="grid_16">
					<table>
						<thead>
							<tr>
								
								<th style="width:25%;">Username</th>
								<th style="width:25%;">Grade Level</th>
								<th style="width:25%;">Status</th>
								<th colspan="2" style="width:25%">Actions</th>
							</tr>
						</thead>
						
						<tbody>
						<?php $_smarty_tpl->tpl_vars["data"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['data']->value)===null||$tmp==='' ? '' : $tmp), null, 0);?>
						<?php  $_smarty_tpl->tpl_vars['user'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['user']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['data']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['user']->key => $_smarty_tpl->tpl_vars['user']->value){
$_smarty_tpl->tpl_vars['user']->_loop = true;
?>
						
							<tr class="alt">
  								<?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['value']->_loop = false;
 $_smarty_tpl->tpl_vars['key'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['user']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
$_smarty_tpl->tpl_vars['value']->_loop = true;
 $_smarty_tpl->tpl_vars['key']->value = $_smarty_tpl->tpl_vars['value']->key;
?>
  									<?php if ($_smarty_tpl->tpl_vars['key']->value=='email'){?>
  										<?php $_smarty_tpl->tpl_vars["name_id"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['value']->value)===null||$tmp==='' ? '' : $tmp), null, 0);?>
  									<?php }?>
  										<?php if ($_smarty_tpl->tpl_vars['key']->value=="approval"&&$_smarty_tpl->tpl_vars['value']->value==10){?>
  											<td><a  href="./management.php" class="approve" name="<?php echo $_smarty_tpl->tpl_vars['name_id']->value;?>
">Inactive/Approve?</a></td><?php }?>
  										<?php if ($_smarty_tpl->tpl_vars['key']->value=="approval"&&$_smarty_tpl->tpl_vars['value']->value==11){?>
	  										<td>Active</td>
  										<?php }?>
  									<?php if ($_smarty_tpl->tpl_vars['key']->value!='approval'){?>
  										<td><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</td>
  									<?php }?>
  								<?php } ?>
  								
  								<td><a  href="#" class="edit" name="<?php echo $_smarty_tpl->tpl_vars['name_id']->value;?>
">Modify</a></td>
								<td><a  href="./management.php" class="delete" name="<?php echo $_smarty_tpl->tpl_vars['name_id']->value;?>
">Delete</a></td>
								
								
							</tr>
						<?php } ?>
													
						</tbody>
					</table>
				</div>
			</div>
		
		<div id="foot">
					<a href="#">Contact Me at <b>yopresent@gmail.com</b></a>
				
		</div>
	</body>
</html><?php }} ?>