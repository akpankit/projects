<?php /* Smarty version Smarty-3.1.5, created on 2011-12-15 02:18:08
         compiled from "./smarty/templates/management/manage_events.tpl" */ ?>
<?php /*%%SmartyHeaderCode:18078178964ee86819d7d6a9-58477821%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '326a1a653e79f431d9545631023914a2b39cb4fa' => 
    array (
      0 => './smarty/templates/management/manage_events.tpl',
      1 => 1323933052,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '18078178964ee86819d7d6a9-58477821',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.5',
  'unifunc' => 'content_4ee86819de0ac',
  'variables' => 
  array (
    'logged_in' => 0,
    'datae' => 0,
    'cake' => 0,
    'value' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_4ee86819de0ac')) {function content_4ee86819de0ac($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>Management</title>
		<link rel="stylesheet" href="./layout/css/960.css" type="text/css" media="screen" charset="utf-8" />
		<!--<link rel="stylesheet" href="css/fluid.css" type="text/css" media="screen" charset="utf-8" />-->
		<link rel="stylesheet" href="./layout/css/template.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="./layout/css/colour.css" type="text/css" media="screen" charset="utf-8" />
		<script src="./layout/js/jquery-1.4.2.js" type="text/javascript"></script>
		<script src="./layout/js/jquery-ui-1.8.1.js" type="text/javascript"></script>
		<script type="text/javascript">
		$(document).ready(function()
	    		{
	    	
		
		
		
		$("a.delete").click(function(){
			alert("delete clicked");
			alert($(this).attr('name'));
			$.post("./manage_event.php", { delete_event:$(this).attr('name')},
					function(data){
				 console.log(data); 
				 window.location.replace("./manage_event.php");});
			});

			
	    		});
		</script>
		
		
	</head>
	<body>
		
					<h1 id="head">Event Administration Portal</h1>
		
		<?php if ($_smarty_tpl->tpl_vars['logged_in']->value==1){?>	
		<ul id="navigation">
			<li><a href ="./management.php"><span class="active">Manage Users</span></a></li>
			<li><a href="./manage_event.php">Manage Events</a></li>
			<li><a href="./logout.php">Logout</a></li>
			
		</ul>
		<?php }?>
		
			<div id="content" class="container_16 clearfix">
			<div class="grid_16">
					<h2>Events happening, what are you doing ?</h2>
				</div>
				<div class="grid_2">
					<p>
						<label>&nbsp;</label>
						<a href="./new_event.php"><input type="submit" name="add_event" value="Add Event" /></a>
					</p>
				</div>
				
				
				<div class="grid_16">
					<table>
						<thead>
							<tr>
								<th style="width:300px;">Event</th>
								<th style="width:50px;">Starting At (12 hr format)</th>
								<th style="width:50px;">Ending At(12 hr format)</th>
								<th colspan="2" width="10%">Actions</th>
							</tr>
						</thead>
						
						<tbody>
						<?php $_smarty_tpl->tpl_vars["datae"] = new Smarty_variable((($tmp = @$_smarty_tpl->tpl_vars['datae']->value)===null||$tmp==='' ? '' : $tmp), null, 0);?>
						<?php  $_smarty_tpl->tpl_vars['cake'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cake']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['datae']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cake']->key => $_smarty_tpl->tpl_vars['cake']->value){
$_smarty_tpl->tpl_vars['cake']->_loop = true;
?>
							<tr class="alt">
						<?php  $_smarty_tpl->tpl_vars['value'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['value']->_loop = false;
 $_smarty_tpl->tpl_vars['even'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['cake']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['value']->key => $_smarty_tpl->tpl_vars['value']->value){
$_smarty_tpl->tpl_vars['value']->_loop = true;
 $_smarty_tpl->tpl_vars['even']->value = $_smarty_tpl->tpl_vars['value']->key;
?>
							
								<td><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</td>
								<?php } ?>
								<td><a href="#" class="delete" name="<?php echo $_smarty_tpl->tpl_vars['cake']->value['description'];?>
">Delete</a></td>
								
									</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		
		<div id="foot">
					<a href="#">Contact Me</a>
				
		</div>
	</body>
</html><?php }} ?>