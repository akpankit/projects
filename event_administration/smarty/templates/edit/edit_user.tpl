<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>User Registration</title>
		<link rel="stylesheet" href="./layout/css/960.css" type="text/css" media="screen" charset="utf-8" />
		<!--<link rel="stylesheet" href="css/fluid.css" type="text/css" media="screen" charset="utf-8" />-->
		<link rel="stylesheet" href="./layout/css/template.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="./layout/css/colour.css" type="text/css" media="screen" charset="utf-8" />
		<script src="./layout/js/jquery-1.4.2.js" type="text/javascript"></script>
		<script src="./layout/js/jquery-ui-1.8.1.js" type="text/javascript"></script>
		<script type="text/javascript">
		$(document).ready(function()
	    		{
					$(".verify").click(function(){
						alert("Updated,Thank you");
					});


	    		});
		</script>
	</head>
	<body>
	<h1 id="head">Welcome to Event Administration Portal</h1>
	{if $logged_in==0}
	<ul id="navigation">
			
			<li><a href="./main.php">Login</a></li>
			<li><a class="active" href="#">Register</a></li>
			
		</ul>
		{else}
		<ul id="navigation">
			
			<li><a href="./logout.php">Logout</a></li>
			<li><a class="active" href="./registration.php">Register</a></li>
			<li><a class="active" href="./management.php">Manage</a></li>
		</ul>
		
		{/if}
			<div id="content" class="container_16 clearfix">
				<div class="grid_16">
					<h2>Edit User Information</h2>
					</div></div>
				<div id="content" class="container_16 clearfix">
				<form method="post" name="edit_user"  id="edit_u" action="./test.php">	
		
				{if $error_flag==1  }
					<p class="error">{$errors2}</p>
				{/if}
						
				<div class="grid_5">
					<p>
					
						<label for="title">First Name <small> Only letters are allowed.</small></label>
						
							<input type="text" value="{$ename}"  name="first_name"/>
									
					</p>
				</div>
				
				<div class="grid_5">
					<p>
					
						<label for="title">Last Name <small> Only letters are allowed.</small></label>
							<input type="text" value="{$elname}" name="last_name" />
						
					</p>
				</div>
				<hr style="visibility:hidden;"></hr>
				<div class="grid_5">
					<p>
						<label for="title">E-mail<small>Please provide valid e-mail address</small></label>
						
						<input type="text"  value="{$email}" name="e-mail" disabled="disabled"  />
						
						
					</p>
				</div>
				
				
				<div class="grid_5">
					<p>
					
						<label for="title">Major</label>
							<input type="text" value="{$emajor}" name="major" />
						
						
					</p>
				</div>
				
				<hr style="visibility:hidden;"></hr>
				<div class="grid_5">
					<p>
						<label>Gender</label>
						<select name="gender">
							{if $egender==m}						
							<option value="male" selected>Male</option>
							{else}
							<option value="male" >Male</option>
							{/if}
							{if $egender==f}
							<option value="female" selected>Female</option>
							{else}
							<option value="female" >Female</option>
							{/if}
							
						</select>
					</p>
				</div>
				<div class="grid_5">
					<p>
						<label>Grade Level</label>
						<select name="level">
							{if $elevel==sophmore}	
							<option  value="sophmore" selected>Sophmore</option>
							{else}
							<option  value="sophmore" >Sophmore</option>
							{/if}
							{if $elevel==junior}
							<option  value="junior" selected>Junior</option>
							{else}
							<option  value="junior" >Junior</option>
							{/if}
							{if $elevel==senior}
							<option  value="senior" selected>Senior</option>
							{else}
							<option  value="senior" >Senior</option>
							{/if}
						</select>
					</p>
				</div>
				<div class="grid_5">
					<p>
					
						<label for="title">Phone<small>(e.g. 123-456-7890 or 1234567890).</small></label>
						
						<input type="text" value="{$ephone}" name="phone" />
						
						
					</p>
				</div>
				
				<div class="grid_16">
					<p class="submit">
						
						<input type="submit" class="verify" value="Modify" name="updt" />
					</p>
				</div>
				</form>
			</div>

			<<div id="foot">
					<a href="#">Contact Me at <b>yopresent@gmail.com</b></a>
				
		</div>


</body>
</html>
				
				
				
			
			