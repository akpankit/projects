<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>User Registration</title>
		<link rel="stylesheet" href="./layout/css/960.css" type="text/css" media="screen" charset="utf-8" />
		<!--<link rel="stylesheet" href="css/fluid.css" type="text/css" media="screen" charset="utf-8" />-->
		<link rel="stylesheet" href="./layout/css/template.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="./layout/css/colour.css" type="text/css" media="screen" charset="utf-8" />
	</head>
	<body>
	<h1 id="head">Welcome to Event Administration Portal</h1>
	{if $logged_in==1}
	<ul id="navigation">
			
			<li><a href="./logout.php">Logout</a></li>
			<li><a class="active" href="#">Register</a></li>
			<li><a class="active" href="./management.php">Manage</a></li>
	</ul>
	{else}
	<ul id="navigation">
			
			<li><a href="./main.php">Login</a></li>
			<li><a class="active" href="#">Register</a></li>
	</ul>
	{/if}
	
			<div id="content" class="container_16 clearfix">
				<div class="grid_16">
					<h2>New Volunteer Registration</h2>
					{if $error_flag==1}
					<span class="error"><marquee>To get best of our service, please provide valid details in your registration process to avoid further delays. Thank You and HAPPY HOLIDAYS!!</marquee></span>
					{/if}
					</div></div>
				<div id="content" class="container_16 clearfix">
				<form method="post" name="registration" action="./registration.php">	
				{assign var="errors" value=$errors|default:""}
				{if $errors.name !=='' && $error_flag==1 && $errors.all==''}
					<p class="error">{$errors.name}</p>
				{/if}
				{if $errors.name1 !==''  && $error_flag==1 && $errors.all==''}
					<p class="error">{$errors.name1}</p>
				{/if}
				{if $errors.email !==''  && $error_flag==1 && $errors.all==''}
					<p class="error">{$errors.email}</p>
				{/if}
				{if $errors.password !==''  && $error_flag==1 && $errors.all==''}
					<p class="error">{$errors.password}</p>
				{/if}
				{if $errors.major !==''  && $error_flag==1 && $errors.all==''}
					<p class="error">{$errors.major}</p>
				{/if}
				{if $errors.phone !==''  && $error_flag==1 && $errors.all==''}
					<p class="error">{$errors.phone}</p>
				{/if}
				{if $errors.all !==''  && $error_flag==1 }
					<p class="error">{$errors.all}</p>
				{/if}
				<div class="grid_5">
					<p>
					
						<label for="title">First Name <small> Only letters are allowed.</small></label>
						{if $errors.name =='' && $error_flag==1 }
						<input type="text"  value="{$fname}" name="first_name"/>
						{else}
						<input type="text"  name="first_name"/>
						{/if}
						
					</p>
				</div>
				
				<div class="grid_5">
					<p>
					
						<label for="title">Last Name <small> Only letters are allowed.</small></label>
						{if $errors.lname =='' && $error_flag==1 }
						<input type="text" value="{$lname}" name="last_name" />
						{else}
						<input type="text" name="last_name" />
						{/if}
					</p>
				</div>
				<hr style="visibility:hidden;"></hr>
				<div class="grid_5">
					<p>
						<label for="title">E-mail<small>Please provide valid e-mail address</small></label>
						{if $errors.email =='' && $error_flag==1 }
						<input type="text" value="{$email}" name="e-mail" />
						{else}
						<input type="text"  name="e-mail" />
						{/if}
						
					</p>
				</div>
				
				<div class="grid_5">
					<p>
					
						<label for="title">Password<small>Atleast 4 characters long .</small></label>
						<input type="password"   name="password" />
					</p>
				</div>
				<div class="grid_5">
					<p>
					
						<label for="title">Major</label>
						{if $errors.major =='' && $error_flag==1 }
						<input type="text" value="{$major}"  name="major" />
						{else}
						<input type="text" name="major" />
						{/if}
						
					</p>
				</div>
				
				<hr style="visibility:hidden;"></hr>
				<div class="grid_5">
					<p>
						<label>Gender</label>
						<select name="gender">
												
							<option value="male"  >Male</option>
							<option value="female"  >Female</option>
							
						</select>
					</p>
				</div>
				<div class="grid_5">
					<p>
						<label>Grade Level</label>
						<select name="level">
								
							<option  value="sophmore">Sophmore</option>
							<option  value="junior">Junior</option>
							<option  value="senior">Senior</option>
						</select>
					</p>
				</div>
				<div class="grid_5">
					<p>
					
						<label for="title">Phone<small>(e.g. 123-456-7890).</small></label>
						{if $errors.phone =='' && $error_flag==1 }
						<input type="text"   value="{$phone}" name="phone" />
						{else}
						<input type="text" name="phone" />
						{/if}
						
					</p>
				</div>
				
				<div class="grid_16">
					<p class="submit">
						<input type="reset" value="Reset" />
						<input type="submit" name="approve" value="Get Approved" />
					</p>
				</div>
				</form>
			</div>

			<div id="foot">
					<a href="#">Contact Me at <b>yopresent@gmail.com</b></a>
				
		</div>


</body>
</html>
				
				
				
			
			