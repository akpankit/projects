<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>Management</title>
		<link rel="stylesheet" href="./layout/css/960.css" type="text/css" media="screen" charset="utf-8" />
		<!--<link rel="stylesheet" href="css/fluid.css" type="text/css" media="screen" charset="utf-8" />-->
		<link rel="stylesheet" href="./layout/css/template.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="./layout/css/colour.css" type="text/css" media="screen" charset="utf-8" />
		<script src="./layout/js/jquery-1.4.2.js" type="text/javascript"></script>
		<script src="./layout/js/jquery-ui-1.8.1.js" type="text/javascript"></script>
		<script type="text/javascript">
		$(document).ready(function()
	    		{
	    	
		
		
		
		$("a.delete").click(function(){
			//alert("delete clicked");
			//alert($(this).attr('name'));
			$.post("./manage_event.php", { delete_event:$(this).attr('name')},
					function(data){
				 console.log(data); 
				 window.location.replace("./manage_event.php");});
			});

			
	    		});
		</script>
		
		
	</head>
	<body>
		
					<h1 id="head">Event Administration Portal</h1>
		
		{if $logged_in==1}	
		<ul id="navigation">
			<li><a href ="./management.php"><span class="active">Manage Users</span></a></li>
			<li><a href="./manage_event.php">Manage Events</a></li>
			<li><a href="./logout.php">Logout</a></li>
			
		</ul>
		{/if}
		
			<div id="content" class="container_16 clearfix">
			<div class="grid_16">
					<h2>Events happening, what are you doing ?</h2>
				</div>
				<div class="grid_2">
					<p>
						<label>&nbsp;</label>
						<a href="./new_event.php"><input type="submit" name="add_event" value="Add Event" /></a>
					</p>
				</div>
				
				
				<div class="grid_16">
					<table>
						<thead>
							<tr>
								<th style="width:300px;">Event</th>
								<th style="width:50px;">Starting At (12 hr format)</th>
								<th style="width:50px;">Ending At(12 hr format)</th>
								<th colspan="2" width="10%">Actions</th>
							</tr>
						</thead>
						
						<tbody>
						{assign var="datae" value=$datae|default:""}
						{foreach $datae as $cake}
							<tr class="alt">
						{foreach $cake as $even=>$value}
							
								<td>{$value}</td>
								{/foreach}
								<td><a href="#" class="delete" name="{$cake.description}">Delete</a></td>
								
									</tr>
							{/foreach}
						</tbody>
					</table>
				</div>
			</div>
		
		<div id="foot">
					<a href="#">Contact Me at <b>yopresent@gmail.com</b></a>
				
		</div>
	</body>
</html>