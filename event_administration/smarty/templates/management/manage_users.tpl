<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>Management</title>
		<link rel="stylesheet" href="./layout/css/960.css" type="text/css" media="screen" charset="utf-8" />
		<!--<link rel="stylesheet" href="css/fluid.css" type="text/css" media="screen" charset="utf-8" />-->
		<link rel="stylesheet" href="./layout/css/template.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="./layout/css/colour.css" type="text/css" media="screen" charset="utf-8" />
		
		
		<script src="./layout/js/jquery-1.4.2.js" type="text/javascript"></script>
		<script src="./layout/js/jquery-ui-1.8.1.js" type="text/javascript"></script>
		<script type="text/javascript">
		$(document).ready(function()
	    		{
	    	
		
		$("a.edit").click(function(){
			//alert("edit clicked");
			//alert($(this).attr('name'));
			$.post("./edit_user.php", { edit_user:$(this).attr('name')},
					function(data){
				 console.log(data); 
				  // alert(data);
				  window.location.replace("./test.php");
				   });
			});
		
		$("a.delete").click(function(){
			//alert("delete clicked");
			//alert($(this).attr('name'));
			$.post("./user_functions.php", { delete_user:$(this).attr('name')},
					function(data){
				 console.log(data); 
				   //alert(data);
				   });
			});

		$("a.approve").click(function(){
			//alert("approve clicked");
			//alert($(this).attr('name'));
			$.post("./user_functions.php", { approve_user:$(this).attr('name')},
					function(data){
				 console.log(data); 
				  //alert(data);
				  });
			});
		


		
	    		});
		</script>
		
		
		
		
		
	</head>
	<body>
		
					<h1 id="head">Event Administration Portal</h1>
	{if  $logged_in==1}	
		<ul id="navigation">
			<li><a href ="./management.php"><span class="active">Manage Users</span></a></li>
			<li><a href="./manage_event.php">Manage Events</a></li>
			<li><a href="./logout.php">Logout</a></li>
			
		</ul>
		{/if}
			<div id="content" class="container_16 clearfix">
			<div class="grid_16">
					<h2>Management</h2>
				</div>
				<form method="post" name="search" action="./management.php">
				<div class="grid_5">
					<p>
						<label>Email Address</label>
						<input type="text" name="search_email" />
					</p>
				</div>
				<div class="grid_5">
					<p>
						<label>Category</label>
						<select name="search_level" >
							<option >select</option>
							<option>Sophmore</option>
							<option>Junior</option>
							<option>Senior</option>
						</select>
					</p>
				</div>
				<div class="grid_2">
					<p>
						<label>&nbsp;</label>
						<input type="submit" value="Search" name="searches" />
					</p>
				</div>
				</form>
				<div class="grid_2">
					<p>
						<label>&nbsp;</label>
						<a href="./registration.php"><input type="submit" name="add_user" value="Add Volunteer" /></a>
					</p>
				</div>
				<div class="grid_16">
					<table>
						<thead>
							<tr>
								
								<th style="width:25%;">Username</th>
								<th style="width:25%;">Grade Level</th>
								<th style="width:25%;">Status</th>
								<th colspan="2" style="width:25%">Actions</th>
							</tr>
						</thead>
						
						<tbody>
						{assign var="data" value=$data|default:""}
						{foreach $data as $user}
						
							<tr class="alt">
  								{foreach $user as $key => $value}
  									{if $key==email}
  										{assign var="name_id" value=$value|default:""}
  									{/if}
  										{if $key=="approval" && $value==10}
  											<td><a  href="./management.php" class="approve" name="{$name_id}">Inactive/Approve?</a></td>{/if}
  										{if $key=="approval" && $value==11}
	  										<td>Active</td>
  										{/if}
  									{if $key!=approval}
  										<td>{$value}</td>
  									{/if}
  								{/foreach}
  								
  								<td><a  href="#" class="edit" name="{$name_id}">Modify</a></td>
								<td><a  href="./management.php" class="delete" name="{$name_id}">Delete</a></td>
								
								
							</tr>
						{/foreach}
													
						</tbody>
					</table>
				</div>
			</div>
		
		<div id="foot">
					<a href="#">Contact Me at <b>yopresent@gmail.com</b></a>
				
		</div>
	</body>
</html>