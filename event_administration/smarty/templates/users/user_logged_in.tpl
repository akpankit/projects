<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>Management</title>
		<link rel="stylesheet" href="./layout/css/960.css" type="text/css" media="screen" charset="utf-8" />
		<!--<link rel="stylesheet" href="css/fluid.css" type="text/css" media="screen" charset="utf-8" />-->
		<link rel="stylesheet" href="./layout/css/template.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="./layout/css/colour.css" type="text/css" media="screen" charset="utf-8" />
		<script src="./layout/js/jquery-1.4.2.js" type="text/javascript"></script>
		<script src="./layout/js/jquery-ui-1.8.1.js" type="text/javascript"></script>
		<script type="text/javascript">
		$(document).ready(function()
	    		{
	    	
		
		$("#join").click(function(){
			//alert("edit clicked");
			//alert($(this).attr('name'));
			
			$.post("./joined_action.php", { join:$(this).attr('name')},
					function(data){
				 console.log(data); 
				   alert(data);
				   // window.location.replace("./logged_in.php");
				  
				   });
			});
		
		});
		</script>
		
	</head>
	<body>
		
					<h1 id="head">Event Administration Portal</h1>
		{if $logged_in==1}
		<ul id="navigation">
			<li><a href="./logout.php">Logout</a></li>
		</ul>
		{/if}
		
			<div id="content" class="container_16 clearfix">
			<div class="grid_16">
					<h2>Events happening, what are you doing ?</h2>
				</div>
				
					
				
				
				<div class="grid_16">
					<table>
						<thead>
							<tr>
								<th style="width:350px;">Event</th>
								<th style="width:50px;">Starting At</th>
								<th style="width:50px;">Ending At</th>
								<th style="width:50px;">Type</th>
								<th colspan="2" width="10%">Actions</th>
							</tr>
						</thead>
						
						<tbody>
							{assign var="fre" value=$evedata|default:""}
						{foreach $fre as $event}
							<tr class="alt">
							{foreach $event as $kj => $ij}
								{if $kj==eve_id}
									{assign var="event_id" value=$ij|default:""}
								{/if}
								{if $kj!==eve_id}
									<td>{$ij}</td>
									{/if}
							{/foreach}
								<td><a href="#" class="delete" id="join" name="{$event_id}">Join</a></td>
								
									</tr>
							{/foreach}
						</tbody>
					</table>
				</div>
			</div>
		
		<div id="foot">
					<a href="#">Contact Me at <b>yopresent@gmail.com</b></a>
				
		</div>
	</body>
</html>