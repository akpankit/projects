<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<title>CCNY Event Administration Portal</title>
		<link rel="stylesheet" href="./layout/css/960.css" type="text/css" media="screen" charset="utf-8" />
		<!--<link rel="stylesheet" href="css/fluid.css" type="text/css" media="screen" charset="utf-8" />-->
		<link rel="stylesheet" href="./layout/css/template.css" type="text/css" media="screen" charset="utf-8" />
		<link rel="stylesheet" href="./layout/css/colour.css" type="text/css" media="screen" charset="utf-8" />
	</head>
	<body>
		
					<h1 id="head" style="text-align:center;">Welcome to CCNY Event Administration Portal</h1>
		
		<ul id="navigation">
			<li><span class="active"><marquee>Hundred's of new and life-changing events are happening at CCNY. If you are member of CCNY we strongly encourage you to sign up to our new event monitoring system.</marquee></span></li>
			
		</ul>
		
			<div id="content" class="container_16 clearfix">
			<div class="grid_16">
					<h2>Log In</h2>
				</div>
				<form method="post" id="signin" action="./main.php">
				<div class="grid_5">
				{if $notfo==1 && $notfound!==''}
				<p class="error">{$notfound}</p>{/if}
				{if $notappr==1 && $notapprs!==''}
				<p class="error">{$notfound}</p>{/if}
					<p>
									
						<label>Username: </label> <input type="text" name="username" id="username"/>
						<label>Password: </label> <input type="password" name="password" id="password"/>
						
					</p>
				</div>
				
				<div class="grid_16">
					<p>
						<label>&nbsp;</label>
						<input type="submit" value="Log In" name="login" />
						<a href="./registration.php">Click here to <b>Register</b> if you are new user</a>
					</p>
				</div>
				</form>
				</div>
			
		
		<div id="foot">
					<a href="#">Contact Me at <b>yopresent@gmail.com</b></a>
				
		</div>
	</body>
</html>