<?php
require_once('database.php');
/*error_reporting(E_ALL);
 ini_set('display_errors', 1);*/

function check_email($email)
{
	global $error_messages;
	$email_error = false;
	
	if ($email==""){
		global $error_messages;
		$er ="* E-mail entry required";
		$error_messages['email']=$er;return false;
	}else{
		
		$Email = htmlspecialchars(stripslashes(strip_tags(trim($email)))); //parse unnecessary characters to prevent exploits
		if ($Email == "") { $email_error = true; }
		elseif (!eregi("^([a-zA-Z0-9._-])+@([a-zA-Z0-9._-])+\.([a-zA-Z0-9._-])([a-zA-Z0-9._-])+", $Email)) { $email_error = true; }
		else {
			list($Email, $domain) = split("@", $Email, 2);
			if (! checkdnsrr($domain, "MX")) { $email_error = true; }
			else {
				$array = array($Email, $domain);
				$Email = implode("@", $array);
			}
		}
	}
	
	/*$check = query("select email from person where email='$email'");
	print_r($check);*/
	
	if(query("select email from person where email='$email'"))
	{


		$er ="* Email already exists!";
		$error_messages['email']=$er;
			
		return false;
	}


	if ($email_error) {
		 
		global $error_messages;
		$er = "* Error occured while processing your E-mail, please enter valid e-mail address";
		$error_messages['email']=$er;

		return false;
	}
	else{return true;}
}
//////////////////////////////NAMES CHECKING//////////////////////////////////////
function names($fnames,$lnames)
{
	if ($fnames=="" || $lnames==""){global $error_messages;
	$er ="* Names entry required";
	$error_messages['name']=$er; return false;
	}else{
		//$smarty->assign('namer', 'cannot use symbols,special characters for names');
		if(preg_match("#^[-A-Za-z' ]*$#",$fnames) && preg_match("#^[-A-Za-z' ]*$#",$lnames))
		{return true;}
		else if (!preg_match("#^[-A-Za-z' ]*$#",$fnames) && preg_match("#^[-A-Za-z' ]*$#",$lnames))
		{
		 global $error_messages;
		 $er = "* cannot use symbols,special characters for first name";
		 $error_messages['name1']=$er;

		 return false;
		}
		else if(!preg_match("#^[-A-Za-z' ]*$#",$lnames) && preg_match("#^[-A-Za-z' ]*$#",$fnames))
		{
			global $error_messages;
			$er = "* cannot use symbols,special characters for last name";
			$error_messages['name']=$er;

			return false;
		}
		else if(!preg_match("#^[-A-Za-z' ]*$#",$fnames) && !preg_match("#^[-A-Za-z' ]*$#",$lnames))
		{
			global $error_messages;
			$er ="* cannot use symbols,special characters for first name and last name";
		 $error_messages['name']=$er;

		 return false;
		}
	}
}
function strpos_arr($haystack, $needle) { 
    if(!is_array($needle)) $needle = array($needle); 
    foreach($needle as $what) { 
        if(($pos = strpos($haystack, $what))!==false) return $pos; 
    } 
    return false; 
} 

function major($maj)
{
global $error_messages;
if ($maj=="" ){global $error_messages;
	$er ="* Major entry required";
	$error_messages['major']=$er; return false;
	}else{
		
		if(strpos_arr($maj,array("'",'"',"!","@","#","$","%","^","&","*","(",")",":",";","<",">",',',".","/","?","|","\\","_","{","}","[","]"))!==false)
		{//echo "****FOUND****";
			global $error_messages;
			$er ="* Cannot use symbols,special characters for major";
		 $error_messages['major']=$er; return false;}else{return true;}
		}
}

//////////////////////PASS////////////////////////////////////////////////////////
function pass($pass)
{
	if ($pass=="" ){
	global $error_messages;
	$er ="* Password entry required";
	$error_messages['password']=$er;return false;
	}else{
	
			if (false===(preg_match("/^(?=.*\d)(?=.*[a-zA-Z]).{4,18}$/", $pass) > 0))
			{
				global $error_messages;
				$er ="* Password format didn't match, please use all lowercase alphanumeric phrase and has to be between 4 to 18 characters in length";
				$error_messages['password']=$er;

				return false;
			}else {return true;}
			
	}
}

function valid_phone($ph)
{
	global $error_messages;
if (preg_match('/^[\d]{3}-[\d]{3}-[\d]{4}$/',$ph)) {
	
   return true;
} else {
	
    $er ="* Invalid format used, please enter valide phone number";
    $error_messages['phone']=$er;
    return false;
}
}


function valid_reg($data){
	
	global $error_messages;
	

		if($data['name']=="" || $data['lname']=="" || $data['email']=="" || $data['password']=="" || $data['major']=="" || $data['phone']=="" || $data['gender']=="" || $data['gradelevel']=="")
		{//echo "false";
			global $error_messages;
			global $smarty;
			$er ="* All fields are required";
			$error_messages['all']=$er;
			print_r ($error_messages);
			$smarty->assign('errors', $error_messages);
			return false;
		}
		else{//echo "true";
			names($data['name'],$data['lname']);
			pass($data['password']);
			check_email($data['email']);
			major($data['major']);
			valid_phone($data['phone']);
		 	if($error_messages['email']=='' && $error_messages['name']=='' && $error_messages['name1']=='' && $error_messages['password']==''
			&& $error_messages['all']=='' && $error_messages['major']=='' && $error_messages['phone']=='')
			{return true;}
			else
			{
				global $smarty;
				global  $error_messages;
				//print_r ($error_messages);
				$smarty->assign('errors', $error_messages);
				return false;
			}
		}

}
function valid_regs($data){
	
	 $errors = '';
	
global $smarty;
if(strpos_arr($data['name'],array("'",'"',"!","@","#","$","%","^","&","*","(",")",":",";","<",">",',',".","/","?","|","\\","_","{","}","[","]","-"))!==false || 
strpos_arr($data['lname'],array("'",'"',"!","@","#","$","%","^","&","*","(",")",":",";","<",">",',',".","/","?","|","\\","_","{","}","[","]","-"))!==false || 
strpos_arr($data['major'],array("'",'"',"!","@","#","$","%","^","&","*","(",")",":",";","<",">",',',".","/","?","|","\\","_","{","}","[","]","-"))!==false || 
strpos_arr($data['phone'],array("'",'"',"!","@","#","$","%","^","&","*","(",")",":",";","<",">",',',".","/","?","|","\\","_","{","}","[","]"))!==false)
		{//echo "****FOUND****";
			
			$er ="* Cannot use symbols,special characters in any field";
			$errors=$er; 
			//print_r($errors);
			$smarty->assign('errors2', $errors);
		 return false;
		}else{return true;}
		}
		
function valid_event($data){
	
	 $errors = '';
	
global $smarty;

if(strpos_arr($data['title'],array("'",'"',"!","@","#","$","%","^","&","*","(",")",":",";","<",">",',',".","/","?","|","\\","_","{","}","[","]","-"))!==false  || 
strpos_arr($data['paisa'],array("'",'"',"!","@","#","$","%","^","&","*","(",")",":",";","<",">",',',".","/","?","|","\\","_","{","}","[","]","-"))!==false || 
strpos_arr($data['strt_time1'],array("'",'"',"!","@","#","$","%","^","&","*","(",")",":",";","<",">",',',".","/","?","|","\\","_","{","}","[","]","-"))!==false || 
strpos_arr($data['strt_time2'],array("'",'"',"!","@","#","$","%","^","&","*","(",")",":",";","<",">",',',".","/","?","|","\\","_","{","}","[","]","-"))!==false || 
strpos_arr($data['strt_time3'],array("'",'"',"!","@","#","$","%","^","&","*","(",")",":",";","<",">",',',".","/","?","|","\\","_","{","}","[","]","-"))!==false || 
strpos_arr($data['end_time1'],array("'",'"',"!","@","#","$","%","^","&","*","(",")",":",";","<",">",',',".","/","?","|","\\","_","{","}","[","]","-"))!==false || 
strpos_arr($data['end_time2'],array("'",'"',"!","@","#","$","%","^","&","*","(",")",":",";","<",">",',',".","/","?","|","\\","_","{","}","[","]","-"))!==false || 
strpos_arr($data['end_time3'],array("'",'"',"!","@","#","$","%","^","&","*","(",")",":",";","<",">",',',".","/","?","|","\\","_","{","}","[","]","-"))!==false || 
strpos_arr($data['date1'],array("'",'"',"!","@","#","$","%","^","&","*","(",")",":",";","<",">",',',".","/","?","|","\\","_","{","}","[","]","-"))!==false || 
strpos_arr($data['date2'],array("'",'"',"!","@","#","$","%","^","&","*","(",")",":",";","<",">",',',".","/","?","|","\\","_","{","}","[","]","-"))!==false || 
strpos_arr($data['date3'],array("'",'"',"!","@","#","$","%","^","&","*","(",")",":",";","<",">",',',".","/","?","|","\\","_","{","}","[","]","-"))!==false || 
strpos_arr($data['prim_contact'],array("'",'"',"!","@","#","$","%","^","&","*","(",")",":",";","<",">",',',".","/","?","|","\\","_","{","}","[","]","-"))!==false || 
strpos_arr($data['sec_contact'],array("'",'"',"!","@","#","$","%","^","&","*","(",")",":",";","<",">",',',".","/","?","|","\\","_","{","}","[","]","-"))!==false || 
strpos_arr($data['street'],array("'",'"',"!","@","#","$","%","^","&","*","(",")",":",";","<",">",',',".","/","?","|","\\","_","{","}","[","]","-"))!==false || 
strpos_arr($data['city'],array("'",'"',"!","@","#","$","%","^","&","*","(",")",":",";","<",">",',',".","/","?","|","\\","_","{","}","[","]","-"))!==false || 
strpos_arr($data['state'],array("'",'"',"!","@","#","$","%","^","&","*","(",")",":",";","<",">",',',".","/","?","|","\\","_","{","}","[","]","-"))!==false || 
strpos_arr($data['zip'],array("'",'"',"!","@","#","$","%","^","&","*","(",")",":",";","<",">",',',".","/","?","|","\\","_","{","}","[","]","-"))!==false || 
strpos_arr($data['summary'],array("'",'"',"!","@","#","$","%","^","&","*","(",")",":",";","<",">",',',".","/","?","|","\\","_","{","}","[","]","-"))!==false)

{
	$er ="* Cannot use symbols,special characters in any field";
	$errors=$er; 
	$smarty->assign('eventerrs', $errors);
	return false;
	}
	else{return true;}
		}

		



?>