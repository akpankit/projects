
// ================================================================
//histo_plot.cpp
//
// Written by: ANKIT PATEL
// ================================================================
//

#include "IP.h"

using namespace std;

void histo_plot(imageP, imageP,int);
int min_max(long [], bool);

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// main:
//
// Main routine to apply histogram equalization
//
int
main(int argc, char **argv)
{
	imageP I1, I2;
	int flag;

	if(argc != 4) {
		cerr << "Usage: ./histo_plot infile outfile flag" << endl;
		exit(1);
	}

	I1 = IP_readImage(argv[1]);
	I2 = NEWIMAGE;
	flag = atoi(argv[3]);

	cout << "flag -- " << flag << endl;

	histo_plot(I1, I2,flag);
	IP_saveImage(I2, argv[2]);

	IP_freeImage(I1);
	IP_freeImage(I2);

	return 1;
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// histo_plot:
//
// Plotting the histogram to I1. Output goes to I2.
//
void
histo_plot(imageP I1, imageP I2 , int flag)
{
	int i, R;
	int left[MXGRAY], width[MXGRAY],count;
	int cur_row = 0;
	uchar *in, *out, lut[256];
	long total, Hsum, Havg, histo[MXGRAY];

	// total number of pixels in image
	total = (long) I1->width * I1->height;

	// init I2 dimensions and buffer
	I2->width  = 256;
	I2->height = 256;
	I2->image  = (uchar *) malloc(total);

	// init input and output pointers
	in  = I1->image;		// input  image buffer
	out = I2->image;		// output image buffer

	// compute histogram
	for(i=0; i<MXGRAY; i++) {
	histo[i] = 0;	// clear histogram
	lut[i] = 0; // init look up table,  setting all values in look up table to zero so that we get black background.
	}

	for(i=0; i<total;  i++) histo[in[i]]++;	// eval  histogram

	// make everything black
	for(i=0; i<total; i++) out[i] = lut[ in[i] ];

	int lower_bound = min_max(histo,0); // 0 is flag to return min.
	int upper_bound = min_max(histo,1); // 1 is flag to return max.

	cout << "max -- " << upper_bound << endl;

	// figuring out ration needed to bring all the values under 255
	double ratio = (double)(upper_bound - lower_bound)/256;

	cout << "ratio -- " << ratio << endl;
	

	int freq = 0;

	for(i = 0; i <=255; i++){ ///columns

		if(flag == 0 )
		 	freq = histo[i] / 2; // divide the frequencies by 2 if we want clip at y = 128 as max
		if(flag == 1 )
		 	freq = histo[i];

		int pix = 0;
		// Remember:  once we get range, each tick on y -axis is equals to (tick*range). so for example: frequency of 12 will be,
		// 			  tick number 3 because here tick is 3 and ratio is 4;
		// 			  Divide the frequency by ratio will give us exact row number corresponding to the frequency of intensity (example explained above).
		// 			  Example: if intensity 100 has frequncy of 12, then it will divide 12 / ratio  to get the correct row number(tick)on y-axis.
		freq = (double)(freq / ratio) ; 
		// cout << "frequency -- "<< ceil(freq)<< endl;

		// Once we get the row number, we have to make sure that histogram draws staright
		// and not upside down, so we subtract the row number we got from above from total rows(256) which will give
		// us the row to start the highlighting of pixel (top to bottom) and then using loop we highlight every pixel until it reaches last row.

		int srt_row = (int)I2->height - (freq); // This basically is giving us the row to start highlighting the pixel from.

		for (int k=srt_row; k <=255; k++){ // keep highlighting until the row reaches to 255 (top to bottom highlighting).
					pix = (k*I2->width) + i; //  This formula will give us pixel right under the previously highlighted pixel for the same column 
					// cout << "pix value" << pix << endl;
					out[pix] = 255;
		}
	}
}

int min_max(long hist[], bool flag){

int temp = 0;

if(flag ==1 ){
    for(int m=0;m<256;m++)
    {
        if(hist[m]>temp)
        temp=hist[m];
    }

    return temp;
	}
	else if (flag == 0){
	int smallest = 9999;

	for (int k = 0; k < 256; k++) {
    	if (hist[k] < smallest) {
        	smallest = hist[k];
    	}
	}

	return smallest;
	}
	return 1;
}
