
// ================================================================
//histo_plot.cpp
//
// Written by: ANKIT PATEL
// ================================================================
//

#include "IP.h"

using namespace std;

void histo_plot(imageP, imageP,int);
int min_max(long [], bool);

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// main:
//
// Main routine to apply histogram equalization
//
int
main(int argc, char **argv)
{
	imageP I1, I2;
	int flag;

	if(argc != 4) {
		cerr << "Usage: histeq infile outfile flag" << endl;
		exit(1);
	}

	I1 = IP_readImage(argv[1]);
	I2 = NEWIMAGE;
	flag = atoi(argv[3]);

	cout << "flag -- " << flag << endl;

	histo_plot(I1, I2,flag);
	IP_saveImage(I2, argv[2]);

	IP_freeImage(I1);
	IP_freeImage(I2);

	return 1;
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// histo_plot:
//
// Plotting the histogram to I1. Output goes to I2.
//
void
histo_plot(imageP I1, imageP I2 , int flag)
{
	int i, R;
	int left[MXGRAY], width[MXGRAY],count;
	int cur_row = 0;
	uchar *in, *out, lut[256];
	long total, Hsum, Havg, histo[MXGRAY];

	// total number of pixels in image
	total = (long) I1->width * I1->height;

	// init I2 dimensions and buffer
	I2->width  = I1->width;
	I2->height = I1->height;
	I2->image  = (uchar *) malloc(total);

	// init input and output pointers
	in  = I1->image;		// input  image buffer
	out = I2->image;		// output image buffer

	// compute histogram
	for(i=0; i<MXGRAY; i++) {
	histo[i] = 0;	// clear histogram
	lut[i] = 0; // init look up table,  setting all values in look up table to zero so that we get black background.
	}

	for(i=0; i<total;  i++) histo[in[i]]++;	// eval  histogram

	// make everything black
	for(i=0; i<total; i++) out[i] = lut[ in[i] ];

	int lower_bound = min_max(histo,0); // 0 is flag to return min.
	int upper_bound = min_max(histo,1); // 1 is flag to return max.

	cout << "max -- " << upper_bound << endl;

	// figuring out ration needed to bring all the values under 255
	double ratio = (double)(upper_bound - lower_bound)/256;

	cout << "ratio -- " << ratio << endl;
	

	int freq = 0;
	int start_column = 0;
	int start_row = 255;

	for(i = 0; i <=total; i++){ //pixels

		int col = i % 256;
		int row = i /256;

		if(start_column  == 255){
			start_column = 0;
			start_row --;
			
		}
		// cout << "start row -- " << start_row << "start column-- "<<start_column << endl;
		int normalized = histo[start_column] / ratio;
		if(normalized >= row){
			out[((start_row)*256)+start_column] = 255;	
		}
		start_column ++;
	}
}

int min_max(long hist[], bool flag){

int temp = 0;

if(flag ==1 ){
    for(int m=0;m<256;m++)
    {
        if(hist[m]>temp)
        temp=hist[m];
    }

    return temp;
	}
	else if (flag == 0){
	int smallest = 1;

	for (int k = 0; k < 256; k++) {
    	if (hist[k] < smallest) {
        	smallest = hist[k];
    	}
	}

	return smallest;
	}
	return 1;
}
