
// ================================================================
// histogram_stretch.cpp - Threshold program.
//
// Written by: ANKIT PATEL
// =====================================================================

#include "IP.h"


using namespace std;

// function prototype
void histo_stretch(imageP, int,int, imageP);
int min_max(uchar [],int,bool);
int min_max2(long [], int , bool);

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// main:
//
// Main routine to gamma_correct the image.
//
int
main(int argc, char** argv)
{
	int t1,t2;
	imageP	I1, I2;

	// error checking: proper usage
	if(argc != 5) {
		cerr << "Usage Erro !!" <<endl;
		cerr << "Example: ./histo_stretch input-file t1  t2 output-file\n";
		exit(1);
	}

	// read input image (I1) and reserve space for output (I2)
	I1 = IP_readImage(argv[1]);
	I2 = NEWIMAGE;

	// read the quantization level
	t1 = atoi(argv[2]);
	t2 = atoi(argv[3]);

	cout << t1 << " -- "<< t2 << endl;

	// threshold image and save result in file
	histo_stretch(I1, t1,t2, I2);
	IP_saveImage(I2, argv[4]);

	// free up image structures/memory
	IP_freeImage(I1);
	IP_freeImage(I2);

	return 1;
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Histo_stretch
// This function basically takes the t1 and t2 and stretch the intensities in that range so that 
// we can have better output image.
//
//
void
histo_stretch(imageP I1, int t1, int t2, imageP I2)
{
	int	 i, total;
	//inverse of gamma to use it in exponent

	uchar	*in, *out, lut[256];
	long histo[MXGRAY] = {};

	// total number of pixels in image
	total = I1->width * I1->height;

	// init I2 dimensions and buffer
	I2->width  = 256;
	I2->height = 256;
	I2->image  = (uchar *) malloc(total);
	if(I2->image == NULL) {
		cerr << "thr: Insufficient memory\n";
		exit(1);
	}

	in  = I1->image;	// input  image buffer
	out = I2->image;	// output image buffer

	//below is the max and min intensity values of the image buffer of original image.

	int max_intensity = min_max(in,total,1);//returns maximum value
	int min_intensity  = min_max(in,total,0);//returns minimum values


	cout << "max_intensity -- " << max_intensity << endl;
	cout << "min_intensity -- " << min_intensity << endl;

	int contrast = max_intensity - min_intensity;

	// // iterate over all pixels

	// // visit all input pixels and apply lut to threshold
	in  = I1->image;	// input  image buffer
	out = I2->image;	// output image buffer

	if( t1 < 0 ){
		t1 = 0;
	}
	
	if(t2 > 255 || t2 < 0){
		t2 = 255;
	}

	for(i=0; i<total; i++) {
		out[i] = 0;
		int temp = 0;

		if( in[i] < t1){
			histo[0]++;
		}else if(in[i] > t2){
			histo[255]++;
		}else{
		temp = (255 * (in[i] - min_intensity)) / (max_intensity - min_intensity) ;
		// cout << "temps --" << temp << endl;
		histo[temp]++;
		}
	}

	//Below max and min will give us maximum and minimum values of intensities count .
	
	int lower_bound = min_max2(histo,MXGRAY,0); // 0 is flag to return min.
	int upper_bound = min_max2(histo,MXGRAY,1); // 1 is flag to return max.

	cout << "max histogram bound-- " << upper_bound << endl;

	// figuring out ration needed to bring all the values under 255
	double ratio = (double)(upper_bound - lower_bound)/256;
	cout << "ratio -- " << ratio << endl;

//******************************************//

	// -- START DRAWING HISTOGRAM NOW --//

/********************************************/
	int freq = 0;
	int flag = 1;

	for(i = 0; i <=255; i++){ ///columns

		if(flag == 0 )
		 	freq = histo[i] / 2; // divide the frequencies by 2 if we want clip at y = 128 as max
		if(flag == 1 )
		 	freq = histo[i];

		int pix = 0;
		// Remember:  once we get range, each tick on y -axis is equals to (tick*range). so for example: frequency of 12 will be,
		// 			  tick number 3 because here tick is 3 and ratio is 4;
		// 			  Divide the frequency by ratio will give us exact row number corresponding to the frequency of intensity (example explained above).
		// 			  Example: if intensity 100 has frequncy of 12, then it will divide 12 / ratio  to get the correct row number(tick)on y-axis.
		freq = (double)(freq / ratio) ; 
		// cout << "frequency -- "<< ceil(freq)<< endl;

		// Once we get the row number, we have to make sure that histogram draws staright
		// and not upside down, so we subtract the row number we got from above from total rows(256) which will give
		// us the row to start the highlighting of pixel (top to bottom) and then using loop we highlight every pixel until it reaches last row.

		int srt_row = (int)I2->height - (freq); // This basically is giving us the row to start highlighting the pixel from.

		for (int k=srt_row; k <=255; k++){ // keep highlighting until the row reaches to 255 (top to bottom highlighting).
					pix = (k*I2->width) + i; //  This formula will give us pixel right under the previously highlighted pixel for the same column 
					// cout << "pix value" << pix << endl;
					out[pix] = 255;
		}
	}
}

//Below are the two functions to find min and max. Reason to make it 2 functions because 
// i wanted to handle the arrays differently.

int min_max(uchar hist[], int size, bool flag){

int temp = 0;

if(flag ==1 ){
    for(int m=0;m<size;m++)
    {
    	// cout << m << ":" << hist[m] << endl;
        if(hist[m]>temp)
        temp=hist[m];
    }

    return temp;
	}
	else if (flag == 0){
	int smallest = 9999;

	for (int k = 0; k < size; k++) {
    	if (hist[k] < smallest) {
        	smallest = hist[k];
    	}
	}
	return smallest;
	}
	return 1;
}

int min_max2(long hist[], int size, bool flag){

int temp = 0;

if(flag ==1 ){
    for(int m=0;m<size;m++)
    {
    	// cout << m << ":" << hist[m] << endl;
        if(hist[m]>temp)
        temp=hist[m];
    }

    return temp;
	}
	else if (flag == 0){
	int smallest = 9999;

	for (int k = 0; k < size; k++) {
    	if (hist[k] < smallest) {
        	smallest = hist[k];
    	}
	}

	return smallest;
	}
	return 1;
}