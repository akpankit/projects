from django.forms import ModelForm
from gamedata.models import UploadedGame, Game, Platform
from django.contrib.auth.models import User
from django import forms


class RegistrationForm(forms.Form):
    username = forms.CharField(max_length=30)
    password = forms.CharField(widget=forms.PasswordInput)
    repassword = forms.CharField(widget=forms.PasswordInput)
    email = forms.EmailField()
    profile_image = forms.ImageField(required=False, error_messages={'invalid_image': 'Upload a valid image.'})

    def clean_username(self):
        entry = self.cleaned_data['username']
        try:
            User.objects.get(username=entry) #using get because username should be unique
        except User.DoesNotExist:
            if entry == "None":
                raise forms.ValidationError("You cannot use that reserved username!")
            else:
                pass
        else:
            raise forms.ValidationError("That username already exists!")

        return entry

    def clean_repassword(self):
        entry = self.cleaned_data['password']
        reentry = self.cleaned_data['repassword']
        if entry != reentry:
            raise forms.ValidationError("The passwords do not match!")
        return reentry


class UploadedGameForm(ModelForm):
    game = forms.ModelChoiceField(
        queryset=Game.objects.all(),
        widget=forms.TextInput(attrs={'size': 20, 'class': 'autocomplete-game typeahead'}),
        error_messages={'invalid_choice': 'Select a valid choice'}
    )
    platform = forms.ModelChoiceField(
        queryset=Platform.objects.all(),
        widget=forms.TextInput(attrs={'class': 'autocomplete-platform typeahead'}),
        error_messages={'invalid_choice': 'Select a valid choice'}
    )
    class Meta:
        model = UploadedGame
        exclude =  ('user', 'datetime', 'upvote', 'downvote', 'original_file_name')


class ContactUsForm(forms.Form):
    name = forms.CharField(max_length=20)
    email = forms.EmailField(max_length=20)
    message = forms.CharField(widget=forms.Textarea())
