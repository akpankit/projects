import logging

from django.views.generic import TemplateView
from django.core.mail import send_mail
from django.template import loader, RequestContext
from django.http import HttpResponse

from gamedata.forms import ContactUsForm


logger = logging.getLogger(__name__)


class StatisticsView(TemplateView):
    template_name = 'statistics.html'

    def get_context_data(self, **kwargs):
        context = super(StatisticsView, self).get_context_data(**kwargs)

        return context


class CareersView(TemplateView):
    template_name = 'careers.html'

    def get_context_data(self, **kwargs):
        context = super(CareersView, self).get_context_data(**kwargs)

        return context


class TeamView(TemplateView):
    template_name = 'team.html'

    def get_context_data(self, **kwargs):
        context = super(TeamView, self).get_context_data(**kwargs)

        return context


class ContactUsView(TemplateView):
    template_name = 'contactus.html'

    def dispatch(self, request, *args, **kwargs):
        self.template = loader.get_template(self.template_name)
        response = super(ContactUsView, self).dispatch(request, *args, **kwargs)

        return response

    def post(self, request, *args, **kwargs):
        contact_us_form = ContactUsForm(self.request.POST)
        if contact_us_form.is_valid():
            name = self.request.POST['name']
            subject = 'Support - User: %s' % (name,)
            message = self.request.POST['message']
            from_address = self.request.POST['email']
            to_addresses = ['noreplysavegame@gmail.com']

            try:
                send_mail(subject, message, from_address, to_addresses)
            except Exception as e:
                logger.exception('Could not send mail')
                logger.exception('%s' % (e,))
            else:
                contact_us_form = ContactUsForm()
                context = RequestContext(request, {
                    'username': name,
                    'form': contact_us_form
                })

                return HttpResponse(self.template.render(context))

        else:
            context = RequestContext(request, {
                'form': contact_us_form
            })

            response = HttpResponse(self.template.render(context))

            return response

    def get(self, request, *args, **kwargs):
        contact_us_form = ContactUsForm()
        context = RequestContext(request, {
            'form': contact_us_form
        })
        return HttpResponse(self.template.render(context))


class RegistrationThanksView(TemplateView):
    template_name = 'account/regthanks.html'
    def get_context_data(self, **kwargs):
        context = super(RegistrationThanksView, self).get_context_data(**kwargs)
        context.update({
            'email': self.request.session['useremail']
        })
        return context