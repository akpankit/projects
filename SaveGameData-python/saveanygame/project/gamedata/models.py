
import random
import os
import logging

from django.db  import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save


max_length = 512

log = logging.getLogger(__name__)


class CompanyManager(models.Manager):
    def get_by_natural_key(self, name):
        return self.get(name=name)


class Company(models.Model):
    objects = CompanyManager()

    name = models.CharField(max_length=max_length, unique=True)

    def __unicode__(self):
        return  u'%s' % self.name

    class Meta:
        verbose_name = 'Companies'
        verbose_name_plural = 'Companies'


class PlatformManager(models.Manager):
    def get_by_natural_key(self, name):
        return self.get(name=name)


class Platform(models.Model):
    objects = PlatformManager()

    name = models.CharField(max_length=max_length, unique=True)
    company = models.ForeignKey(Company)

    def __unicode__(self):
        return  u'%s' % self.name

    class Meta:
        verbose_name = 'Platforms'
        verbose_name_plural = 'Platforms'


class GenreManager(models.Manager):
    def get_by_natural_key(self, name):
        return self.get(name=name)

class Genre(models.Model):
    objects = GenreManager()

    name = models.CharField(max_length=max_length, unique=True)

    def __unicode__(self):
        return  u'%s' % self.name

    class Meta:
        verbose_name = 'Genres'
        verbose_name_plural = 'Genres'


class Game(models.Model):
    title = models.CharField(max_length=max_length)
    release_date = models.DateField(null=True)
    platform = models.ForeignKey(Platform)
    genre = models.ForeignKey(Genre)
    smallcover = models.ImageField(upload_to='gamedata/static/images')
    description = models.TextField()

    def admin_image(self):
        return '<img src="%s"/>' % self.smallcover.name

    admin_image.allow_tags = True

    def __unicode__(self):
        return  u'%s' % self.title

    class Meta:
        verbose_name = 'Games'
        verbose_name_plural = 'Games'


class UserProfile(models.Model):
    user = models.ForeignKey(User)
    avatar = models.ImageField(upload_to='static/images')
    private = models.BooleanField()

    def __str__(self):
        return "%s's profile" % self.user

    def admin_image(self):
        return '<img src="/static/images/%s" />' % self.avatar.name

    def get_profile_url(self, type='full'):
        try:
            directory = os.path.dirname(str(self.avatar.name))
            filename = os.path.basename(self.avatar.path)

            if type == 'thumb':
                filename = filename.split('.')[0] + '_thumb.' + filename.split('.')[1]
            else:
                filename = filename.split('.')[0] + '.' + filename.split('.')[1]

            filepath = 'static/images/users/%s/profiles/%s' % (
                            self.user.id,
                            filename
                        )
        except Exception as e:
            log.error('Error: ' % (e))
            filepath = False

        return filepath

    def get_full_profile_url(self):
        return self.get_profile_url()

    def get_thumbnail(self):
        return self.get_profile_url('thumb') 

    admin_image.allow_tags = True


def create_user_profile(sender, instance, created, **kwargs):
    if created:
        profile, created = UserProfile.objects.get_or_create(user=instance)
        profile.save()

post_save.connect(create_user_profile, sender=User)

class UploadedGame(models.Model):
    game = models.ForeignKey(Game)
    platform = models.ForeignKey(Platform)
    file = models.FileField(upload_to='gamedata/static/saved_games')
    user = models.ForeignKey(User)
    datetime = models.DateTimeField()
    title = models.CharField(max_length=max_length)
    description = models.TextField()
    upvote = models.IntegerField()
    downvote = models.IntegerField()
    private = models.BooleanField()
    original_file_name = models.CharField(max_length=max_length)

    def __unicode__(self):
        return  u'%s' % self.game.title

    class Meta:
        verbose_name = 'UploadedGames'
        verbose_name_plural = 'UploadedGames'

VOTE_CHOICES = (('upvote', 'upvote'), ('downvote', 'downvote'), ('none', 'none'))
class UploadedGameVote(models.Model):
    user = models.ForeignKey(User)
    game = models.ForeignKey(UploadedGame)
    vote = models.CharField(choices = VOTE_CHOICES, max_length = max_length)

    def __unicode__(self):
        return u'%s' % self.user

    class Meta:
        verbose_name = 'UploadedGameVote'
        verbose_name_plural = 'UploadedGameVotes'

class Comments(models.Model):
    uploadedgame = models.ForeignKey(UploadedGame)
    user = models.ForeignKey(User)
    comment = models.TextField()
    datetime = models.DateTimeField()

    def __unicode__(self):
        return  u'%s' % self.comment

    class Meta:
        verbose_name = 'Comments'
        verbose_name_plural = 'Comments'

