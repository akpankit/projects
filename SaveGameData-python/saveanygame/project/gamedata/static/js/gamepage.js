/**
 *
 * Requires: jQuery
 */

var Gamedata = Gamedata || {};

Gamedata.GamePage = (function ($) {
    var _staticURL, _uploadedID, _userID;

    function init(staticURL, uploadedID, userID) {
        _staticURL = staticURL;
        _uploadedID = uploadedID;
        _userID = userID;

        setEventHandlers();
    }

    function getUploadedFileData(data){
        try {
            data = JSON.parse(data);
        } catch (e) {
            data = {};
        }

        $('#data_title').html(data['data_title']);
        $('#download_link').attr('href', data['download_link']); /* Set the attribute dynamically */
        $('#savefilename').text(data.savefilename);
        $('#uploader').html(data['uploader']);
        $('#date').html(data['date']);

        if (data.profile_path) {
            $('#profile').attr('src', data['profile_path']);
        } else {
            $('#profile').attr('src', _staticURL + 'images/profile.png');
        }

        $('#game_desc').html(data['game_desc']);
        $('#upvotes').html(data['upvotes']);
        $('#downvotes').html(data['downvotes']);

        if (data['vote_status'] == -1) {
            $('#dnv').attr('src', _staticURL + 'images/dnv.png');
            $('#upv').attr('src', _staticURL + 'images/upv-in.png');
        } else if (data['vote_status'] == 1) {
            $('#upv').attr('src', _staticURL + 'images/upv.png');
            $('#dnv').attr('src', _staticURL + 'images/dnv-in.png');
        } else {
            $('#upv').attr('src', _staticURL + 'images/upv-in.png');
            $('#dnv').attr('src', _staticURL + 'images/dnv-in.png');
        }

        /**
         * Load the comment section get all the date properly formatted
         **/

        var info = [];

        for(var i in data['info2']) {
             info.push(Number(i));
        }

        info = info.reverse();

        var comment_id = 0,
            date = 0,
            comment = '',
            user = '';

        for(var i = 0; i < info.length; i++) {
            comment_id = data['info2'][info[i]]['id'];
            date = data['info2'][info[i]]['datetime'];
            comment = data['info2'][info[i]]['comment'];
            user = data['info2'][info[i]]['username'];

            var div = "<div id='" + comment_id +
            "' class='acomment'><br /><a href='#'>" + user + "</a> &nbsp; &nbsp; <span style='font-style:italic'>" +  date + " </span><p>" +
            comment
            + "</p><hr /><br /></div>";
            $("#comment_section").append(div);
        }
    }

    function getCommentData(data){
        data = JSON.parse(data);

        var comment_id = data['only']['id'],
            date = data['only']['datetime'],
            comment = data['only']['comment'],
            user = data['only']['username'],
            div = "<div id='"
                + comment_id + "' class='acomment'><br /><a href='#'>" + user
                + "</a> &nbsp; &nbsp; <span style='font-style:italic'>" + date
                + "</span><p>" + comment + "</p><hr /><br /></div>";

        if ($("#comment_section").children().length > 0 ) {
            $(div).insertBefore(".acomment");
            $("#" + comment_id ).hide();
            $("#" + comment_id ).show('slow');
        } else {
            $("#comment_section").append(div);
            $("#" + comment_id ).hide();
            $("#" + comment_id ).show('slow');
        }
    }

    function getvotedata(data){
        data = eval('(' + data + ')');
        var arrow = data['arrow']
        if (arrow === 'UP') {
            $('#upv').attr('src', _staticURL + 'images/upv.png');
        } else if (arrow === 'DWN')
            $('#dnv').attr('src', _staticURL + 'images/dnv.png');
        else {
            $('#upv').attr("src", _staticURL + 'images/upv-in.png');
            $('#dnv').attr("src", _staticURL + 'images/dnv-in.png');
        }
        $("#upvotes").html(data['upvotes']);
        $("#downvotes").html(data['downvotes']);
    }

    function setEventHandlers() {
       $(document).ready(function (){
            var uploaded_game_id = _uploadedID;

            /* Adding this next line for voting */
            var cur_user = _userID;

            /* This part uses ajax to get the necessary
               data from the db, given the user_id and uploaded_game_id */

            $.ajax({
                type: 'GET',
                url: '/getUploadedFileData/',
                data: {'uploaded_game_id' : uploaded_game_id, 'cur_user' : cur_user },
                async: false,
                success: getUploadedFileData
            });

            $('#sub').click(function () {
                // We remove extra whitespace from the comment input
                var commentData = $('#comment_area').val();

                commentData = commentData.replace(/(\r\n|\n|\r)/gm,"");
                commentData = commentData.replace(/\s+/g," ");

                if (commentData === '' || commentData === ' ') {
                    alert('Provide a comment!');
                    return;
                }

                if (cur_user !== 'None') {
                    $.ajax({
                        async: false,
                        success: getCommentData,
                        url: '/getCommentData/',
                        data: {
                            'user_id' : cur_user,
                            'uploaded_game_id' : uploaded_game_id,
                            'comment_data': commentData
                        }
                    });
                } else {
                    alert('Please log-in to comment!');
                }
            });

            $("#upv").click(function(){
                if (cur_user != "None")
                    $.ajax({
                        async: false,
                        success: getvotedata,
                        url: "/getvotedata/",
                        data: { 'uploaded_game_id' : uploaded_game_id, 'vote': 1, 'cur_user' : cur_user }
                    });
                else
                    alert("Please log-in to vote!");

            });

            $("#dnv").click(function(){
                if (cur_user != "None")
                    $.ajax({
                        async: false,
                        success: getvotedata,
                        url: "/getvotedata/",
                        data: { 'uploaded_game_id' : uploaded_game_id, 'vote': 0, 'cur_user' : cur_user }
                    });
                else
                    alert("Please log-in to vote!");
            });

        });
    }

    return {
        init: init
    };
}(jQuery));




