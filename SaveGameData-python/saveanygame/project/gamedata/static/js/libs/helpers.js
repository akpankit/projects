/**
 * Requires: jQuery, Underscore
 */

var Gamedata = Gamedata || {};

if (!Gamedata.Helpers) {
   Gamedata.Helpers = {};
}

Gamedata.Helpers = (function () {
    function _redirect () {
       window.location.href = '/';
    }

    function _counter (selector) {
        return function () {
            var c = $(selector).text();
            $(selector).text(c -= 1);
        };
    }

    function countdown(selector) {
        setTimeout(_redirect, 5000);
        setInterval(_counter(selector), 1000);
    }

    return {
        countdown: countdown
    };
}($));