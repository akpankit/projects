/**
 *
 * Upload Page Module
 *
 * Requires: jQuery, Twitter TypeaheadJS
 */

var Gamedata = Gamedata || {};

Gamedata.Upload = (function ($) {
    function init () {
        setPageEventHandlers();
    }

    function setPageEventHandlers() {
        $(document).ready(function () {
            $('input.autocomplete-game').autocomplete({
                source: function (request, response) {
                    var url = '/ajax/games',
                        data = request;

                    $.ajax({
                        url: url,
                        data: data,
                        success: function (res) {

                        },
                        error: function (res) {

                        }
                    });
                },
                messages: {
                    noResults: '',
                    results: $.noop
                }
            });

            $('input.autocomplete-platform').autocomplete({
                source: '/ajax/platforms',
                messages: {
                    noResults: '',
                    results: $.noop
                }
            });
        });
    }

    return {
        init: init
    };
}(jQuery));

// Entry point
Gamedata.Upload.init();


