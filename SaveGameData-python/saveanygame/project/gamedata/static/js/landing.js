/*
 * Require: jQuery
 *
 */


var Gamedata = Gamedata || {};

Gamedata.Landing = (function ($) {
    function init () {
        setEventHandlers();
    }

    function setEventHandlers() {
        $(document).ready(function () {
            $('#access-btn').click(function (e) {
                e.preventDefault();
                e.stopPropagation();
                $(this).popover();
            });
        });
    }

    return {
        init: init
    }
}($));