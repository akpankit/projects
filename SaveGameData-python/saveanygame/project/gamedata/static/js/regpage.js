/**
 * Registration Page Module
 *
 * Requires: jQuery, underscore
 */


var Registration = Registration || {};

Registration.Main = (function () {

  function init() {
    setEventHandlers();
  }

  function setEventHandlers() {
    $(document).ready(function () {
      var selectors = [
        '#username-error',
        '#email-error',
        '#password-error',
        '#image-error'
      ];
      $('#registerbutton').button();


      _.each(selectors, function (selector, index) {
        var $e = $(selector);
        $e.popover();
        $e.trigger('click');
      })
    });
  }

  return {
    init: init
  };
}($, _));

Registration.Main.init();

