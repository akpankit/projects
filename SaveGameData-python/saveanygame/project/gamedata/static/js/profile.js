/**
 *  Profiles Module
 *  Requires: jQuery
 */


var Gamedata = Gamedata || {};


Gamedata.Profile = (function () {

  function init () {
    setEventHandlers();
  }

  function showPanel() {
      button = document.getElementById('open-panel-button');
      button.value = 'Cancel'

      curpanel = document.getElementById('info');
      curpanel.style.display = 'none';

      panel = document.getElementById('settings-panel');
      panel.style.display = 'block';
      button.onclick = function () {
          button.onclick = showPanel;
          button.value = 'Update';
          panel.style.display = 'none';
          curpanel.style.display = 'block';
      }
  }
  function submitPost() {
      if ($('input[type=file]').val() == '') {
          $.post('', $('#profile-form').serialize(),
                  function (data) {
                      var $firstName = $('#first_name_info'),
                          $lastName = $('#last_name_info');

                      $firstName.html(data[0].fields.first_name);
                      $firstName.removeClass('text-muted');
                      $lastName.html(data[0].fields.last_name);
                      $lastName.removeClass('text-muted');

                      $('#username').html(data[0].fields.username);
                      $('#avatar-img').attr('src', '/' + data[1].fields.avatar);
                      var button = document.getElementById('open-panel-button');

                      if (!_.isNull(button)) {
                          button.click();
                      }

                  }).error(function () {
                      $('#profile-form').submit();
                  });
      } else {
          $('#profile-form').submit();
      }
  }

  function deleteSuccessCallback (id) {
      return function (data) {
          if ('error' in data) {
              alert('There was an error, upload_id: ' + id + ' message: ' + data.error);
          } else if ('upload_id' in data) {
              $('#upload_tbl td').each(
                  function () {
                      if ($(this).parent().attr('id') == data.upload_id) {
                          $(this).parent().remove();
                      }
                  }
              );
          } else {
              alert('I was expecting a return from this post?!?!?');
          }
      };
  }

  function deleteErrorCallback (id) {
      return function (res, data) {
          $('#delete_upload_' + id).submit();
      };
  }

  function deleteUpload(id) {
      var des = window.confirm("Are you sure you want to delete this upload? \nthere's no going back ...");
      if (des) {
          $.post('',
              {
                  upload_id: id,
                  csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value
              }, deleteSuccessCallback(id)).error(deleteErrorCallback(id));
      }
  }

  function updateUploadPrivate(id, value) {
      $.post('', {
              upload_private_id: id,
              csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
              upload_private_value: value
              },
              function (data) {
                  if ('error' in data)
                      alert('there was an error \nupload_private_id: ' + upload_private_id + ' value: ' + value + ' msg: ' + data.message);
                  else if ('upload_private_id' in data)
                      $('#upload_tbl td').each(
                              function () {
                                  if ($(this).parent().attr('id') == data.upload_private_id)
                                      $(this).find('[type=checkbox]').attr('checked', data.upload_private);
                              });
              }).error(function () {
                  alert("There was an error, sry ...");
              });
  }

  function setEventHandlers() {
    $(document).ready(function () {
        $('.delete-upload').click(function (e) {
            e.preventDefault();
            e.stopPropagation();

            var id = $(this).attr('data-id');
            deleteUpload(id);
        });

        $('input#profile-submit').click(function (e) {
          submitPost();
        });

        $('#private-flag').click(function (e) {
          var $this = $(this),
              id = Number($this.attr('data-id'));
          updateUploadPrivate(id, $this.is(':checked'));
        });
    });
  }

  return {
    init: init
  };
}($));

Gamedata.Profile.init();



