import os
import hashlib
import datetime
import logging

from django.conf import settings


log = logging.getLogger(__name__)


def get_app_path(filepath):
    return os.path.join(settings.TEMPLATE_BASE + '/gamedata', filepath)


def save_profile_pic(request, profile, file):
    """ Method to save the registered profile image (if applicable)
        to the filesystem.

        :param request:
        :param profile:
        :param file:
    """
    hash = hashlib.sha256()
    hash.update(datetime.datetime.now().strftime('%Y-%m-%d-%s'))

    random_string = hash.hexdigest()

    filename = 'static/images/users/%s/profiles/%s' % (
                  profile.user.id,
                  random_string + '.' + file.name.split('.')[-1:][0]
                )

    filename_thumb = 'static/images/users/%s/profiles/%s' % (
                        profile.user.id,
                        random_string + '_thumb' + '.' + file.name.split('.')[-1:][0]
                      )

    destination = open(get_app_path(filename), 'wb')
    destination_thumb = open(get_app_path(filename_thumb), 'wb')

    # Save normal size image
    for chunk in file.chunks():
        destination.write(chunk)
        destination_thumb.write(chunk)

    destination.close()
    destination_thumb.close()

    profile.avatar = file
    profile.avatar.name = filename

