from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import direct_to_template
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin

from gamedata.views import (
    LandingPageView,
    StatisticsView,
    CareersView,
    TeamView,
    ContactUsView,
    RegistrationThanksView,
    RegistrationView
)


admin.autodiscover()


urlpatterns = patterns(
   '',
    url(r'^$', LandingPageView.as_view(), name='landing-page'),

    url(r'sign[Ii]n/', 'gamedata.views.sign_in'),
    url(r'sign[Oo]ut/', 'gamedata.views.sign_out'),
    url(r'gamepage/', 'gamedata.views.gamepage'),
    url(r'results/', 'gamedata.views.results'),
    url(r'upload/', 'gamedata.views.upload'),
    url(r'infopage/', 'gamedata.views.gameinfo'),
    url(r'^platform/', 'gamedata.views.platform'),
    url(r'^genre/', 'gamedata.views.genre'),
    url(r'getvotedata/', 'gamedata.views.getvotedata'),
    url(r'getUploadedFileData/', 'gamedata.views.getUploadedFileData'),
    url(r'getCommentData/', 'gamedata.views.getCommentData'),
    url(r'^profile/(?P<user_id>\d*)/$', 'gamedata.views.profile'),
    (r'aboutus/$', direct_to_template, {'template':'aboutus.html'}),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^ajax/games/', 'gamedata.views.ajax_games'),
    url(r'^ajax/platforms/', 'gamedata.views.ajax_platforms'),

    (r'^privacy/$', direct_to_template, {'template': 'privacy.html'}),

    # Redirects
    url(r'^registration/thanks/$', RegistrationThanksView.as_view()),

    # Main Class Based Views
    url(r'^statistics/$', StatisticsView.as_view()),
    url(r'^careers/$', CareersView.as_view()),
    url(r'^team/$', TeamView.as_view()),
    url(r'^contactus/$', ContactUsView.as_view()),
    url(r'^registration/$', RegistrationView.as_view()),
)

urlpatterns += staticfiles_urlpatterns()
