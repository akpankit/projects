<?php

    require_once 'api.php';
    
    $db = mysql_connect('localhost', 'root', '1234');
    mysql_select_db('api');
    
    $api = new Api($db);
    
    // index.php?namespace=user
    $api->register('users')
        ->setTable('users')
        ->setOutput(array(
            'json'
        ))
        ->setKey(false) // optional
        ->setColumns(array(
            'id',
            'username',
            'firstname'
        )); // optional
    
    // index.php?namespace=user&username=Daniel&key=9291096c33bcd40c6bf1a2e7bfdb266b5e2bafbb
    $api->register('user')
        ->setTable('users')
        ->setOutput(array(
            'json', 
            'xml',
            'csv',
            'yaml'
        ))
        ->setKey(false) // optional
        ->setCache(10, './cache/')  // optional
        ->setColumns(array(
            'id',
            'username',
            'firstname'
        ))
        ->setLimit(200) // optional
        ->setWhere(array(
            'username' => array(
                'route'     => 'username', // get param $_GET['username']
                //'rule'      => 'alpha_numeric' or 'numeric',
                'connect'   => false // connect next where condition with "or" or "and"
            )
        )); // optional
    
    $api->render($_GET);
    
?>
